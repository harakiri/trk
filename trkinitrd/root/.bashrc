# .bashrc

PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin
ENV=$HOME/.bashrc
USERNAME="root"
export USERNAME ENV PATH
# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# enable programmable completion features
if [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
fi
export SCREENDIR=$HOME/tmp
TTYDEV=`who am i| awk '{print $2}'| grep tty`
# Run trkmenu only on the first terminal
test r$TTYDEV = rtty1 && grep -q trkmenu /proc/cmdline && ! test r$TRKMENU = ron && /bin/trkmenu
# A screen only on tty6 will do just fine
if [ r$TTYDEV = rtty6 -a !  -p $SCREENDIR/*.$TTYDEV ]; then
screen -S $TTYDEV
fi;
# Make sure scp doesn 't fail because there is output when there 's no terminal.
tty -s && cat /etc/issue
