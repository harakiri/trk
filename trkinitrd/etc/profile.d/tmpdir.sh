#!/bin/sh
# $Id: tmpdir.sh,v 1.12 2003/09/02 09:37:49 flepied Exp $
# Set TMPDIR to ~/tmp and create it if directory not exist.

if [ -f /etc/sysconfig/system ];then
    . /etc/sysconfig/system
fi

if [ -f /etc/sysconfig/msec ];then
    eval `grep ^SECURE_LEVEL= /etc/sysconfig/msec | head -1`
fi

if [[ "$SECURE_TMP" = "yes" || "$SECURE_TMP" = "1" || "$SECURE_TMP" = "YES" || "$SECURE_LEVEL" -ge 2 ]];then
    if [ -d ${HOME}/tmp -a -w ${HOME}/tmp ];then
	export TMPDIR=${HOME}/tmp
	export TMP=${HOME}/tmp
    elif mkdir -p ${HOME}/tmp >/dev/null 2>&1;then
	chmod 700 ${HOME}/tmp
	export TMPDIR=${HOME}/tmp
	export TMP=${HOME}/tmp
    else
	export TMPDIR=/tmp/
	export TMP=/tmp/
    fi
fi
