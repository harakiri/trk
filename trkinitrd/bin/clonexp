#!/bin/bash
#set -x
# See that we need to backup the remote partition bootrecord
NUMARGS=$#
if [ "r$1" = "r--bkupbr" ];  
then
BRBKUP=y
shift 1;
else
BRBKUP=n;
fi;
# Then shift some parameters to know what options are provided
until [ r$nomoreoptions = "r0" ];
do
echo $1 | cut -b 1 | grep -q "\-"
if [ $? = 0 ]; then
nomoreoptions=1
OPTIONS="$OPTIONS $1"
shift 1;
else
nomoreoptions=0
fi;
done;
			
function scripthelp 	{
echo
echo "Usage: $0 <--bkupbr> <ntfscloneoptions> localdevice remotehost:remotedevice"
echo "where  <--bkupbr> is the option whether you want do perform a backup of the original remote bootrecord from that device"
echo "<ntfscloneoptions> are valid options to ntfsclone"
echo "'localdevice' is a valid NTFS partition in the form of e.g. /dev/hda1"
echo "'remotehost:remotedevice' is a valid host or IP-address with a device name divided by a colon."
echo "E.g. 192.168.0.60:/dev/hda1"
			} #End function scripthelp

function startclone 	{
echo
echo "All seems ok, start ntfsclone ..."
ntfsclone $OPTIONS --save-image --output - $INPUTDEV | gzip -c | ssh -o NoneSwitch=yes $RHOST 'gunzip -c | ntfsclone --restore-image --overwrite `cat /tmp/.rhostdev` - '
			} # End function startclone
			
function cppubkey	{
echo
echo "Testing if host is running an ssh server"
nmap -p 22 $RHOST | grep -q "1 host up"
# Check that host is up and we haven 't run succesfully yet
if [ $? = 0 ]; then
	if [ ! -e /tmp/.rsacopied ]; then
echo
echo "Copying public keyfile id_rsa.pub to authorized_keys of $RHOST to avoid password prompts afterwards"
echo "Enter the password for root just this once"
scp /root/.ssh/id_rsa.pub $RHOST:/root/.ssh/authorized_keys && touch /tmp/.rsacopied;
	else echo "Public key already copied";
	fi;
CPPUBKEY=0
else echo && echo "Host isn 't running a ssh server or is down"
CPPUBKEY=1;
fi;
			} # End function cppubkey
			
function testpartition	{
echo $RHOSTDEV > /tmp/.rhostdev
scp /tmp/.rhostdev $RHOST:/tmp > /dev/null
ssh $RHOST fdisk -s `cat /tmp/.rhostdev` > /tmp/.rdevsize
RDEVSIZE=`cat /tmp/.rdevsize`
LDEVSIZE=`fdisk -s $INPUTDEV`
if [ $LDEVSIZE -gt $RDEVSIZE ]; then

echo
echo "Local device size is bigger than remote device size"
echo "Please make the remote device bigger or the local device smaller (use qtparted or something)"
exit 1;
fi; 
# Now test if nothing is mounted anywhere
mkdir /mntrhost
modprobe fuse
mount -t sshfs $RHOST:/ /mntrhost
grep -q $RHOSTDEV /mntrhost/etc/mtab && RMTAB=1
sleep 1
umount /mntrhost
if [ "r$RMTAB" = "r1" ]; 
then 
echo "Remote device is mounted"
echo "You cannot have any filesystem mounted on either TRK host"
exit 1;
fi;
grep -q $INPUTDEV /etc/mtab && LMTAB=1
if [ "r$LMTAB" = "r1" ];
then
echo "Local device is mounted"
echo "You cannot have any filesystem mounted on either TRK host"
exit 1;
fi;

			} # End function testpartition

function testinputdev	{
file -s $INPUTDEV  | grep  -q -i NTFS
if [ "$?" = 0 ];
then FSTEST=0;
else echo && echo "Local device doesn 't contain an NTFS filesystem or is invalid";
FSTEST=1;
fi;
			} # End function testinputdev


function bkupbr		{
if [ r$BRBKUP = "ry" -o r$BRBKUP = "rY" ];
then 
ssh $RHOST 'rbtrecord --backup '
fi;
			} # End function backup boot record on remote device
function putbkupbr		{
if [ r$BRBKUP = "ry" -o r$BRBKUP = "rY" ];
then
ssh $RHOST 'rbtrecord --writetofilesystem'
fi;
			} # This function should write the original bootsector as 
			  # a file on the remote filesystem

			  
function  partitionscp {
BASEINPUTDEV=`echo $INPUTDEV | tr -d [:digit:]`
BASERHOSTDEV=`echo $RHOSTDEV | tr -d [:digit:]`
dd $BASEINPUTDEV | ssh $RHOST dd of=$BASERHOSTDEV bs=512 count=1 \
&& echo "Wrote partition table of local $BASEINPUTDEV to $BASERHOSTDEV on $RHOST"
	} 

if [ $# -lt 1 ]; then
# If only a single command with no arguments is given we start interactively
	
INPUTDEV=qsghswsqfkwhdsh	
echo
echo "No arguments given, entering $0 interactive mode"
echo "This script clones ntfs partitions between computers"
until [ "r$FSTEST" = r0 ];
do
echo
printf "Enter partition from which to copy from (e.g. /dev/hda1): "
read INPUTDEV;
testinputdev;
done;

until [ "r$CPPUBKEY" = r0 ];
do
echo
printf "Enter IP-address to copy partition to of remote TRKhost running a ssh server: "
read RHOST
cppubkey;
done;

echo $RHOSTDEV | grep -q "/dev/"
until [ $? = 0 ];
do
echo
printf "Enter the remote hosts\' device to copy to (e.g. /dev/hda1). Make sure the partition on the remote host exists and is equal to or more than the source filesystem: "
read RHOSTDEV
echo $RHOSTDEV | grep -q /dev/
if [ $? != 0 ];
then  echo && echo "Invalid remote host device format string (duh?)";
fi;
testpartition
done;
# Add some more options
echo
printf "Enter any options you would like to add to ntfsclone (e.g. --rescue to skip bad sectors): "
read OPTIONS
if [ "r$OPTIONS" != "r" ]; then
until [ "r$VALIDOPT" = "r0" ]; do
# Test to see ntfsclone will accept the options. Giving any other error than "unrecognized option"
# is valid to continue
echo $OPTIONS | grep -q -e "-" && ntfsclone $OPTIONS -o /tmp/dsskjf 2>&1 | grep -q "unrecognized option"
if [ $? = 1 -o "r$OPTIONS" = "r" ] ; then VALIDOPT=0; 
else 
echo
printf  "Unrecognized option, enter a valid one or hit return to continue: "
fi;
done;
fi;

echo
echo "Would you like to take a backup of your original partition bootrecord first?"
echo "It will be written to the root folder of your new filesystem as bootrecordbackup.bin"
printf "You can restore your bootrecord on the remote machine with the command bootrecord --restore $RHOSTDEV (y/N) "
read BRBKUP
bkupbr
# All arguments are ok, start the clone process
startclone
putbkupbr;


else
# This is where non interactive portion of the script starts
# First do some argument tests
if [ $NUMARGS -lt 2 ];
then
echo "Last 2 arguments must be sourcedevice destination:destinationdevice"
scripthelp
exit 1;
fi;
echo $2 | grep -q ":"
if [ $? != 0 ];
then
echo "Destination needs a colon separation (:)"
scripthelp
exit 1;
fi;

INPUTDEV=$1
RHOST=`echo $2 | cut -d ":" -f 1`
RHOSTDEV=`echo $2 | cut -d ":" -f 2`

testinputdev
if [ "r$FSTEST" = "r1" ]; then
echo "Local partition doesn 't contain an NTFS filesystem"
scripthelp
exit 1;
fi;

cppubkey
if [ "r$CPPUBKEY" = "r1" ];
then scripthelp
exit 1;
fi;

echo $RHOSTDEV | grep -q "/dev/" 
if [ $? != 0 ];
then  echo "Invalid remote host device format string (duh?)"
scripthelp
exit 1;
fi;
testpartition

# All arguments are ok, start the clone process
bkupbr
startclone
putbkupbr;
#echo "Would you like to write the local MBR to the remote harddisk? (y/n) "
#read CHOICEMBR
#if  [ "r$CHOICEMBR" = "ry" -o "r$CHOICEMBR" = "rY" ]; then
#mbrcp
#fi;
fi;
