#!/bin/bash
#set -x
if [ -e /etc/mountallfstab ];
then

for i in `cat /etc/mountallfstab 2>/dev/null`;
	
# Do a trick: when a drive is not mounted, there should be no problem, but the script would exit anyway, since mount would return an error

do MOUNTED=`umount -f /$i 2>&1 | grep "not mounted" | cut -d ":" -f 3`
	if [ "r$MOUNTED" = "r" -o  "r$MOUNTED" = "r not mounted" ]; 
	then echo Unmounting /$i if mounted; 
	else
	echo "/$i is not so easily unmountable, make sure you don 't have any open files or shells open on it (maybe PWD). Unmount them manually if you have to"  && exit 1;
	fi;

done;

# Unload NTFS module if loaded
NTFSMOD=`lsmod | grep ntfs | awk '// {print $1}'`
if [ r$NTFSMOD != "r" ]; then echo "Unloading ntfs module"; 
rmmod $NTFSMOD;
fi;

if [ $? = 0 ]; then rm -f /etc/mountallfstab;
fi;
else echo "/etc/mountallfstab doesn't exist, nothing todo." && exit 1;
fi;
