��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  <  �  _   �     !     5     M     d  !   �     �     �     �     �  z   	  K   �	     �	     �	     �	     
  .   $
  
   S
     ^
     m
     v
     �
  R   �
  L   �
  _   ?     �  &   �  G   �     +  E   8  9   ~     �     �  +   �  '        7  G   K     �  0   �               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu 1.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-06-26 16:09+0200
Last-Translator: Maxim Dzumanenko <mvd@mylinux.com.ua>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
ПОМИЛКА - для виконання kudzu необхідно мати права root.
 пристрій %s пристрій CD-ROM контролер IDE контролер IEEE1394 контролер PCMCIA/Cardbus контролер RAID контролер SCSI контролер USB %s як псевдонім %s провадити тільки 'безпечні' проби, які не турбують апаратні засоби  файл для читання інформації про пристрої дисковод жорсткий диск версія ядра клавіатура створено посилання %s до %s модем монітор миша мережна карта принтер шукати лише пристрої, для яких зазначено 'class' шукати пристрої лише на вказаній шині('bus') лише пошук, виводити інформацію у стандартний вивід виконано loadkeys %s виконано mouseconfig для %s читати інформацію про пристрої з файлу сканер пошук модулів для окремої версії ядра встановити затримку в секундах звукова карта стриммер налаштовано службу aep1000 ввімкнено службу bcm5820 видалено %s посилання видалено %s (було зв'язано з %s) відео адаптер пристрій захоплення відео 