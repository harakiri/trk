��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  @  �  H   �  
             *     8     K     d     s     �     �  \   �  &   �     &	  
   6	     A	     X	     f	     ~	     �	     �	     �	  	   �	  &   �	  "   �	  9   �	     3
  -   J
  4   x
  
   �
  0   �
  !   �
            %   *  $   P     u  1   �     �     �               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-07-17 12:22+0200
Last-Translator: Arpad Biro <biro_arpad@yahoo.com>
Language-Team: Hungarian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
 
HIBA - A Kudzu futtatásához rendszergazdai jogosultság szükséges.
 %s eszköz CD-ROM-meghajtó IDE-vezérlő IEEE1394-vezérlő PCMCIA/Cardbus-vezérlő RAID-vezérlő SCSI-vezérlő USB-vezérlő %s alias neve: %s csak biztonságos detektálási módok használata (melyek nem állíthatják el a hardvert) hardverinformációt tartalmazó fájl floppymeghajtó merevlemez a kernel verziószáma billentyűzet %s linkelése ehhez: %s modem monitor egér hálózati kártya nyomtató csak a megadott osztály ellenőrzése csak a megadott busz ellenőrzése csak detektálás és információ kiírása az stdout-ra loadkeys %s lefuttatva a mouseconfig program elindítva %s részére a detektált hardvereszközök beolvasása fájlból lapolvasó modulkeresési útvonal egy adott kernel esetén a várakozási idő másodpercben hangkártya szalagos meghajtó az aep1000 szolgáltatás bekapcsolva a bcm5820 szolgáltatás bekapcsolva %s linkelés megszüntetve %s linkelés megszüntetve (eddig ehhez volt: %s) videokártya videófelvevőkártya 