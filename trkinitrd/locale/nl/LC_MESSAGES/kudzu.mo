��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  [  �  7   �  	        "     0     ?     S     m     }     �     �  =   �  &   �     	     +	     8	     F	     R	     h	     n	     v	     {	     �	  (   �	  %   �	  <   �	     
     3
  *   R
     }
  :   �
     �
     �
     �
     �
          '  *   <     g     u               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-03-06 00:12+0100
Last-Translator: Peter van Egdom <p.van.egdom@chello.nl>
Language-Team: Dutch <vertaling@nl.linux.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3
 
FOUT - U moet root zijn om kudzu te kunnen uitvoeren.
 %s device CDROM-station IDE-controller IEEE1394 controller PCMCIA/Cardbus controller RAID-controller SCSI-controller USB-controller alias gemaakt voor %s: %s Doe alleen veilige onderzoeken die de hardware niet verstoren bestand waar de hardware-info op staat diskettestation harde schijf kernel-versie toetsenbord %s is gelinkt naar %s modem monitor muis netwerkkaart printer alleen de opgegeven 'klasse' onderzoeken alleen de opgegeven 'bus' onderzoeken alleen onderzoeken, informatie op de standaard uitvoer tonen loadkeys %s uitgevoerd mouseconfig uitgevoerd voor %s onderzochte hardware van een bestand lezen scanner zoeken naar modules voor een bepaalde versie van de kernel timeout instellen in seconden geluidskaart tapestation aep1000-dienst aangezet bcm5820-dienst aangezet link vrijgemaakt: %s link vrijgemaakt: %s (was gelinkt naar %s) video-adapter video-capture kaart 