��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  6  �  (        /     >     O     [     l     �     �     �     �  ;   �     �     	     *	     6	     C	     O	     j	     p	     x	     }	  	   �	  *   �	  +   �	  4   �	     #
     :
  &   W
  &   ~
     �
  *   �
  &   �
     �
          !     9     Q     _  
        �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu VERSION
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-19 01:46+0100
Last-Translator: Miloslav Trmac <mitr@volny.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8-bit
Report-Msgid-Bugs-To: 
 
CHYBA - Kudzu může spustit jen root.
 %s zařízení mechanika CD-ROM řadič IDE řadič IEEE1394 řadič PCMCIA/Cardbus řadič RAID řadič SCSI řadič USB alias %s jako %s provést jen 'bezpečné' detekce, které neruší hardware soubor s informacemi o hardware disketová jednotka pevný disk verze jádra klávesnice symbolický odkaz %s na %s modem monitor myš síťová karta tiskárna detekovat jen pro specifikovanou 'třídu' detekovat jen na specifikované 'sběrnici' jen detekovat a vytisknout informace na std. výstup spuštěno loadkeys %s spuštěn mouseconfig pro %s číst detekovaný hardware ze souboru číst detekovaný hardware ze socketu scanner hledat moduly pro konkrétní verzi jádra nastavit časový limit ve vteřinách zvuková karta pásková jednotka zapnuta služba aep1000 zapnuta služba bcm5820 odstraněn %s odstraněn %s (byl odkaz na %s) videokarta karta pro zachytávání videa 