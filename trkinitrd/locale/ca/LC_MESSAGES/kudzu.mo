��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  G  �  J        b     p     �     �     �     �     �     �  %   �  <   	  8   \	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   
  %   
  !   9
  =   [
     �
  "   �
  '   �
  +        -  4   6  $   k     �     �     �     �     �  2        :     N        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-22 09:00+0100
Last-Translator: Xavier Conde Rueda <xaviconde@eresmas.com>
Language-Team: Catalan <fedora@softcatala.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
 
S'ha produït un error: heu de ser el superusuari per executar el kudzu.
 dispositiu %s unitat de CD-ROM controladora IDE controladora IEEE1394 controladora PCMCIA/Cardbus controladora RAID Controladora SCSI controladora USB s'ha creat un sobrenom de %s com a %s només fes proves 'segures', que no interrompin el maquinari fitxer del qual es llegirà la informació del maquinari unitat de disquet disc dur versió del nucli teclat s'ha enllaçat %s a %s mòdem monitor ratolí targeta de xarxa impressora només prova la 'classe' especificada només prova el 'bus' especificat només prova, escriu la informació per la sortida estàndard s'ha executat el loadkeys %s s'ha executat mouseconfig per a %s llegeix el maquinari provat d'un fitxer llegeix el maquinari provat des d'un sòcol escàner cerca mòduls per a una versió particular del nucli defineix el temps d'espera en segons targeta de so unitat de cinta s'ha activat el servei aep1000 s'ha activat el servei bcm5820 s'ha esborrat l'enllaç %s s'ha esborrat l'enllaç %s (estava enllaçat a %s) adaptador de vídeo targeta de captura de vídeo 