��    J      l  e   �      P     Q     h     v       
   �     �     �     �     �     �     �     �  
   �     �     �     �               -     A     P     d     w     �     �     �     �     �     �     �     �  
                  )     @     M     Y     g     t     }     �     �     �     �     �     �     �     �     �     �     �     �     �     �     	     	     .	     B	  
   O	     Z	     j	     ~	     �	  
   �	     �	  '   �	     �	  4   �	     %
     ,
     0
     >
  6  J
     �     �     �     �     �     �     �     �     �                 
        (     1     =     L     g     z     �     �     �     �     �     �               3     F  	   b     l     u     �     �     �     �     �     �     �                    $     *     0     C     J     P     X  
   _     j     �     �     �  
   �     �     �               3     D     Q     b     r     v     �  0   �     �  '   �     
       
        !        ;   <               &          :             5   (   4           1       3      0                   8   -              +               F          *      	   )       !   6       /   "           ?   A   %               C   H   @         B          E   I      2       '   G   J   #   7   .           >   
      9      =                           D              $      ,    %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status USB USB Bandwidth USB Version Project-Id-Version: hal
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-07-20 10:21+0200
PO-Revision-Date: 2006-01-13 15:10+0100
Last-Translator: Linas Spraunius <linas@operis.org>
Language-Team: Lithunian <i18n@suse.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s Išorinis standusis diskas %s Standusis diskas %s Laikmena %s Keičiama laikmena %s%s diskasukis /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Juosta:</b> <b>Magistralės tipas:</b> <b>Galimybės:</b> <b>Įrenginio tipas:</b> <b>Įrenginys:</b> <b>OEM produktas:</b> <b>OEM gamintojas:</b> <b>Galios naudojimas:</b> <b>Produktas:</b> <b>Versija:</b> <b>Būsena:</b> <b>USB versija:</b> <b>Gamintojas:</b> Įtraukti įrašą į fstab Papildoma Audio CD Tuščias CD-R Tuščias CD-RW Tuščias DVD+R Tuščias dvisluoksnis DVD+R Tuščias DVD+RW Tuščias DVD-R Tuščias DVD-RAM Tuščias DVD-RW Magistralės tipas CD-R CD-ROM CD-RW DVD+R Dvisluoksnis DVD+R DVD+RW DVD-R DVD-RAM DVD-RW Įrenginys Įrenginių tvarkyklė Įrenginio pavadinimas Įrenginio tipas Įrenginio gamintojas Diskasukis Išorinis %s%s diskasukis Išorinis diskelių įrenginys Išorinis standusis diskas Diskelių įrenginys Standusis diskas Gamintojo ID OEM Gamintojo ID OEM produkto ID PCI Produkto ID Produkto versija Pašalinti visus sugeneruotus įrašus iš fstab Pašalinti įrašą iš fstab Išsamiai informuoti apie vykdymo eigą Būsena USB USB juosta USB versija 