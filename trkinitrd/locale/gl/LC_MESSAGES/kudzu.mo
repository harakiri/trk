��    #      4  /   L        (   	  	   2     <     I     c     s     �     �  1   �     �     �  	                  %     +     3     9     F  $   N     s  '   �     �     �      �          
  
   !  
   ,     7     Q     ]     |     �  E  �  -   �                2     M     ^     o       <   �  -   �       
             '     7     =     E     J  	   Z  *   d  (   �  5   �     �      	  %   &	     L	  &   T	     {	     �	     �	     �	  "   �	     �	     �	                 	                                      #   
                       "       !                                                                     
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner set timeout in seconds sound card tape drive turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2001-03-18 10:42+0100
Last-Translator: Jes�s Bravo �lvarez <jba@pobox.com>
Language-Team: Galician <trasno@ceu.fi.udc.es>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 
ERRO - Debe ser root para executar o kudzu.
 dispositivo %s unidade de CD-ROM controlador PCMCIA/Cardbus controlador RAID controlador SCSI controlador USB f�xose un alias de %s como %s facer s� detecci�ns 'seguras' que non interfiran co hardware ficheiro do que ler a informaci�n do hardware disqueteira disco duro teclado ligouse %s a %s m�dem monitor rato tarxeta de rede impresora analizar unicamente a 'clase' especificada analizar unicamente o 'bus' especificado s� analizar, escribir a informaci�n na sa�da est�ndar executouse loadkeys %s executouse o mouseconfig para %s ler o hardware detectado dun ficheiro esc�ner establecer tempo de espera en segundos tarxeta de son unidade de cinta activado o servicio bcm5820 desligouse %s desligouse %s (estaba ligado a %s) adaptador de v�deo tarxeta de captura de v�deo 