��    N      �  k   �      �     �  (   �     �     �       +   #  #   O     s     �     �     �     �  
   �     �     �     �                /     C     V     j     z     �     �     �     �     �     �  
   �     �     �     	     	     ,	     8	     F	     S	     \	     a	     i	     o	     u	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     *
  
   7
     B
     R
     c
     w
     �
  
   �
     �
  '   �
     �
  4   �
          %     )     -     ;     G  "   X     {  A  �     �  (   �          $     9  .   H  #   w     �     �     �     �     �     �     �                3     F     T     h     �     �     �     �     �     �     �                 	   &  	   0     :  
   K  	   V     `  
   l     w     �     �     �     �     �     �     �     �     �     �     �     �  
   �  
   �     �               )     @     Q     `     i     v     �     �     �  
   �     �  *   �     �  %        8     ?     C     G     W     c  (   v  !   �     &       >       	      )      1      @   4   J      *                K   9       H      =                                         0   .   +   %   8                I       N       "      6       !   /   C                  -         
                   L   ;   5          E   F   3      ?   '   D   G          M   $       7   A                      2       :   ,      #   <         B   (            
 %d: ###################################
 %d: %s entering; %s udi=%s
 %d: Acquiring advisory lock on  %d: Lock acquired
 %d: couldn't open %s O_RDONLY; bailing out
 %d: is_generated=%d, is_mounted=%d
 %d: mount_root='%s'
 %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM  CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-ROM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID Max 	Power Drain OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status UDI USB USB Bandwidth USB Version _Virtual Devices removed all generated mount points removed mount point %s for %s Project-Id-Version: HAL 0.5.x
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-10-10 09:13+0200
PO-Revision-Date: 2005-10-10 09:18+0200
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team:  <i18n-nb@lister.ping.uio.no>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
 %d: ###################################
 %d: %s går inn i; %s udi=%s
 %d: Henter lås på  %d: Lås tatt
 %d: kunne ikke åpne %s O_RDONLY; stopper her
 %d: is_generated=%d, is_mounted=%d
 %d: mount_root=«%s»
 %s ekstern harddisk %s harddisk %s medie %s avtagbart medie %s%s-stasjon <b>Båndbredde:</b> <b>Busstype:</b> <b>Funksjoner:</b> <b>Enhetstype:</b> <b>Enhet:</b> <b>OEM-produkt:</b> <b>OEM-leverandør:</b> <b>Strømforbruk:</b> <b>Produkt:</b> <b>Revisjon:</b> <b>Status:</b> <b>USB-versjon:</b> <b>Leverandør:</b> Legg til en oppføring i fstab Avansert Lyd-CD Tom CD-R Tom CD-RW Tom DVD+R Tom DVD+R tolags Tom DVD+RW Tom DVD-R Tom DVD-RAM Tom DVD-RW Busstype CD-R CD-ROM  CD-RW DVD+R DVD+R tolags DVD+RW DVD-R DVD-RAM DVD-ROM DVD-RW Enhet Enhetshåndterer Enhetsnavn Enhetstype Leverandør av ehnet Stasjon Ekstern %s%s-stasjon Ekstern diskettstasjon Ekstern harddisk Diskettstasjon Harddisk Produsent-ID Maks. 	strømforbruk OEM-produsent-ID OEM-produkt-ID PCI Produkt-ID Produktversjon Fjern alle generete oppføringer fra fstab Fjern en oppføring fra fstab Gi detaljert informasjon om forløpet Status UDI USB USB-båndbredde USB-versjon _Virtuelle enheter fjernet alle genererte monteringspunkter fjernet monteringspunkt %s for %s 