��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  ?  �  4        D     S     a     o     �     �     �     �     �  S   �  *   /	     Z	     k	     w	  
   �	     �	     �	     �	     �	     �	     �	  ,   �	  &   �	  +   #
     O
     g
  0   �
  0   �
     �
  *   �
  "        =     O     _     {     �  %   �     �     �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: pl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-24 14:51+0100
Last-Translator: Tom Berner <tom@lodz.pl>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
 
BŁĄD - musisz być rootem, aby uruchomić kudzu.
 urządzenie %s napęd CD-ROM kontroler IDE kontroler IEEE1394 kontroler PCMCIA/Cardbus kontroler RAID Kontroler SCSI kontroler USB %s aliasowany do %s wykonaj jedynie 'bezpieczne' testy które nie spowodują zaburzeń w pracy sprzętu plik do czytania z informacją o sprzęcie napęd dyskietek twardy dysk wersja jądra klawiatura dowiązano %s do %s modem monitor mysz karta sieciowa drukarka szukaj tylko w określonej klasie urządzeń szukaj tylko na określonej magistrali tylko szukaj, wyślij informację na stdout uruchomiono loadkeys %s uruchomiono mouseconfig dla %s listę wyszukiwanych urządzeń odczytaj z pliku listę wyszukiwanych urządzeń odczytaj z pliku skaner szukaj modułów dla podanej wersji jądra ustaw czas oczekiwania w sekundach karta dźwiękowa napęd taśmowy włączono usługę aep1000 włączono usługę bcm5820 odwiązano %s odwiązano %s (był dowiązany do %s) karta graficzna karta przechwytująca wideo 