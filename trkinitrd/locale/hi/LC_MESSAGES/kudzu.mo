��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  Y  �     )     �     �     �  !   �  '   	     :	     X	     v	  U   �	  �   �	  c   p
     �
     �
  %        :  7   S     �     �     �     �     �  X   �  >   G  g   �  >   �  0   -  Y   ^  Y   �       �   %  S   �              (   7  (   `  ;   �  i   �  (   /  5   X        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: hi
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-03-28 19:04+0530
Last-Translator: Rajesh Ranjan <rranjan@redhat.com>
Language-Team: Hindi <fedora-trans-hi@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.9.1
 
दोष - कुडज़ू को सिर्फ रूट होकर ही आप चला सकते हैं. 
 %s युक्ति CD-ROM चालक IDE नियंत्रक IEEE1394 नियंत्रक PCMCIA/Cardbus नियंत्रक RAID नियंत्रक SCSI नियंत्रक USB नियंत्रक %s को %s के रूप में उपनामित किया गया  सिर्फ वो 'सुरक्षित' खोज करें जो हार्डवेयर को न छेड़े हार्डवेयर की जानकारी पढने के लिए फाइल फ्लोपी चालक हार्ड डिस्क कर्नल संस्करण कुंजीपटल %s को %s से लिंक किया गया मोडेम मॉनिटर माउस संजाल कार्ड मुद्रक सिर्फ विशिष्ट 'क्लास' के लिए खोजें सिर्फ विशिष्ट 'बस' खोजें सिर्फ खोजें, stdout को जानकारी मुद्रित करें %s भारणकुंजियों को चलाया  %s के लिए mouseconfig चलाया फाइल से खोजी हुई हार्डवेयर जांचें सॉकेट से जांचा गया हार्डवेयर पढ़ा स्कैनर एक प्रत्येक कर्नल संस्करण के लिए मोड्यूल को जाँचें समय-समाप्ति को सेकण्ड में जमाएँ ध्वनी कार्ड टेप चालक aep1000 सेवा चालू की bcm5820 सेवा चालू की %s विश्रृंखलित किया गया %s विश्रृंखलित किया गया(%s से श्रृंखलित था) विडियो एडाप्टर विडियो कैप्चर कार्ड 