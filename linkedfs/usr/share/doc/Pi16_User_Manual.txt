﻿Pi  revision 1.6
 

A. Overview
 
The pi script was designed for:

• make possible a fully automated backup system.
 
• facilitate the use of partimage

Two operations are available:

 
1. Automatic mode
 
To enable automatic operation, you must file a pi.cfg in the root directory of a partition of the machine (all mountable partitions are scanned) and the script must be  launched with option -c 
If the file is found, the parameters are loaded and the backup is started. This backup will be excuted without any user intervention if AUTO has been set to 1. If this is not the case, there will be a confirmation request.
If you add the command pi -c to your trklocscript file in the trk3 directory, then you can have a CD or USB key which will backup the partitions you have defined without any intervention of the operator : just boot on the CD or the USB key, and you are done. This is handy if you want users who don't know anything about computers to be able to save their system disk from time to time. 
 

2. Interactive mode
 
If the pi script is run without options, or if there is no pi.cfg file in the root directory of any disk in the machine, pi is launched interactively: the user will have to select:
 
- The unit or units to save.
- The destination disk that store the backup (local drive or network) 
- The destination directory
 

Pi handles the number of copies of the image file to keep. With high capacity hard disks, you may want to keep older images. Use the -k parameter (see details below) to define how many copies of your partitions pi will keep on your disk.

On the other hand, if you want to make a copy on an USB key and you have just the space for one copy, the -m parameter allows you to control the space needed by pi : in normal operation, you need up to three times the space of the image. If it is too much, you can reduce to 2 times the space, or just the space of the image. See details below. 

B. Launch of the script
 
Type pi then <RETURN> to launch interactive mode.
Five command line options are available:
 
    -c: search a pi.cfg configuration file and loads it if found. If this file contains the parameter AUTO = 1, the backup will run automatically.
 
    -a : Starts the backup automatically (same as AUTO = 1 in pi.cfg)
 
    -k # : This option (keep) sets the number of copies of the image to be kept. See details in Section “F: Old backups” below
 
    -m # (0, 1, 2, default 2): handles the number of total copies during process. 

    -d: debug mode. The backup will not be actually launched and the command that would have launched partimage is only displayed on the screen
 
Trick: If you execute pi with –c and –d parameters, you will see what partition to save and which destination are selected from the found pi.cfg file. 


C. CONFIGURATION FILES
 
1. pi.cfg file
 
CAUTION: In all that follows names are case-sensitive because we are on a Linux system. 
The name of the configuration file is pi.cfg and not Pi.cfg or PI.cfg or PI.CFG: ALL THESE SPELLINGS ARE DIFFERENT ! Only pi.cfg (lowercase) will be accepted.
 
Spaces are not allowed in file names or directory (replace them by _ or -)
 
The trk3 directory contains a pi.cfg sample file which contains:
 
___________________________________________________________________

# No space before and after the = sign 
# The names are case-sensitive 
# None backslashs: \, only slashs/ 

# Destination backup unit. Maybe a network drive, for example: BACKUPDRIVE =//192.168.1.4/sysbackup 
# In this case the parameters LOGIN and PASSWORD are required 
# Or a local unit such as BACKUPDRIVE=/sdb1 
BACKUPDRIVE=//192.168.1.4/sysbackup 
LOGIN=sysbackup 
PASSWORD=sysbackup 

# Backup directory backup 
DEST=Lawrence 

# Beginning of backup files names (will be followed by - and the name of the unit to backup
# For example “-hda1.000” 
FILE=part 

# Partitions to backup 
# If there are multiple partitions, separate them by spaces, and put it between quotation marks 
# Example PARTITION = "sda1 sda5" 
PARTITION="sda1" 

# If Auto = 1 and a pi.cfg configuration file was found, the backup is done without any user intervention 
AUTO=1 
___________________________________________________________________


Remarks: 

Always use the forward slash and not the backslash: we are in Linux and not in Windows. Long before Microsoft existed, the backslash was an escape character on Unix. Only Microsoft's commitment to be "like no other" has led to ignore this meaning and use the backslash in directories' path. Linux meets Unix standards. 
There are never spaces before or after the equal sign under penalty of non-functioning of the script .
The parameters' names can be written in uppercase or lowercase, but you cannot mix the case : BACKUPDRIVE and backupdrive are OK. BackupDrive will not work.. 

BACKUPDRIVE: 
May be a network drive, and in this case it will be also mandatory to specify LOGIN and PASSWORD. The format will be:
 //xxx.xxx.xxx.xxx/shared_directory_name. 
This can also be a local drive and in this case the format will be, for instance: /dev/sda1.
It is prudent to use the complete device names returned by info on partitions. /sda1 should work too, but /dev/sda1 is safer.
 

DEST 
This is the destination directory to be used (and possibly created) in the unit selected by BACKUPDRIVE. It is possible to select a subdirectory, but in this case it is mandatory that the directory already exists. Example:

DEST=backup/system/unite_c
 
will work if and only if backup/system already exists. The last sub-directory can be created by the script, but not the previous ones. 


FILE
It's just the beginning of the file names that will be created. The final name is built as following:

<FILE> - <partition> .000
 
If we kept the default value (FILE=part) and if the saved partitions are hda1 and hda5, the following files are created:
 
part-hda1.000 
part-hda5.000 


PARTITION 
This parameter contains the names of the partitions to backup. If there is one partition, you can simply specify it as follows:
 
PARTITION=hda1
 
If there are multiple partitions, they must be separated by spaces and put  between quotation marks:
 
PARTITION="hda1 hda5"
 
It is allowed to have spaces between quotation marks but it is prohibited outside. PARTITION= "hda1 hda5" will not work because you should never have a space before and after the = sign
 
Note the different syntax from BACKUPDRIVE. There's no slash or /dev/. 

AUTO 
AUTO=1 enables automatic backup and a delay is placed on the confirmation messages. It is possible to interrupt the process by pressing any key other than SPACE or ENTER during the period. After the delay and without user intervention, backup is started.


2. FILE pi.ini
 
This file is optional and is located in the directory /trk3/share/pi. Its essential purpose is to allow the addition of units of destination to interactive mode. It incidentally allows to define default settings. It is as follows:
 
________________________________________________________________
 
drive[1]=//192.168.1.4/sysbackup 
login[1]=sysbackup 
password[1]=sysbackup 

drive[2]=//obelix/sysbackup 
login[2]=asterisk 
password[2]=aplusbegalix 

drive[3]=/sda2/backupSysteme/2010 
___________________________________________________________________

The above file would define three new options for the choice menu of destination: 

Two network drives starting with // and for which we must provide a username and password.

A local unit which is defined along with a sub-directory.

Note the parameters names: lower case (except the D of Drive), and followed by a bracketed number that determines the number of the option.

Incidentally it is possible to add in this file parameters identical to those of pi.cfg, they will then become the default values of the program. To be used with caution because it will prevent the presentation of some menus, the menus beeing only displayed when there is no defined value.
 

D. INTERACTIVE MODE
 
Launched without the -c option, or if no valid configuration file has been found, pi starts in interactive mode. If a configuration file has been found but not all options necessary are set, the interactive mode will be started and the script will ask for the missing options. Three successive menus query the user to ask:
 
a) The partitions to save
 
Here you can select a partition in the list of partitions found by the system, or select "information on partitions" for more information on your partitions, or select "select several partitions”. In this case you must type the names of the partitions to save as they appear in the list, separated by spaces. For example:
 
hda1 sda1
 
b) The destination drive (the one on which the backup will be saved).
 
A list presents successively the units defined in pi.ini and the units that the system finds on local disks.
After selecting the unit, the script will attempt to mount the disk. If it fails, an error message is sent and the script stops. Possible causes include, among others: 

1. You have chosen an NTFS partition and Windows has been put in hibernate state on the disk. In this case, NTFS partitions are not closed and they cannot be mounted rread/write, you can only read. It is possible to make a backup of such partition, but you cannot use it as the destination unit. To avoid this error, completely shut down Windows before starting TRK Autobackup.
Since TRK revision 3.4 : hibernated partition cannot be written, but the state of other not closed partitions is normally solved by ntfs-3g which is able to successfully mount them for writing. However it is always better to properly shut down Windows before making a backup.
 
2. The system also put in the list extended partitions, which can disturb. An extended partition is a mere container and cannot receive files. For information on how your partitions are made, use the "information about partitions" option of the main menu.	 
3. You have selected a network drive and the network connection is not good, or your LOGIN and PASSWORD parameters are incorrect.
 
c) The destination directory
 
In the destination device previously defined, the script can either use the existing directories that are presented here, or a new directory: in this case select the last option and type a directory name without spaces in its name. We are in Linux which does not accept spaces in the names of directories and files.
Then the script will present you a summary of your choices that you can confirm or reject by y(es). or n(o). In the last case the script closes.
Finally before running the backup a last message information is presented. You can again interrupt the process by pressing any key other than SPACE or ENTER, or type on ENTER to immediately start the backup, or let the script look after himself. After 15 seconds, if you do nothing, it will launch the backup.


E. BACKUP SCREEN
 
During the backup, partimage shows a screen that displays the progress of the operation. Despite the message that is displayed at the bottom of the screen, it is unfortunately not possible to interrupt a backup because the * option does not work. This is a bug in partimage. If you want to stop anyway, you can do a ctrl+alt+del. You will just need to think about delete the created files.
 
F. OLD BACKUPS
 
Normally the script does not delete the last backup you made, it creates a directory named "old" in which he retains that backup (which is now the penultimate). The process is as follows: 

a) creation of the new image in temporary files.
b) If it succeeds, the image which is in the "old" directory is deleted if it exists.
c) the old image found in the normal directory is transferred into "old" sub-directory.
d) the new image files are renamed.
e) unless this is your first backup of this partition, there are two copies of the image, one in the destination directory, the other in the "old" sub-directory.

If you have a very large disk for your destination, which is common nowadays with 1T or more disks, you may want to keep more than two copies of your image. Use the -k parameter for that : -k 4 will keep four copies. The behaviour is the following :


a)the current backup is performed into a temporary file.
b)If not already existing, a sub-directory is created with the following name: bk_saved_partition_YYYY-MM-DD, for example: bk_hda2_2010-05-17 and the already existing backup file of this partition (from a previous backup) is transferred under this sub-directory.
c)the temporary backup file is renamed with its final partition backup filename.
d)depending of the number specified with the –k option old backups are deleted: for example, if user has specified pi –c –k 4 and there are already 3 existing bk_saved_partition_YYYY-MM-DD sub-directories, the oldest one will be automatically removed once the current backup will have successfully completed.


G. IN CASE OF LOW SPACE ON THE DESTINATION 
using –m # option of pi
 
In the normal process described above, there is a moment where you have three images of your partition : one that is currently on temporary files, one which is in the normal directory and one which is in the "old" directory. If you do not have enough space on your disk for this, use the -m option to limit the number of copies. This option receives a mandatory parameter that can be 0, 1 or 2. Any value greater than 2 will be treated as 2 which is the default.
 
0: If you have just the place for an image, this parameter will give the following behavior: 
a) erase the existing image
b) creation of the new image
Warning: this mode is not secure because if the creation of the new image fails, the old image has already been erased.
 
1: If you have enough disc space for two copies of the image but want to keep only the last, this parameter will cause the following behaviour:
a) creation of the new image in temporary files
b) if the image was successful, erase the old image
c) the new image files are renamed.
If the image had failed, temporary files remain on the disk. They will be crushed by the new image when you will try again to make one. You can also delete them yourself.
 
2: Normal mode already described above:
a) creation of the new image in temporary files.
b) If it succeeds, the image which is in the "old" directory is deleted if it exists.
c) the old image found in the normal directory is transferred into "old" sub-directory.
d) the new image files are renamed.
e) there are two copies of the image, one in the destination directory, the other in the "old" sub-directory.



TODO :  expliquer l'ajout de paramètres par défaut dans pi.ini
modifier pi pour permettre la syntaxe upper / lower dans ce fichier

