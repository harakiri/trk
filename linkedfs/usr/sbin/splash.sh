#!/bin/bash
#
# splash.sh - script to paint progress bar during
# system startup/shutdown. This script is solely run
# by the init scripts (shell function rc_splash)
#
# (w) 2002-2003 Stefan Reinauer, <stepan@suse.de>
# 2003 Florent Villard, <warly@mandrakesoft.com>
# It's licensed under GPL, of course.
#
# this script expects the following environment variables:
#  nbservices = number of start/stop scripts to be executed for runlevel change
#  progress = number of currently executed start/stop script
#  runlevel = runlevel to be reached.

_procsplash="`cat /proc/splash 2>/dev/null`"
TEXTDOMAIN=bootsplash
TEXTDOMAINDIR=/etc/locale 
LOCPATH=/etc/locale

# execute splash binary utility because we are a wrapper:
if [ "$1" == "-s" -o "$1" == "-u" -o "$1" == "-n" -o "$1" == "-f" ]; then
    exec /sbin/splash $*
else
   ( exec /sbin/splash "$*" )
fi

gprintf() {
    # request translations to be always in UTF-8
        if [ -x /bin/gettext -a -n "$1" ]; then
	if [ -n "$GP_LANG" ]; then
	    TEXT=`OUTPUT_CHARSET=UTF-8 LANGUAGE=$GP_LANG LC_ALL=$GP_LANG gettext -e --domain=$TEXTDOMAIN "$1"`
	else
	    TEXT=`OUTPUT_CHARSET=UTF-8 gettext -e --domain=$TEXTDOMAIN "$1"`
	fi
    else
	TEXT=$1
    fi
    [ "${1#*\\n}" ] || TEXT="$TEXT\n"
}

# assertions
test -r /proc/splash || exit 0
test -z "`echo $_procsplash|grep on`" && exit 0

if [[ -z $res ]]; then 
    res=`fbresolution`
    if [[ -f /etc/sysconfig/bootsplash ]]; then
        . /etc/sysconfig/bootsplash
	theme=$THEME
	tmpval=$LOGO_CONSOLE
	if [[ -f /etc/bootsplash/themes/$theme/config/bootsplash-$res.cfg ]]; then
	    function box() { true; }
            . /etc/bootsplash/themes/$theme/config/bootsplash-$res.cfg
        fi
	if [[ $tmpval != "theme" ]];then
	    LOGO_CONSOLE=$tmpval
	fi
    fi
fi

_shutdown="no"
_silent="no"
grep -q silent /proc/cmdline && grep -q silent /proc/splash && _silent="yes"
test "$runlevel" == "6" -o "$runlevel" == "0" && _shutdown="yes"

if [ "$1" == "verbose" ]; then
    echo 'verbose' > /proc/splash
fi

if [ "$1" == "stop" -a $_shutdown == "no" ]; then
    if [[ $LOGO_CONSOLE == No ]] || [[ $LOGO_CONSOLE == no ]] || [[ $LOGO_CONSOLE == NO ]]; then                                                        
	if [[ -e /proc/splash ]];then
	    echo 0 > /proc/splash
	    exit 0
	fi 
    else 
	if [[ $LOGO_CONSOLE == vt ]]; then
	    for i in 0 1 2 3 4 5
	    do
		if [[ -f /etc/bootsplash/themes/$theme/config/vt$i-$res.cfg ]]; then
		    /sbin/splash -s -u $i /etc/bootsplash/themes/$theme/config/vt$i-$res.cfg
		fi
	    done
	fi
    fi
fi

test -z "$progress" -a -z "$nbservices" && exit 0
grep -vq "splash=silent" /proc/cmdline && exit 0 

if [ "$previous" == "3" -o "$previous" == "5" ] ; then
  if [ "$runlevel"  = "3" -o "$runlevel" == "5" ] ; then
    exit 0
  fi
fi

# acquire data
# 
num=$(( $nbservices + 2 ))

# Initialize and print text string..
if [ "$progress" == 1 -o "$1" == "start" ]; then
    # depending on the languages, we need to use a font different of
    # the default one; in such case the font must be accessible, or
    # the translation is disabled.
    # bugs:
    # - RTL languages need a fake *.po file, where bidi reordering and
    #   shaping is already done.
    # - indic languages have not shaping
    # - some indic fonts don't have ascii glyphs; so the corresponding
    #   languages have to be disabled (I added a '_' in front) until they are
    #   fully translated.
    case "${GP_LANG}${LANGUAGE}" in
	# Azeri needs the schwa, which is not yet in Nimbus Sans L
	az*)
		font=/usr/share/fonts/otf/mdk/mdk_teams.otf ;;
	# languages written in latin that use letters not included in luxi
	cy*|gn*|vi*)
		font=/usr/share/fonts/default/Type1/n019023l.pfb ;;
	# basic cyrillic, covered by cp1251
	be*|bg*|mk*|ru*|sr*|uk*)
		font=/usr/share/fonts/default/Type1/n019023l.pfb ;;
	# extended cyrillic, not covered by cp1251
	ky*|mn*|tg*|uz*)
		font=/usr/share/fonts/default/Type1/n019023l.pfb ;;
	# other scripts
	am*) font=/usr/share/fonts/ttf/ethiopic/gfzemenu.ttf ;;
	ber*) font=/usr/share/fonts/ttf/tifinagh/hapaxber.ttf ;;
	ar*) font=/usr/share/fonts/ttf/arabic/Kacst-Qr.ttf ;;
	fa*) font=/usr/share/fonts/ttf/arabic/farsi/nazli.ttf ;;
	ur*) font=/usr/share/fonts/ttf/arabic/nastaliq_unicode.ttf ;;
	as*|bn*) font=/usr/share/fonts/ttf/bengali/MuktiNarrow.ttf ;;
	el*) font=/usr/share/fonts/type1/greek/Kerkis-Italic.pfb ;;
	# this font doesn't has ascii chars
	_gu*) font=/usr/share/fonts/ttf/gujarati/padmaa-Bold-0.5.ttf ;;
	he*|yi*) font=/usr/share/fonts/type1/hebrew/NachlieliCLM-Bold.pfb ;;
	# this font doesn't has ascii chars
	hi*|_mr*) font=/usr/share/fonts/otf/mdk/raghu.ttf ;;
	hy*) font=/usr/share/fonts/ttf/armenian/artsnk_b.ttf ;;
	ja*) font=/usr/share/fonts/ttf/japanese/sazanami-gothic.ttf ;;
	# this font doesn't has ascii chars
	_kn*) font=/usr/share/fonts/ttf/kannada/Sampige.ttf ;;
	ko*) font=/usr/share/fonts/ttf/korean/gulim.ttf ;;
	# we don't ship any ttf font for Lao yet
	_lo*) font=/usr/share/fonts/ttf/lao/xxxx.ttf ;;
	ml*) font=/usr/share/fonts/otf/mdk/malayalam.ttf ;;
	# we don't ship any gurmukhi font yet
	_pa_IN*) font=/usr/share/fonts/ttf/gurmukhi/xxxxx.ttf ;;
	# using tscii font as currently there isn't shaping
	ta*) font=/usr/share/fonts/ttf/tscii/tscava.ttf ;;
	th*) font=/usr/share/fonts/ttf/thai/norasi_b.ttf ;;
	zh*) font=/usr/share/fonts/ttf/chinese/fireflysung.ttf ;;
	# languages supported by the default font; latin1
	af*|br*|ca*|da*|de*|es*|et*|eu*|fi*|fr*|fur*|ga*|gl*|id*|is*|it*|li*)
		font=default ;;
	# languages supported by the default font; latin1, more
	ms*|nb*|nds*|nl*|nn*|no*|oc*|ph*|pt*|sc*|sv*|tl*|wa*)
		font=default ;;
	# languages supported by the default font; latin2
	bs*|cs*|hr*|hu*|pl*|ro*|sk*|sl*|sq*|tk*)
		font=default ;;
	# languages supported by the default font; other latin coverages
	eo*|ku*|lt*|lv*|mt*|tr*)
		font=default ;;
	# for others, we do a last chance possibility with
	# Code2000, if the user installed it...
	*) font=/usr/X11R6/lib/X11/fonts/drakfont/ttf/code2000.ttf ;;
    esac

    if [ "$font" != "default" ]; then
    	if [ -r "$font" ]; then
		custom_font="-f $font"
	elif [ -r "/etc/locale/`basename $font`" ]; then
		custom_font="-f /etc/locale/`basename $font`"
	elif [ -r "/etc/locale/default.ttf" ]; then
		custom_font="-f /etc/locale/default.ttf"
	elif [ -r "/usr/X11R6/lib/X11/fonts/drakfont/ttf/code2000.ttf" ]; then
		custom_font="-f usr/X11R6/lib/X11/fonts/drakfont/ttf/code2000.ttf"
	elif [ -r "/usr/X11R6/lib/X11/fonts/drakfont/ttf/arialuni.ttf" ]; then
		custom_font="-f usr/X11R6/lib/X11/fonts/drakfont/ttf/arialuni.ttf"
	else
		# no font available, translations are not done
		GP_LANG=C ;
		LANGUAGE=C ;
	fi

	# some fonts need a bigger size to have a nice display
	case "$custom_font" in
	*norasi_b.ttf) text_size=$(( $text_size + 6 )) ;;
	*fireflysung.ttf) text_size=$(( $text_size + 6 )) ;;
	*Kacst-Qr.ttf|*nazli.ttf|*nastaliq_unicode.ttf) text_size=$(( $text_size + 6 )) ;;
	# sizes lower than 22 display as noise in FB
	*sazanami-gothic.ttf) if [ "$text_size" -lt "22" ]; then text_size=22 ; fi ;;
	esac 
    fi

    if test "$_shutdown" == "yes"; then 
        gprintf "Shutting down the system..."
        [[ -f /etc/bootsplash/themes/$theme/config/bootsplash-$res.cfg ]] && /sbin/splash -s -u 0 /etc/bootsplash/themes/$theme/config/bootsplash-$res.cfg
        echo "silent" >/proc/splash
        chvt 1
    else 
	if [ "$font" != "default" -a "$progress" != "1" -a -d /usr/share/bootsplash -a "$_silent" == "yes" ]; then
	    if [[ -f /etc/bootsplash/themes/$theme/config/bootsplash-$res.cfg ]]; then
		/sbin/splash -s -u 0 /etc/bootsplash/themes/$theme/config/bootsplash-$res.cfg
		echo "show $(( 65534 * ( $progress + 1 ) / $num ))" > /proc/splash
	    fi
	fi
        gprintf "Booting the system..."
    fi
    _boot=$TEXT
    if [ "$text_x" != "" -a "$text_y" != "" \
        -a "$text_color" != "" -a "$text_size" != "" -a "$_silent" == "yes" -o "$_shutdown" == "yes" ]; then
        gprintf "Press Esc for verbose mode."
        fbtruetype.static -x $text_x -y $text_y -t $text_color -s $text_size \
  			$custom_font "$_boot $TEXT"
    fi
fi

# Paint progressbar..
test -z "$progress_enable" && exit 0
echo "show $(( 65534 * ( $progress + 1 ) / $num ))" > /proc/splash
