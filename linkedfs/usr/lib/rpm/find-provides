#!/bin/bash

# This script reads filenames from STDIN and outputs any relevant provides
# information that needs to be included in the package.

filelist=`sed "s/['\"]/\\\&/g"`

solist=$(echo "$filelist" | grep -v "^/lib/ld\.so" | egrep '(/usr(/X11R6)?)?/lib(64)?/.*\.so' | \
	xargs file -L 2>/dev/null | grep "ELF.*shared object" | cut -d: -f1)
pythonlist=
tcllist=

#
# --- Alpha does not mark 64bit dependencies
case `uname -m` in
  alpha*)	mark64="" ;;
  *)		mark64="()(64bit)" ;;
esac

#
# --- Library sonames and weak symbol versions (from glibc).
for f in $solist; do
    soname=$(objdump -p $f 2>/dev/null | awk '/SONAME/ {print $2}')

    lib64=`if file -L $f 2>/dev/null | \
	grep "ELF 64-bit" >/dev/null; then echo "$mark64"; fi`
    if [ "$soname" != "" ]; then
	if [ ! -L $f ]; then
	    echo $soname$lib64
	    objdump -p $f 2>/dev/null | awk '
		BEGIN { START=0 ; }
		/Version definitions:/ { START=1; }
		/^[0-9]/ && (START==1) { print $4; }
		/^$/ { START=0; }
	    ' | \
		grep -v $soname | \
		while read symbol ; do
		    echo "$soname($symbol)`echo $lib64 | sed 's/()//'`"
		done
	fi
    else
	echo ${f##*/}$lib64
    fi
done | sort -u

#
# --- Perl modules.
[ -x /usr/lib/rpm/perl.prov ] &&
    echo "$filelist" | tr '[:blank:]' \\n | /usr/lib/rpm/perl.prov | grep 'perl([A-Z]' | sort -u

#
# --- Python modules.
[ -x /usr/lib/rpm/python.prov -a -n "$pythonlist" ] &&
    echo "$pythonlist" | tr '[:blank:]' \\n | /usr/lib/rpm/python.prov | sort -u

#
# --- Tcl modules.
[ -x /usr/lib/rpm/tcl.prov -a -n "$tcllist" ] &&
    echo "$tcllist" | tr '[:blank:]' \\n | /usr/lib/rpm/tcl.prov | sort -u

#
# --- .so files.
for i in `echo $filelist | tr '[:blank:]' "\n" | egrep '(/usr(/X11R6)?)?/lib(|64)(/gcc(-lib)?/.+)?/[^/]+\.so$'`; do
    objd=`objdump -p ${i} | grep SONAME`
    [ -h ${i} -a -n "${objd}" ] && \
    lib64=`if file -L $i 2>/dev/null | grep "ELF 64-bit" >/dev/null; then echo "(64bit)"; fi` && \
    echo ${objd} | perl -p -e "s/.*SONAME\s+(\S+)\.so.*/devel(\1$lib64)/g"
done | sort -u

#
# --- mono provides
if [ -x /usr/bin/mono-find-provides ]; then
echo $filelist | tr '[:blank:]' '\n' | /usr/bin/mono-find-provides
fi

exit 0
