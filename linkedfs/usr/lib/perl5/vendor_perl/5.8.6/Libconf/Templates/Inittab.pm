
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Inittab;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
use Libconf::Templates::Generic::KeyValues;
@ISA = qw(Libconf::Templates::Generic::KeyValues);

=head1 NAME

Libconf::Templates::Inittab - Libconf low level template for inittab file styles config files

=head1 DESCRIPTION

Libconf::Templates::Passwd is a template that handles files that contain informations like the ones in /etc/inittab, with lines like :

 id:runlevels:action:process

=head1 SYNOPSIS

 $template = new Libconf::Templates::Inittab({
                                              filename => '/etc/inittab',
                                             });
 $template->read_conf();
 $template->edit_atom(-1, { key => 'l4',
                            values => {  runlevels => '45',
                                         action => 'once',
                                         process => '/etc/rc.d/rc 5',
                                      },
                          });
 ...
 (see L<Libconf::Templates> for transformation methods on $template)
 ...
 $template->write_conf();

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Inittab({
                                               filename => 'some_file',
                                             })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new({
                                   filename => $args->{filename},
                                   regexp => '^\s*([^:]{1,4}?):([^:]*):([^:]*):([^:]*)\s*$',
                                   keys_list => [qw(id runlevels action process)],
                                   identifier_key => 'id',
				   comments_struct => [['#']],
                                   output_function => sub { my $atom = shift; join(':', map { $atom->{values}{$_} } @_ ) },
                                  });
    $self->{template_name} = 'Inittab';
    bless $self, $class;
}

1
