
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Generic::KeyValueSections;
use strict;
use vars qw(@ISA);
use Libconf qw(:helpers);
use Libconf::Templates;
use Libconf::Templates::Generic::KeyValue;
use Libconf::Templates::Generic::Sections;
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::KeyValueSections - Libconf low level template for semantic (KEY - VALUE) and SECTION

=head1 DESCRIPTION

Libconf::Templates::KeyValueSections - Libconf low level template for semantic (KEY - VALUE) and SECTION

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

 my $template = new Libconf::Templates::KeyValueSections({
                                                           filename => '/etc/samba/smb.conf'
                                                           separator_char => '=',
                                                           allow_space => 1,
                                                           handle_quote => 0,
                                                           handle_multiples_lines => 0,
                                                           section_regexp => '^\s*\[([^\]]+)\]\s*$',
                                                           section_output_function => sub { "[$_[0]]" },
                                                           has_endsection => 0,
                                                           endsection_regexp => '',
                                                           endsection_output_function => '',
                                                         });

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

comments_struct [B<type> : ARRAY_REF of ARRAY_REF of STRING,STRING,SCALAR,
B<default> : [['#']] ] : defines the type and the characters of comments. See
L<Libconf::Templates::Keyvalue> for additional details on it

handle_quote [B<type> : SCALAR, B<default> : false] : if true, quotes are
interpreted and removed from the value. When writing to the config file back,
quotes are added if needed. if false, quotes are not interpreted, and present
in the value.

handle_multiples_lines [B<type> : BOOLEAN, B<default> : false] : if true, line
that begins with a space character and that doesn't contain the separator_char,
are taken as the continuation of the upper line

accept_empty_value [B<type> : BOOLEAN, B<default> : true] : if true, lines
where the value is not specified (like ``key='') are accepted, and the value is
set to empty string. If set to false, such a line will trigger an error.

separator_char [B<type> : STRING, B<default> : '='] : the separator between key and value

allow_spaces [B<type> : SCALAR, B<default> : 1] : if true, space around
B<separator_char> are allowed and ignored if false, they are not allowed.

section_regexp [B<type> : STRING, B<default> : '^\s*\[([^\]]+)\]\s*$'] : the
regexp that is applied to determine if a line is a section start. $1 has to be
the section name.

section_output_function [B<type> : FUNCTION_REF, B<default> : sub { "[$_->[0]]"
} ] : the code to be applied to a section atom, to generate the line to output.
It takes in arguments the name of the section, the atom structure, and
$parser_env, the parser environment variable. It should return the line to be
written

has_endsection [B<type> : BOOLEAN, B<default> : 1] : if true, section have both
start and end tag. If false, a section is considered to end where another
section begins, or at the end of the file

endsection_output_function [B<type> : FUNCTION_REF, B<default> : sub { '' } ] :
the code to be applied to a section atom, to generate the end section line to
output. It takes in arguments the name of the section, the atom structure, and
$parser_env, the parser environment variable. It should return the line to be
written

endsection_regexp [B<type> : STRING, B<default> : ''] : the regexp that is
applied to determine if a line is a end of a section.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut



# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $ref_atoms = [];
    my $keyvalue_template = new Libconf::Templates::Generic::KeyValue($args);
    $keyvalue_template->{atoms} = $self->{atoms};
    my $sections_template = new Libconf::Templates::Generic::Sections($args);
    $sections_template->{atoms} = $self->{atoms};
    add2hash_($self, deepcopy($args));
    bless add2hash_($self, {
                            filename => $args->{filename},
                            template_name => 'KeyValueSections',
                            keyvalue_template => $keyvalue_template,
                            sections_template => $sections_template,
                            comments_struct => [['#'], [';']],
                           }), $class;
}

# private function, called by Libconf::Template::read_conf
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    # rule for sections
    ($out, $command) = $self->{sections_template}->_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    # rule for keyvalue
    $self->{keyvalue_template}->_parse_chunk($in, $atom, $parser_env);
}

# private function, called by Libconf::Template::write_conf
sub _output_atom {
    my ($self, $atom, $parser_env) = @_;

    # rule for keyvalue
    $atom->{type} eq 'KEY_VALUE' and
      return $self->{keyvalue_template}->_output_atom($atom, $parser_env);

    # sections, endsections and miscellaneous
    $self->{sections_template}->_output_atom($atom, $parser_env);
}

1
