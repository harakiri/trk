
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Generic::List;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
use Libconf qw(:helpers);
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::Generic::List - Libconf generic low level template for semantic association (VALUE0, VALUE1, ... VALUEn) styles config files

=head1 DESCRIPTION

Libconf::Templates::Generic::List is a generic template that handles config files that contain semantic informations of type : (VALUE0, VALUE1, ... VALUEn).

=head1 SYNOPSIS

 my $template = new Libconf::Templates::Generic::List({
                                                       filename => 'some_file',
                                                       comments_struct => [['#']],
                                                       regexp => '^\s*(\S+?):(\S*?):(\S*?):(.*?)\s*$',
                                                       output_function => sub { join(':', @{$atom->{list}} ) }
                                                      })
 $template->read_conf();
 ...
 (see L<Libconf::Templates> for transformation methods on $template)
 ...
 $template_write_conf();

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Generic::List({
                                                           filename => 'some_file',
                                                           comments_struct => [['#']],
                                                           regexp => '^\s*(\S+?):(\S*?):(\S*?):(.*?)\s*$',
                                                           output_function => sub { join(':', @{$atom->{list}} ) }
                                                         })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

comments_struct [B<type> : ARRAY_REF of ARRAY_REF of STRING,STRING,SCALAR,
B<default> : undef ] : defines the type and the characters of comments. See
L<Libconf::Templates::Generic::Keyvalue> for more details

regexp [B<type> : STRING, B<default> : ''] : the regexp that is applied to each line

output_function [B<type> : FUNCTION_REF, B<default> : sub {} ] : the code to be
applied to an atom, to generate the line to output. It takes in arguments the
atom.

_input_function [B<type> : FUNCTION_REF, B<default> : undefined ] : if defined,
it's used to parse. This function should behave as _parse_chunk. Don't
use it unless you know it. See the source for more info about _parse_chunk

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $default_args = {
                        filename => '',
                        regexp => '',
                        comments_struct => [[]],
                        output_function => {},
                        template_name => 'Generic::List'
                       };
    bless $self->init_args($args, $default_args), $class;
}

# private function, called by Libconf::Template::read_conf
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom);
    $command eq 'failed' or return ($out, $command);

    # rule
    my $regexp = $self->{regexp};
    if (my @list = $in =~ /$regexp/) {
        $atom->{type} = 'LIST';
        $atom->{list} = [ @list ];
        $in =~ s/$regexp//;
        return ($in, 'stop');
    }
    ($in, 'failed');
}

# private function, called by Libconf::Template::write_conf
sub _output_atom {
    my ($self, $atom, $parser_env) = @_;
    $atom->{type} eq 'LIST' and return ($self->{output_function}->($atom), 0);
    $self->SUPER::_output_atom($atom);
}

1
