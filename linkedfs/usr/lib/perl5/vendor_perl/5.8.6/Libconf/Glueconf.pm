#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf;
use strict;
use vars qw(@ISA @EXPORT_OK
$OldLibconfTemplates
);
use Libconf qw(:helpers);

$OldLibconfTemplates = 0;

=head1 NAME

Libconf::Glueconf - Libconf high level common module

=head1 DESCRIPTION

Libconf::Glueconf is a class that represents a config file at a high level. It
provides an easy to use and powerful structure to edit the config file.

=head1 GLOBAL OPTIONS

=over

=item B<$Libconf::Templates::OldLibconfTemplates>

This BOOLEAN tells wether to use the old libconf template system (if set to
true), or the new one (if set to false, default). This is kept for
compatibility only, it will be removed at some point

=back

=head1 SYNOPSYS

This is the high level layer of libconf. You should use it to read/write the
informations to the config files. Here are some examples, but you should look
in the specific modules below for more examples.

  use Libconf::Glueconf::Samba::Smb_conf;
  my $samba = new Libconf::Glueconf::Samba::Smb_conf('/etc/samba/smb.conf');
  $samba->{homes}->{writable} = 'TEST';
  $samba->{global}->{workgroup} eq 'WORKGROUP' or die "workgroup is not correct\n";
  $samba->write_conf();

  use Libconf::Glueconf::X::XF86Config;
  use Data::Dumper;
  $Data::Dumper::Deepcopy = 1;
  $Data::Dumper::Quotekeys = 0;
  my $xorg = new Libconf::Glueconf::X::XF86Config('/etc/X11/xorg.conf');
  print Dumper($xorg) . "\n";


=head1 MODULE LIST

See their documentation for specific methods

=over

=item B<Generic - Shell>

L<Libconf::Glueconf::Generic::Shell> : glueconf representation of shell-like files.

=item B<Samba - Smb_conf>

L<Libconf::Glueconf::Samba::Smb_conf> : glueconf representation of smb.conf from samba.

=item B<X - XF86Config>

L<Libconf::Glueconf::X::XF86Config> : glueconf representation of XF86Config or xorg.conf from X.

=back

=head1 GLUECONF GENERAL METHODS

=over

=item B<read_conf()>

  use Data::Dumper;
  my $structure = new Libconf::Glueconf::Some::Module;
  $structure->read_conf();
  print Dumper($structure) . "\n";

B<description>

This method reads the config file, and maps it to the structure. It doesn't use
any argument for its own use, but you can give it arguments, they will be
passed to the underlying Libconf::Templates::read_conf().

=cut

sub read_conf {
    my ($obj) = shift;
    if ($OldLibconfTemplates) {
        return $obj->{libconf}->readConf(@_);
    }
    $obj->{libconf}->read_conf(@_);
}

=item B<write_conf()>

  my $structure = new Libconf::Glueconf::Some::Module;
  $structure->read_conf();
  ... edit $structure ...
  $structure->write_conf();

B<description>

This method writes the structure back to the config file. It doesn't use any
argument for its own use, but you can give it arguments, they will be passed to
the underlying Libconf::Templates::write_conf().

=cut

sub write_conf {
    my $obj = shift;
    if ($OldLibconfTemplates) {
        return $obj->{libconf}->writeConf(@_);
    }
    $obj->{libconf}->write_conf(@_);
}

=item B<toXMLString($format)>

    $template->toXMLString();

B<description>

toXMLString exports the template informations into an XML string.

The optional $format parameter sets the indenting of the output.

=cut

sub toXMLString {
    my ($obj, $format) = @_;
    require Data::DumpXML;
    import Data::DumpXML qw(dump_xml);
#    local $Data::DumpXML::INDENT = "";
#    local $Data::DumpXML::XML_DECL = 0;
#    local $Data::DumpXML::DTD_LOCATION = "";
#    local $Data::DumpXML::NS_PREFIX = "dumpxml";
#    print  "++++++++++" . ref($obj) . "\n";
#    my $ref;
#    UNIVERSAL::isa($obj, 'HASH') and $ref = \%$obj;
#    UNIVERSAL::isa($obj, 'ARRAY') and $ref = \@$obj;
#    print  "--------" . ref($ref) . "\n";
#    print Dumper(\%hash) . "\n";
#    print "*--------------------*\n";
    if (UNIVERSAL::isa($obj, 'HASH')) {
        my %hash = %$obj;
        dump_xml(\%hash);
    } elsif (UNIVERSAL::isa($obj, 'ARRAY')) {
        my @array = @$obj;
        dump_xml(\@array);
    }        
}

=item B<toXMLFile($filename, $format)>

    $template->toXMLFile('xml_filename');

B<description>

toXMLFile exports the template informations into an XML file.

The optional $format parameter sets the indenting of the output.

=cut

sub toXMLFile {
    my ($obj, $filename, $format) = @_;
    output($filename, toXMLString($obj, $format));
}

=item B<fromXMLFile($filename)>

    $template->fromXMLFile('file.xml');

B<description>

fromXMLFile imports the data from an XML file previously generated by toXMLFile

=cut

sub fromXMLFile {
    my ($obj, $filename) = @_;
    require Data::DumpXML::Parser;
    my $p = Data::DumpXML::Parser->new();
    my $data = $p->parsefile($filename)->[0];
    if (UNIVERSAL::isa($obj, 'HASH')) {
        %$obj = %$data;
    } elsif (UNIVERSAL::isa($obj, 'ARRAY')) {
        @$obj = @$data;
    }        
}
# dummy function, should be cleaned up

sub bool2text { $_[0] ? "true" : "false" }
sub bool2yesno { $_[0] ? "yes" : "no" }
sub text2bool { my $t = lc($_[0]); $t eq "true" || $t eq "yes" ? 1 : 0 }


# dummy functions for compatibility

sub readConf {
    warn "readConf is OBSOLETE, you should use read_conf now. readConf will be removed soon.";
    shift->read_conf(@_);
}

sub writeConf {
    warn "writeConf is OBSOLETE, you should use write_conf now. writeConf will be removed soon.";
    shift->write_conf(@_);
}

=back

=cut

1;
