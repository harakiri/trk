
# Author : Charles LONGEAU (chl@tuxfamily.org)
#
#
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : xinetd
#
# This templates is used to parse xinetd style configuration files.
#

package Libconf;

$templates{xinetd} = {
			   rules => [ 
 				     q(
                                       if ($in =~ s/^\s*{\s*$//) {
                                           $matched = 1;
                                       }
                                      ),
 				     q(
                                       if ($in =~ s/^\s*}\s*$//) {
                                           $atom->{type} = 'ENDSECTION';
                                           $matched = 1;
                                       }
                                      ),
				     q(
                                       if ($in =~ s/^\s*(\S+?)\s*$//) {
                                           $atom->{type} = 'SECTION';
                                           $atom->{section_name} = $1;
                                           $keep_atom = 1;
                                           $out->{line_continued} = 1;
                                           $out->{current_section} = { name => $1 };
                                           $keep_atom = 1;
                                           $matched = 1;
                                       }
                                     ),
 				     q(
                                        if ($in =~ s/^\s*\{\s*$// && $out->{line_continued}) {
                                           $matched = 1;
                                        }
                                      ),
				     q(
                                       if ($in =~ s/^\s*(.*?)\s*=\s*(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $out->{current_section} and $atom->{sections} = [ $out->{current_section} ];
                                           $matched = 1;
                                       }
                                     ),
				     q(
                                       if ($in =~ s/^\s*(include(dir)?)\s+(.*)\s*$//) {
                                           $atom->{type} = 'INCLUDE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $3;
                                           $matched = 1;
                                       }
                                     ),
				    ],
			   comments => [ ['#'] ],
			   comment_output => q(/^(\s*)$/ ? "$_\n" : "#$_\n"),
		           output => {
				      KEY_VALUE => q(
                                            my ($key, $value) = ($atom->{key}, $atom->{value});
                                            return qq($key = $value\n);
                                      ),
				      INCLUDE => q(
                                            my ($key, $value) = ($atom->{key}, $atom->{value});
                                            return qq($key $value\n);
                                      ),
				      SECTION => q(
                                            return qq($atom->{section_name}\n{\n);
                                      ),
				      ENDSECTION => q(
                                            return qq(}\n);
                                      ),
				     },
			   edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	$_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                           	$i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args)
                                         ),
			  };

1
