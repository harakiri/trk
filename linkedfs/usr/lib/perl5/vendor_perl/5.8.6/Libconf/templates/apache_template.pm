
# Author : damien KROTKINE (damien@tuxfamily.org)
#
#
# Copyright (C) 2002 damien KROTKINE (damien@tuxfamily.org)
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Libconf Template : apache
#
# This templates is used to parse apache configuration files.
# Sections are fully supported
#

package Libconf;

$templates{apache} = {
                           rules => [
				     q(
                                       if ($in =~ s|^\s*<([^/]\S+)\s*(.*)>\s*$||) {
                                           $atom->{type} = 'SECTION';
                                           $atom->{section_type} = $1;
                                           $atom->{section_name} = $2;
                                           push @{$out->{current_sections}}, { type => $1, name => $2 };
                                           $matched = 1;
                                       }
                                     ),
				     q(
                                       if ($in =~ s|^\s*</(\S+)>\s*$||) {
                                           $atom->{type} = 'ENDSECTION';
                                           $atom->{section_type} = $1;
                                           pop @{$out->{current_sections}};
                                           push @{$out->{current_sections}}, { type => $1, name => $2 };
                                           $matched = 1;
                                       }
                                     ),
				     q(
                                       if ($in =~ s/^\s*(\S+)\s*(.*)\s*$//) {
                                           $atom->{type} = 'KEY_VALUE';
                                           $atom->{key} = $1;
                                           $atom->{value} = $2;
                                           $out->{current_sections} and $atom->{sections} = [ @{$out->{current_sections}} ];
                                           $matched = 1;
                                       }
                                     ),
                                    ],
                           comments => [ ['#'] ],
			   comment_output => q(
                                              my $indent = $INDENT_SPACES x $out->{current_indentation};
                                              /^(\s*)$/ ? "$indent$_\n" : "$indent#$_\n"
                                              ),
		           output => {
				      KEY_VALUE => q(
                                          my ($key, $value) = ($atom->{key}, $atom->{value});
                                          return $INDENT_SPACES x $out->{current_indentation} . qq($key $value\n);
                                      ),
				      SECTION => q(
                                          my $indent = $INDENT_SPACES x $out->{current_indentation}++;
                                          return $indent . qq(<$atom->{section_type} $atom->{section_name}>\n);
                                      ),
				      ENDSECTION => q(
                                          my $indent = $INDENT_SPACES x --$out->{current_indentation};
                                          return $indent . qq(</$atom->{section_type}>\n);
                                      ),
				     },
                           edit_atom => q(
                                         if ($index == -1) {
                                             my $i = 0;
                                             foreach (@{$out->{atoms}}) {
                                                 $_->{section_type} eq $args{section_type} and $index = $i; #idem
                                                 $_->{key} eq $args{key} and $index = $i; #we don't exit the loop, to have the last atom if multiples ones match
                                                $i++;
                                             }
                                             $index == -1 and return -2;
                                         }
                                         @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args)
                                       ),
                          };

1
