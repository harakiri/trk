
# Author : damien KROTKINE (damien@tuxfamily.org)
#
#
# Copyright (C) 2002 damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.




# Libconf Template : hosts
#
# This templates is used to /etc/hosts file type
# There is no section handling.


# the hosts template goal is to parse config files type whith atoms of the form :
#        127.0.0.1       localhost
#        192.168.1.10    foo.mydomain.org  foo
#        192.168.1.13    bar.mydomain.org  bar
#        216.234.231.5   master.debian.org      master
#        205.230.163.103 www.opensource.org

package Libconf;

$templates{hosts} = {
			  rules => [ q( if ($in =~ s/^\s*(\S+)\s+(\S+)//) {
                                            $atom->{type} = 'KEY_VALUES';
                                            $atom->{key} = $1;
                                            $atom->{values}{canonical_hostname} = $2;
                                            $atom->{values}{aliases} = [];
                                            while ($in =~ s/^\s*(\S+)//) {
                                                push @{$atom->{values}{aliases}}, $1;
                                            }
                                            @{$atom->{values}{aliases}} == 0 and push @{$atom->{values}{aliases}}, '';
                                            $matched = 1;
                                        }
                                      ),
                                   ],
			  comments => [ ['#'] ],
		          output => {
				     KEY_VALUES => q(
                                            my ($key, $values) = ($atom->{key}, $atom->{values});
                                            $output_text = $atom->{key} . "\t" . $atom->{values}{canonical_hostname} . "\t" .
                                                           join (' ', @{$atom->{values}{aliases}});
                                     ),
				    },
			  edit_atom => q(
                                           if ($index == -1) {
                                               my $i = 0;
                                               foreach (@{$out->{atoms}}) {
                                           	   $_->{key} eq $args{key} and $index = $i; #we do not exit the loop, to have the last atom if multiples ones match
                                           	   $i++;
                                               }
                                               $index == -1 and return -2;
                                           }
                                           if (defined $args{type}) { @{$out->{atoms}}[$index]->{type} = $args{type}; }
                                           if (defined $args{key}) { @{$out->{atoms}}[$index]->{key} = $args{key}; }
                                           if (defined $args{values}) {
                                               defined $args{values}{canonical_hostname} and
                                                   @{$out->{atoms}}[$index]->{values}{canonical_hostname} = $args{values}{canonical_hostname};
                                               defined $args{values}{aliases} and
                                                   @{$out->{atoms}}[$index]->{values}{aliases} = [ @{$args{values}{aliases}} ];
                                           }
                                           return $index;
                                         ),
			  find_atom_pos => q(
                                             my $i = 0;
                                             my @res;
                                             foreach my $atom (@{$out->{atoms}}) {
                                                 my $flag = 1;
                                                 if (defined $args{key}) { $atom->{key} eq $args{key} or $flag = 0; }
                                                 if (defined $args{values}) {
                                                     if (defined $args{values}->{canonical_hostname}) {
                                                         $atom->{values}->{canonical_hostname} eq $args{values}->{canonical_hostname} or $flag = 0;
                                                     }
                                                     if (defined $args{values}->{aliases}) {
                                                         foreach (0..@{$args{values}->{aliases}}) {
                                                             $atom->{values}->{aliases}->[$_] eq $args{values}->{aliases}->[$_] or $flag = 0;
                                                         }
                                                     }
                                                 }
                                                 $flag and push(@res, $i);
                                                 $i++;
                                             }
                                             wantarray ? @res : $res[0];
                                           ),
                    };

1
