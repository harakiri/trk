#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::System::Autologin;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf::Generic::Shell;
#use Libconf::Glueconf;
our @ISA = qw(Libconf::Glueconf::Generic::Shell);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $filename, $data_synopsis_version) = @_;
    my ($data_description, $data_mapping);
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    bless $class->SUPER::new($filename, $data_description, $data_mapping), $class;
}

$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {
                   # Start the session as the user specified here.
                   # This setting is mandatory. If omitted, autologin will not run.
                   # If autologin was compiled with --enable-paranoid, autologin will
                   # not run if the user specified has UID or GID 0.
                   USER => { type => 'STRING',
                             values => sub {
                                 require Libconf::System::Users;
                                 my $sys_users = new Libconf::System::Users;
                                 [$sys_users->getUsersList(sub { $_[0]->{UID} >= 500 })];
                             }
                           },

                   # The script or program listed here will be executed as the user
                   # specified above.
                   # If this setting is omitted, /usr/X11R6/bin/startx will be used.
                   EXEC => { type => 'COMMAND' }, #[script or program]

                   # You can use this setting to turn off autologin even if it is
                   # installed and the config file exists and is considered safe.
                   # If this setting is omitted, "yes" is assumed.
                   AUTOLOGIN => { type => 'BOOLEAN' },
                  },
  };

1;

