#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Networking::Network;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf::Generic::Shell;

our @ISA = qw(Libconf::Glueconf::Generic::Shell);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $filename, $data_synopsis_version) = @_;
    my ($data_description, $data_mapping);
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    bless $class->SUPER::new($filename, $data_description, $data_mapping), $class;
}

$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {

                   NETWORKING => { type => 'BOOLEAN' },
                   HOSTNAME => { type => 'HOSTNAME', default => 'localhost.localdomain' },
                   GATEWAY => { type => 'IP' },
                   GATEWAYDEV => { type => 'NET_INTERFACE' },
                   NISDOMAIN => { type => 'DOMAINNAME' },
                   VLAN => { type => 'BOOLEAN' },

                   IPX => { type => 'BOOLEAN', default => 0 },
                   IPXAUTOPRIMARY => { type => 'STRING', values => [ qw (on off) ], default => 'off' },
                   IPXAUTOFRAME => { type => 'STRING', values => [ qw (on off) ], default => 'off' },
                   IPXINTERNALNETNUM => { type => 'INTEGER', default => 0 },
                   IPXINTERNALNODENUM => { type => 'INTEGER', default => 0 },
                   IFPLUGD_ARGS => { type => 'STRING', default => '-w -b' },

                   NETWORKING_IPV6 => { type => 'BOOLEAN', default => 0 },
                   IPV6FORWARDING => { type => 'BOOLEAN', default => 0 },
                   IPV6INIT => { type => 'BOOLEAN' },

                   IPV6_AUTOCONF => { type => 'BOOLEAN', default => sub { !$_[0]->{IPV6FORWARDING} } },
                   IPV6_ROUTER => { type => 'BOOLEAN',  default => sub { $_[0]->{IPV6FORWARDING} } },
                   IPV6_AUTOTUNNEL => { type => 'BOOLEAN',  default => 0 },
                   IPV6_DEFAULTGW => { type => 'STRING', },
                   IPV6_DEFAULTDEV => { type => 'NET_INTERFACE', },
                   IPV6_RADVD_PIDFILE => { type => 'FILENAME', default => '/var/run/radvd/radvd.pid' },
                   IPV6TO4_RADVD_PIDFILE => { type => 'FILENAME', default => '/var/run/radvd/radvd.pid' },
                   IPV6_RADVD_TRIGGER_ACTION => { type => 'STRING', values => [ 'start', 'stop', 'reload', 'restart', 'SIGHUP'], default => 'SIGHUP' },

                   FORWARD_IPV4 => { type => 'BOOLEAN', default => 0 },
                   DEFRAG_IPV4 => { type => 'BOOLEAN', default => 0 },

                  },
   mapping => {
               BOOLEAN => sub {
                   my ($value) = @_;
                   $value eq 'yes' and return 1;
                   $value eq 'no' and return 0;
                   $value ? 'yes' : 'no';
               }
              },
  };

1;




