#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 2004 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Generic::Shell;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
use Libconf::Templates::Shell;
@ISA = qw(Libconf::Glueconf);

=head1 NAME

Libconf::Glueconf::Generic::Shell - Glueconf high level template for shell styles config files

=head1 DESCRIPTION

  use Data::Dumper;
  my $make_conf = new Libconf::Glueconf::Generic::Shell({ filename => "/etc/make.conf"
                                                          shell_style => "true_bash",
                                                          shell_command => "/bin/bash",
                                                        });
  print Dumper($make_conf) . "\n";
  $make_conf->{CHOST} = 'i586-pc-linux-gnu';
  $make_conf->write_conf('/etc/make.conf_new');

This template maps any shell config files into a virtual hashref with key =>
value association. You can edit the hash and regenerate the config file.

=head1 CONSTRUCTOR

  my $conf = new Libconf::Glueconf::Generic::Shell({ filename => "shell_style.conf",
                                                     simplify_quote => 1,
                                                   });

The constructore returns a variable which is at the same time an object on
which you can call the B<Glueconf General methods> (see L<Libconf::Glueconf> ),
and at the same time a reference on a hash, whith the keys/values of your
config file.

the options you can give to the constructore are the same as in
L<Libconf::Templates::Shell>

=cut

sub new {
    my ($class, $args, $data_description, $data_mapping) = @_;
    my $libconf = new Libconf::Templates::Shell($args);
    $libconf->read_conf();
    tie my %wrapper, 'Libconf::Glueconf::Generic::Shell::Wrapper', $libconf, $data_description, $data_mapping;
    bless \%wrapper, $class;
}

sub description { 'high level template for shell styles config files' }
package Libconf::Glueconf::Generic::Shell::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    bless {
           libconf => $libconf,
           _data_description => $data_description,
           _data_mapping => $data_mapping,
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'KEY_VALUE', key => $key });
    foreach (@pos) {
        $obj->{libconf}->delete_atom($_);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->get_atom(0);
    $atom->{key};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->find_atom_pos( { type => 'KEY_VALUE', key => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'KEY_VALUE', key => $lastkey }); #FIXME : double entries should be removed elsewhere
    $pos[-1]+1 >= $obj->{libconf}->get_size and return undef;
    $obj->{libconf}->get_atom($pos[-1]+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq '' or die 'trying to store anything else than a value';
    my $index;
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key });
    if (@pos == 0) {
        $index = $obj->{libconf}->append_atom({ type => 'KEY_VALUE', key => $key, value => $value });
    } else {
        $index = $pos[-1];
    }
    $obj->{libconf}->edit_atom($index, { type => 'KEY_VALUE', key => $key}, {value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key });
    @pos == 0 and return undef;
    $obj->{libconf}->get_atom($pos[-1])->{value};
}

1;

