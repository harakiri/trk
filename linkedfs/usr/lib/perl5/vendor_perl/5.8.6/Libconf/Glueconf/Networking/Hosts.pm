#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Networking::Hosts;
use strict;
use Libconf;

our @ISA = qw(Libconf);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $filename, $data_synopsis_version) = @_;
    my ($data_description, $data_mapping);
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    my $libconf = Libconf::new('Libconf', $filename, 'hosts', '');
    tie my %wrapper, 'Libconf::Glueconf::Networking::Hosts::Wrapper', $libconf, $data_description, $data_mapping;
    bless \%wrapper, $class;
}

sub readConf {
    my ($obj) = shift;
    $obj->{libconf}->readConf(@_);
}

sub writeConf {
    my ($obj) = shift;
    $obj->{libconf}->writeConf(@_);
}

package Libconf::Glueconf::Networking::Hosts::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    bless {
           libconf => $libconf,
           _data_description => $data_description,
           _data_mapping => $data_mapping,
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUES', key => $key });
    foreach (@pos) {
        $obj->{libconf}->deleteAtom($_);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->getAtom(0);
    $atom->{key};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUES', key => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'KEY_VALUES', key => $lastkey }); #FIXME : double entries should be removed elsewhere
    $pos[-1]+1 >= $obj->{libconf}->size and return undef;
    $obj->{libconf}->getAtom($pos[-1]+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq '' or die 'trying to store anything else than a value';
    my $index;
    my @pos = $obj->{libconf}->findAtomPos({ type => 'KEY_VALUES', key => $key });
    if (@pos == 0) {
        $index = $obj->{libconf}->appendAtom({ type => 'KEY_VALUES', key => $key, values => $value });
    } else {
        $index = $pos[-1];
    }
    $obj->{libconf}->editAtom($index, { type => 'KEY_VALUES', key => $key, value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my @pos = $obj->{libconf}->findAtomPos({ type => 'KEY_VALUES', key => $key });
    @pos == 0 and return undef;
    $obj->{libconf}->getAtom($pos[-1])->{values};
}

package Libconf::Glueconf::Networking::Hosts;
use strict;
use Libconf::Types;

$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {
                   $Libconf::Types::IP4_REGEXP => {
                                                   canonical_hostname => { type => 'HOSTNAME' },
                                                   aliases => { type => 'HOSTNAME' },
                                                  },
                  }
  };

1;

