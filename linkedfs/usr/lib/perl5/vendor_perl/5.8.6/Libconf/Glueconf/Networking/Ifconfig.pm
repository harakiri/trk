#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Networking::Ifconfig;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf::Generic::Shell;
#use Libconf::Glueconf;

our @ISA = qw(Libconf::Glueconf::Generic::Shell);
our $data_synopsis;

sub new {
    my ($class, $filename, $data_synopsis_version) = @_;
    my ($data_description, $data_mapping);
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    bless $class->SUPER::new($filename, $data_description, $data_mapping), $class;
}

$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {
                   NAME => { type => 'STRING' },
                   DEVICE => { type => 'DEVICE' },
                   IPADDR => { type => 'IPV4_ADDR' },
                   SRCADDR => { type => 'IP' },
                   NETMASK => { type => 'MASK' },
                   GATEWAY => { type => 'IP' },
                   ONBOOT => { type => 'BOOLEAN', default => 1 },
                   USERCTL => { type => 'BOOLEAN', default => 0 },
                   BOOTPROTO => { type => 'STRING', values => [ qw (none static bootp dhcp) ], default => 'static' },
                   MTU => { type => 'STRING' },
                   PEERDNS => { type => 'BOOLEAN', default => 1 },
                   DNS1 => { type => 'IP' },
                   DNS2 => { type => 'IP' },
                   FIREWALL_MODS => { type => 'BOOLEAN', default => 1 },
                   SRCADDR => { type => 'STRING' },
                   MII_NOT_SUPPORTED => { type => 'BOOLEAN', default => 0 },
                   DHCP_CLIENT => { type => 'STRING', values => [ qw(/sbin/dhcpcd /sbin/pump /sbin/dhcpxd /sbin/dhclient) ] },
                   DHCP_TIMEOUT => { type => 'INTEGER' },
                  },
  };

1;

