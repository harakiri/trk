#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors :
#
# Copyright (C) 2002-2003 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::NUT::Ups_conf;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
use Libconf::Templates::Ups;
@ISA = qw(Libconf::Glueconf);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $args, $o_data_synopsis_version) = @_;
    my ($data_description, $data_mapping);
    if (defined $data_synopsis) {
        $o_data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$o_data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$o_data_synopsis_version}{mapping};
    }
    my $libconf = new Libconf::Templates::Ups($args);
    $libconf->read_conf();
    $libconf->set_uniq(qw(key sections));
    tie my %wrapper, 'Libconf::Glueconf::NUT::Ups_conf::Wrapper', $libconf, $data_description, $data_mapping;
    bless \%wrapper, $class;
}

package Libconf::Glueconf::NUT::Ups_conf::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    bless {
           libconf => $libconf,
           _data_description => $data_description,
           _data_mapping => $data_mapping,
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear();
}

#sub DESTROY {
#}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $key });
    foreach (@pos) {
        $obj->{libconf}->delete_section($_, 1);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->get_atom(0);
    if (defined $atom->{type} && $atom->{type} ne '') {
        $atom->{type} eq 'SECTION' or warn "atom n� 0 is not a section\n";
    }
    $atom->{section_name};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [] });
    my $i = 0;
    my $index;
    foreach (@pos) {
        $obj->{libconf}->get_atom($_)->{section_name} eq $lastkey and $index = $i;
        $i++;
    }
    defined $index or return undef;
    $index == $#pos and return undef;
    print " $lastkey NXTK : " . $obj->{libconf}->get_atom($pos[$index+1])->{section_name} . "\n";
    $obj->{libconf}->get_atom($pos[$index+1])->{section_name};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq 'HASH' or die 'trying to store something else than hash ref';
    my $index;
    my $pos = $obj->{libconf}->find_atom_pos({ type => 'SECTION', section_name => $key, sections => [] });
    defined $pos or $pos = $obj->{libconf}->append_atom({ section_name => $key, type => 'SECTION', sections => [] });
    my %hash;
    tie %hash, 'Libconf::Glueconf::NUT::Ups_conf::SectionWrapper', $obj->{libconf}, $pos;
    %hash = %$value;
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my $section_position = $obj->{libconf}->find_atom_pos( { type => 'SECTION', section_name => $key, sections => [] });
    defined $section_position or $section_position = $obj->{libconf}->append_atom({ section_name => $key, type => 'SECTION', sections => [] });
    my %ret;
    tie %ret, 'Libconf::Glueconf::NUT::Ups_conf::SectionWrapper', $obj->{libconf}, $section_position;
    \%ret;
}

package Libconf::Glueconf::NUT::Ups_conf::SectionWrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $beginning) = @_;
    debug("beginning : $beginning");
    my $atom = $libconf->get_atom($beginning);
    my @depth = @{$atom->{sections}};
    scalar(keys %{$depth[0]}) == 0 and shift @depth;
    push @depth, { name => $atom->{section_name} };

    my $key = $beginning+1;
    foreach ($beginning+1..$libconf->get_size-1) {
        my $atom = $libconf->get_atom($_);
        @{$atom->{sections}} >= @depth and next;
        Libconf::compare_depth($atom->{sections}, \@depth) or last;
    } continue {
        $key++;
    }
    my %hash = (
                libconf => $libconf,
                firstatom => $beginning,
                sections => \@depth,
                lastatom => $key,
               );
    debug("firstatom : $hash{firstatom} - lastatom : $hash{lastatom}");
    return bless \%hash, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{lastatom} -= $obj->{libconf}->clear_section($obj->{firstatom}, 1);
    $obj->{lastatom} == $obj->{firstatom} + 1 or die "removed atoms number is wrong";
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key : $key");
    my @pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    foreach (@pos) {
        $obj->{libconf}->delete_atom($_);
        $obj->{lastatom}--;
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    $obj->{firstatom} + 1 == $obj->{lastatom} and return undef;
    my $atom = $obj->{libconf}->get_atom($obj->{firstatom}+1);
    $atom->{type} eq 'KEY_VALUE' and return $atom->{key};
    die "houston, we have a problem";
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    defined $pos ? 1 : 0;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("SectionWrapper - NEXTKEY - lastkey : $lastkey");
    my $pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $lastkey },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    !defined $pos || $pos+1 == $obj->{lastatom} and return undef;
    return $obj->{libconf}->get_atom($pos+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    #ref $value eq 'HASH' or die 'trying to store anything else than hash ref';
    ref $value eq '' or die "try to store a hash in a kery_value atom";
    my $pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    if (!defined $pos) {
        $pos = $obj->{libconf}->insert_atom($obj->{lastatom}, { type => 'KEY_VALUE', key => $key,
                                                               sections => [ @{$obj->{sections}} ],
                                                             }
                                          );
        $obj->{lastatom}++;
    }
    $obj->{libconf}->edit_atom($pos, { value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key\n");
    my $pos = $obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    defined $pos or return undef;
    return $obj->{libconf}->get_atom($pos)->{value};
}

package Libconf::Glueconf::NUT::Ups_conf;
$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {
                  },
   mapping => {
              },
  };

1;

