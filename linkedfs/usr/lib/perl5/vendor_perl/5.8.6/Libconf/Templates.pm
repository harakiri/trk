#!/usr/bin/perl

# Author : Damien KROTKINE (dams@libconf.net)
#
# Contributors : 
#
# Copyright (C) 2004 Damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates;
use strict;
use Libconf qw(:helpers);

$Libconf::Templates::Trimspaces = 1;
$Libconf::Templates::Indentspaces = '    ';

=head1 NAME

Libconf::Templates - Libconf low level templates common module

=head1 DESCRIPTION

Libconf::Templates is a class that represents a config file at a low level.

=head1 GLOBAL OPTIONS

=over

=item B<$Libconf::Templates::Indentspaces>

This STRING is written for each indentation level. By default, it's 4 spaces :
'    '

=item B<$Libconf::Templates::Trimspaces>

if set to true, unused space characters (+ tabs, ...) are removed when writing
down a config file. By default, true

=back

=head1 TEMPLATE LIST

See their documentation for specific methods

=over

=item B<KeyValue>

L<Libconf::Templates::Generic::KeyValue> : Libconf low level template for semantic (KEY VALUE) styles config files.

=item B<KeyValueSections>

L<Libconf::Templates::Generic::KeyValueSections> : Libconf low level template for semantic (KEY VALUE) + sections styles config files.

=item B<KeyValues>

L<Libconf::Templates::Generic::KeyValues> : Libconf low level template for semantic (KEY (list of named VALUES)) styles config files.

=item B<List>

L<Libconf::Templates::Generic::List> : Libconf low level template for semantic (LIST) informations styles config files.

=item B<Sections>

L<Libconf::Templates::Generic::Sections> : Libconf low level template for semantic (SECTIONS) handling in config files.

=item B<Value>

L<Libconf::Templates::Generic::Value> : Libconf low level template for semantic (VALUE) styles config files.

=item B<KeyList>

L<Libconf::Templates::Generic::Value> : Libconf low level template for semantic (KEY LIST) styles config files.

=item B<Shell>

L<Libconf::Templates::Shell> : Libconf low level template for shell styles config files.

=item B<Group>

L<Libconf::Templates::Group> : Libconf low level template for group config files

=item B<Passwd>

L<Libconf::Templates::Passwd> : Libconf low level template for password config files

=item B<Samba>

L<Libconf::Templates::Samba> : Libconf low level template for samba config files

=item B<XF86Config>

L<Libconf::Templates::XF86Config> : Libconf low level template for XF86Config/Xorg config files

=back

=head1 TEMPLATE GENERAL METHODS

=over

=cut

#generic constructor (should be called by all templates constructors
sub new {
    my $self = {
                atoms => [],
               };
    bless $self, shift;
}

#--------- input functions --------

=item B<read_conf()>

=item B<read_conf($filename)>

  $template->read_conf();
  $template->read_conf($alternate_filename);

B<arguments>

$filename [B<type> : STRING, B<default> : $self->{filename}] : Specifies the file to read.

B<description>

This method reads the config file by following the rules of the template. The
datas created are attached to the template instance. This function is very
useful.

Note that the file is read using the special encoding $self->{encoding}, if
specified. If not specified, it is up to perl to choose the encoding.

Usually you want to simply do :

  $template->read_conf(); # reads the config file associated to the template

If you want, you can read an alternate config file like this :

  $template->read_conf($alternate_filename); # reads another filename

But I doubt it is useful, if you want to work on another file, you should do :

  $template->set_filename($alternate_filename); # change the file we are working on
  $template->read_conf(); # reads the config file associated to the template

It doesn't return anything, but stores the interpreted values in $template->{atoms}.

=cut

sub read_conf {
    my ($self, $filename) = @_;
    $filename ||= $self->{filename};
    my @file;
    if (-e $filename) {
        @file = cat_($filename, $self->{encoding});
    } else {
        @file = ();
    }
    _parse($self, \@file);
}

# parse(LIST_REF)

# This function parses a list of lines, by calling $template->parse_chunk() on each line.

my $parser_env = {};

sub _parse {
    my ($self, $file) = @_;
    my @file = @$file;
    my $item = 0;
#    my $next_item = 0;
#    my $template_name = $self->{template_name};
    my @out_atoms;

    $parser_env = { current_sections => [] };
    my $line_idx = 1;
    foreach my $in (@file) {
	$out_atoms[$item] ||= _new_atom( { sections => [ @{$parser_env->{current_sections}} ] });
        $out_atoms[$item]->{comments} ||= [];
	$in =~ /^\s*\n$/ and push(@{$out_atoms[$item]->{comments}}, { comment => '', inline => 0 }), next;
#	$in eq "\n" and $out_atoms[$item]->{line_feed}++, next;
	chomp $in;
	#see if we are in a line previously commented
	if ($out_atoms[$item]->{current_comment}) {
	    my $c = $out_atoms[$item]->{current_comment};
	    if ($in =~ s/^(.*\Q$c\E)//) {
                push(@{$out_atoms[$item]->{comments}}, { comment => $1, inline => $in =~ /^\s*$/ ? 0 : -1 });
		undef $out_atoms[$item]->{current_comment};
	    } else {
                push(@{$out_atoms[$item]->{comments}}, { comment => $in, inline => 0 });
		$in = '';
	    }
	} else {
	    # we remove comments
	    # the comment structure : [ open_char, close_char, nested ]
	    # There is an additional comment command : disable_inline_comments, to avoid handling inline comment characters
	    my ($open, $close, $nested);
	    foreach (@{$self->{comments_struct}}) {
		($open, $close, $nested) = @$_;
		if ($nested) {
		    my ($level, $start) = (0, 0);  #we don't use $out_atoms[$item]->{current_level} directly to avoid overriding it.
		    my $end = 0; #length($in);
		    for (my $i=0; $i < length($in); $i++) {
			substr($in, $i, length($open)) eq $open and $level++, $start ||= $i;
			if ($level && substr($in, $i, length($close)) eq $close) {
			    $level--;
			    $level or $end = $i-$start;
			}
		    }
		    $end and push(@{$out_atoms[$item]->{comments}}, { comment => substr($in, $start, $end + length($close)),
                                                                    inline => $start > 0 ? 1 : 0 });
		    substr($in, $start, $end + length($close)) = '';
		    $level and $out_atoms[$item]->{current_comment} = $close, $out_atoms[$item]->{current_level} = $level;
		} elsif (defined $open && defined $close) {
		    $in =~ s/(\Q$open\E.*\Q$close\E)/push(@{$out_atoms[$item]->{comments}}, { comment => $1, inline => length($') ? -1 : (length($`) ? 1 : 0) }); undef/eg;
		    #$in =~ s/(\Q$open\E.*\Q$close\E)/$out_atoms[$item]->{comments} .= "$1\n"; undef/eg;
		    $in =~ s/(\Q$open\E.*)$// and push(@{$out_atoms[$item]->{comments}}, { comment => $1,
                                                                                        inline => $in =~ /^\s*$/ ? 0 : 1 }
                                                      ), $out_atoms[$item]->{current_comment} = $close, last;
		} elsif (defined $open) {
                    if ($self->{disable_inline_comments}) {
                        if ($in =~ s/^\s*(\Q$open\E.*)$//) {
                            my $tmp = $1;
                            push(@{$out_atoms[$item]->{comments}}, { comment => $tmp, inline => 0 });
		        }
		    } else {
		        if ($in =~ s/(\Q$open\E.*)$//) {
                            my $tmp = $1;
                            push(@{$out_atoms[$item]->{comments}}, { comment => $tmp,
		                                                     inline => $in =~ /^\s*$/ ? 0 : 1 });
                        }
                    }
		}
	    }
        }
	### End Comment handling ###
        my $keep_atom = 0;
        my $switch_to_next_atom = 0;
        my $switch_to_prev_atom = 0;
        my $atom = $out_atoms[$item];
	my $command;
	while (1) {
	    # print " ---------- line : $line_idx | in : [[$in]] \n;";
            ($in, $command) = $self->_parse_chunk($in, $atom, $parser_env);
	    # print "->>>>> $command\n";
	    $command eq 'stop' and last;
	    if ($command eq 'switch_to_prev_atom') {
                $item--;
                $atom = $out_atoms[$item];
                $atom ||= _new_atom( { sections => [ @{$parser_env->{current_sections}} ] });
                $atom->{comments} ||= [];
                next;
            }
            if ($command eq 'switch_to_next_atom') {
                $item++;
                $atom = $out_atoms[$item];
                $atom ||= _new_atom( { sections => [ @{$parser_env->{current_sections}} ] });
                $atom->{comments} ||= [];
		next;
            }
            if ($command eq 'next_chunk') {
                $keep_atom = 1, last;
            }
	    if ($command eq 'failed') {
                warn "Libconf::Template warning: no rules to parse line n�" . $line_idx . " | template $self->{template_name} | file $self->{filename} | line was : [[$in]]\n";
                $atom->{type} = 'UNKNOWN';
                $atom->{value} = $in;
                $in = '';
                last;
            }
	}

        if ($in !~ /^\s*$/) {
            push @{$atom->{invalid}}, $in;
            print "Libconf.pm warning: can't parse line n�" . $line_idx . " | template $self->{template_name} | file $self->{filename} | line was : [[$in]]\n";
        }
        exists $atom->{sections} or die '$atom->{sections} doesn\'t exist';
        $keep_atom or $item++;
    } continue {
        $line_idx++;
    }
    # we don't use reference copy, that's on purpose. We don't want to overwrite the ref.
    @{$self->{atoms}} = @out_atoms;
    $self->parse_eof($parser_env);
}

# generic parse_eof function for children
sub parse_eof {
    my ($self, $parser_env) = @_;
    delete $parser_env->{current_sections}; #BUG : is this really needed (it looks ugly)
}

# generic parse function for children
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;
    # if the chunk is empty, return a next_chunk command
    # this command go to the next chunk without changing to the next atom.
    $in =~ s/^\s*$// and return ($in, 'next_chunk');
    exists $self->{_input_function} and return $self->{_input_function}->(@_);
    ($in, 'failed');
}

#--------- output functions --------

=item B<write_conf()>

=item B<write_conf($filename)>

  $template->write_conf();
  $template->write_conf($alternate_filename);

B<arguments>

$filename [B<type> : STRING, B<default> : $self->{filename}] : Specifies the file to write into.

B<description>

This method writes the config file by following the rules of the template. This function is very useful.

Note that the file is written using the special encoding $self->{encoding}, if
specified. If not specified, it is up to perl to choose the encoding.

Usually you want to simply do :

  $template->write_conf(); # writes the config file associated to the template

If you want, you can write an alternate config file like this :

  $template->write_conf($alternate_filename); # writes down another filename

But I doubt it is useful, if you want to work on another file, you should do :

  $template->set_filename($alternate_filename); # change the file we are working on
  $template->write_conf(); # writes the config associated to the template

=cut

sub write_conf {
    my ($self, $filename) = @_;
    $filename ||= $self->{filename};
#    my $template_name = $self->{template_name};
    if (defined $self->{timestamp} && (getTimestamp($filename) != $self->{timestamp})) {
        print "Warning $filename has changed !\n";
    }
    local *F;
    open F, "> $filename" or die "cannot create config file `$filename' (do you have proper permissions?)\n";
    #special rule called at the beginning
    print F _handle_output($self->_output_header($parser_env));

    #output for each atom
    my $local_func = exists $self->{_output_function} ? $self->{_output_function} : sub { shift->_output_atom(@_) };
    my $i = 0;
    foreach (@{$self->{atoms}}) {
        my ($text, $indentation);
        eval {
            ($text, $indentation) = $local_func->($self, $_, $parser_env);
        };
        $@ and chomp $@, die "$@ - atom n� $i - \n";
        print F _handle_output($text, $indentation, $_->{comments});
        $i++;
    }

    #special rule called at the end
    print F _handle_output($self->_output_footer($parser_env));
}


sub _handle_output {
    my ($text, $indentation, $comments) = @_;
    my $ret = '';
    my $flag = 1;
    my $same_line = 0;
    if ($Libconf::Templates::Trimspaces) {
        $text =~ s/( )+/$1/g; #SMALL BUG : when the string contains "\t   " it returns " ". Should we keep \t ?
        $text =~ s/^\s*//;
        $text =~ s/\s*$//;
    }
    $indentation = $Libconf::Templates::Indentspaces x $indentation;
    if (defined $comments) {
        for (my $i; $i < @$comments; $i++) {
            my $comment = $comments->[$i];
            if ($comment->{inline} == -1) {
                $ret .= $indentation . $comment->{comment};
                $same_line = 1;
                if ($i == @$comments-1) {
                    $ret .= " $text" . "\n";
                    $flag = 0;
                }
            } elsif ($comment->{inline} == 0) {
                $ret .= $indentation . $comment->{comment} . "\n";
            } elsif ($comment->{inline} == 1) {
                if ($same_line) {
                    $same_line = 0;
                } else {
                    $ret .= $indentation;
                }
                if ($i == @$comments-1) {
                    $ret .= "$text " . $comment->{comment} . "\n";
                    $flag = 0;
                } else {
                    $ret .= $comment->{comment} . "\n";
                }
            }
        }
    }
    $flag && length($text) and $ret .= $indentation . $text . "\n";
    $ret;
}

#--------- atom manipulation public methods --------

=item B<edit_atom($index, $struct1, $struct2)>

 $template->edit_atom(5, { key => 'AUTOLOGIN',
                           value => 'no' }
                     );
 $template->edit_atom(-1, { key => 'daemon' },
                          { values => { passwd => 'x',
                                        GID => 2,
                                        user_list => 'root,bin,daemon,ldap,rpc'
                                      }
                          });
 $template->edit_atom(-1, { values => { foo => 'some value',
                                      }
                          },
                          { values => { foo => 'some value',
                                        bar => 'an other value',
                                      }
                          });
 $template->edit_atom(-1, { list => [ 'some value', 2 ] }, { list => [ 'foo', 2, 3, 'an other value' ] });
 ...

B<arguments>

$index [B<type> : INTEGER] : Specify the atom to edit (first atom is 0). If $index = -1, the method will
try to find the atom that is the most similar to $struct, then edit it.

$struct1 [B<type> : HASH_REF] : If $index is NOT -1, this structure contains
the new properties of the atom at $index. Properties are added or overwritten,
but not removed from the atom.
If $index is equal to -1, then this struct contains the properties to be looked
for in the atom lists of the template.

$struct2 [B<type> : HASH_REF] : Used only if $index is equal to -1, this
structure contains the new properties of the atom at $index. Properties are
added or overwritten, but not removed from the atom.

B<description>

This method allows to edit one atom, by setting its properties to the struct
ones. It returns the atom index that has been modified (usefull when -1 is
passed), or -2 if $index was equal to -1 but no atom where found.

=cut

sub edit_atom {
    my ($self, $index, $search_struct, $replacement_struct) = @_;
    $self->_atom_edition($index, $search_struct, deepcopy($replacement_struct));
}

sub _atom_edition {
    my ($self, $index, $search_struct, $replacement_struct) = @_;
    if ($index == -1) {
        defined $replacement_struct or
          die 'you are using the old edit_atom API. edit_atom should be called : edit_atom($search_struct, $replacement_struct). See the doc';
        ($index = $self->find_atom_pos($search_struct)) >=0 or return -2;
    } else {
        $replacement_struct ||= $search_struct;
    }
    put_in_hash(@{$self->{atoms}}[$index], $replacement_struct);
    return $index;
}

=item B<append_atom($atom)>

  $template->append_atom({type => 'KEYVALUE', key => 'foo', value => $value });

B<arguments>

$atom [B<type> : HASH_REF ] : reference on an atom structure.

B<description>

appends an atom at the end of the template

=cut

sub append_atom {
    my ($self, $struct) = @_;
    push(@{$self->{atoms}}, _new_atom());
    $self->edit_atom($#{$self->{atoms}}, $struct);
}

=item B<insert_atom($index, $atom)>

  $template->insert_atom($position, { type => 'KEY_VALUE', sections => [ @depth ], key => $key, value => $value} );

B<arguments>

$index [B<type> : INTEGER ] : position where to insert the atom

$atom [B<type> : HASH_REF ] : reference on an atom structure.

B<description>

inserts the given atom at the given position. atoms are shift to the right.

=cut

sub insert_atom {
    my ($self, $index, $struct) = @_;
    $index >= @{$self->{atoms}} and return $self->append_atom($struct);
    $struct->{sections} ||= @{$self->{atoms}}[$index-1]->{sections};
    $struct->{sections} ||= [];
    splice(@{$self->{atoms}}, $index, 0, _new_atom($struct));
}

=item B<get_atom($atom_pos)>

  my $atom = $template->get_atom($atom_pos)

B<arguments>

$atom_pos [B<type> : INTEGER ] : position of the atom.

B<description>

Returns the atom located at the given position.

=cut

sub get_atom {
    my ($self, $index) = @_;
    $self->{atoms}[$index];
}

=item B<get_size()>

  my $size = $template->get_size();

B<description>

returns the number of atoms of the template.

=cut

sub get_size {
    my ($self) = @_;
    scalar(@{$self->{atoms}});
}

=item B<get_section_size($section_atom_pos)>

  my $section_size = $template->get_section_size($section_atom_pos)

B<arguments>

$section_atom_pos [B<type> : INTEGER ] : position of the section.

B<description>

Given the atom position of the beginning of a section, returns the number of
atoms of the section, including the 'SECTION' and 'ENDSECTION' (if available)
atoms.

=cut

sub get_section_size {
    my ($self, $start_atom) = @_;
    $self->get_section_end_atom($start_atom) - $start_atom + 1;
}

=item B<get_section_end_atom($section_atom_pos)>

  my $last_atom = $template->get_section_end_atom($section_atom_pos)

B<arguments>

$section_atom_pos [B<type> : INTEGER ] : position of the section.

B<description>

Given the atom position of the beginning of a section, returns the position of the
last atom of the section, (usually the ENDSECTION atom if the sections have ENDSECTIONS).

=cut

sub get_section_end_atom {
    my ($self, $start_atom) = @_;
    my $atom = $self->get_atom($start_atom);
    my @depth = @{$atom->{sections}};
    push @depth, { name => $atom->{section_name} };

    my $end_atom = $start_atom+1;
    foreach ($start_atom+1..$self->get_size()-1) {
        my $atom = $self->get_atom($_);
        @{$atom->{sections}} > @depth and next;
        #@{$atom->{sections}} >= @depth or next;
        Libconf::compare_depth($atom->{sections}, \@depth) or return $end_atom-1;
        $atom->{type} eq 'ENDSECTION' and return $end_atom;
    } continue {
        $end_atom++;
    }
    $end_atom;
}

=item B<clear()>

  $template->clear();

B<description>

Clears the atoms of the templates. The templates is then empty of any
configuration informations. But it keeps its settings.

=cut

sub clear {
    my ($self) = @_;
    $self->{atoms} = [];
}

=item B<delete_atom()>

  $template->delete_atom(5);

B<description>

removes the atom at the given position.

=cut

sub delete_atom {
    my ($self, $index) = @_;
    splice(@{$self->{atoms}}, $index, 1);
}

=item find_atom_pos($struct)

=item find_atom_pos($struct, $first_atom)

=item find_atom_pos($struct, $first_atom, $last_atom)


 my @atom_pos = $template->find_atom_pos({key => 'AUTOLOGIN', value => 'yes'});
 my $last_atom_pos = $template->find_atom_pos({key => 'AUTOLOGIN', value => 'yes'});
 my $last_atom_pos = $template->find_atom_pos({key => 'AUTOLOGIN', value => 'yes'}, 5, 10);

B<arguments>

$struct [B<type> : HASH_REF] : This hash ref contains the properties that the atom looked for should be compliant to.

$first_atom [B<type> : INTEGER, B<default> : 0 ] : If specified, the search is started from this position.

$last_atom_atom [B<type> : INTEGER, B<default> : scalar(@{$self->{atoms}}) ] : If specified, the search is stopped at this position.

B<description>

This function allows to find the position of an atom, given a struct that
describes the atom, and an optional interval. In list context, it returns the
list of the positions of the atoms that match the $struct criteria. In scalar
context, returns the last atom position to meet the criteria.

=cut

sub find_atom_pos {
    my ($self, $search_struct, $first_atom, $last_atom) = @_;
    defined $first_atom or $first_atom = 0;
    defined $last_atom or $last_atom = @{$self->{atoms}};
    my @res = ();
    foreach my $i ($first_atom..$last_atom) {
        my $atom = $self->{atoms}->[$i];
        deepcompare_($search_struct, $atom) and push(@res, $i);
    }
    wantarray ? @res : $res[-1];
}

=item B<delete_section($section_pos, $no_end_section)>

  # deletes the 5th section. This section has no ENDSECTION atom to indicate its termination.
  $template->delete_section(4, 1);

  # deletes the 1st section. This is a normal section, with ENDSECTION termination
  $template->delete_section(0);

  $nb_atom -= $template->delete_section(4);

  my @removed_atom = $template->delete_section(4);

B<arguments>

$section_pos [B<type> : INTEGER ] : position of the section to delete.

$no_end_section [B<type> : BOOLEAN ] : if 1, tells that the section has no
ENDSECTION atom to indicate its termination. The end of the section is detected
by the start of another section.

B<description>

deletes the given section. The section and the atoms it contains are removed
from the template. Returns the list of atoms removed.

=cut

sub deleteSection {
    my ($self, $index, $no_end_section) = @_;
    my $atom = $self->get_atom($index);
    my @end_positions = $no_end_section ?
      $self->find_atom_pos( { type => 'SECTION' }) :
        $self->find_atom_pos( { type => 'ENDSECTION', sections => $atom->{sections} });
    my $end;
    foreach (@end_positions) {
        $_ > $index and $end = $_, last;
    }
    !defined $end && $no_end_section and $end = @{$self->{atoms}}-1;
    defined $end or die "no ENDSECTION for SECTION n� $index";
    my @ret = splice(@{$self->{atoms}}, $index, $end-$index+1);
    @ret;
}

=item B<clear_section($section_pos, $no_end_section)>

  # clears the 5th section. This section has no ENDSECTION atom to indicate its termination.
  $template->clear_section(4, 1)

  # clears the 1st section. This is a normal section, with ENDSECTION termination
  $template->clear_section(1) 

  $nb_atom -= $template->clear_section(4);

  my @removed_atom = $template->clear_section(4);

B<arguments>

$section_pos [B<type> : INTEGER ] : position of the section to delete.

$no_end_section [B<type> : BOOLEAN ] : if 1, tells that the section has no
ENDSECTION atom to indicate its termination. The end of the section is detected by the start of another section.

B<description>

clears the given section. The atoms it contains are removed, but the section is
still kept, empty. Returns the list of atoms removed.

=cut

sub clearSection {
    my ($self, $index, $no_end_section) = @_;
    my $atom = $self->get_atom($index);
    my @end_positions = $no_end_section ?
      $self->find_atom_pos( { type => 'SECTION' }) :
        $self->find_atom_pos( { type => 'ENDSECTION', sections => $atom->{sections} });
    my $end;
    foreach (@end_positions) {
        $_ > $index and $end = $_, last;
    }
    !defined $end && $no_end_section and $end = @{$self->{atoms}}-1;
    defined $end or die "no ENDSECTION for SECTION n� $index";
    my @ret = splice(@{$self->{atoms}}, $index+1, $end-$index);
    @ret;
}

=item B<match_in_comments($expression, $evaluate_expression)>

  # will perform the regexp /\Qfoo.*bar\E/ on the templates comments
  my $is_in_comment = $template->match_in_struct_comments('foo.*bar')

  # will perform the regexp /foo.*bar/ on the templates comments
  my $is_in_comment = $template->match_in_struct_comments('foo.*bar', 1)

B<arguments>

$expression [B<type> : STRING ] : expression to look for in the template.

$evaluate_expression [B<type> : BOOLEAN ] : if 1, tells that the expression
should not be escaped when passed to the regexp matcher.

B<description>

returns 1 if the given expression matches in any templates comments.
If $evaluate_expression is true, EXPR will be evaluated in the pattern matching.

=cut

sub match_in_struct_comments {
    my ($self, $expr, $eval_expr) = @_;
    foreach my $atom (@{$self->{atoms}}) {
        foreach my $comment (@{$atom->{comments}}) {
            if ($eval_expr) {
                $comment->{comment} =~ /\Q$expr\E/ and return 1;
            } else {
                $comment->{comment} =~ /$expr/ and return 1;
            }
        }
    }
    0;
}

=item B<set_uniq()>

    my $template = new Libconf::Templates::Samba({filename => 'smb.conf'});
    $template->read_conf();

    # will ensure there is no duplicated atom. the keys are the element of the
    # hash that need to be compared
    $libconf->setUniq(qw(type key sections));

B<description>

This function removes dupliacted atoms from a $template structure. In list
context, it returns the list of the positions of the atoms that has been
removed. In scalar context, returns the first atom that has been removed (this is not really
useful)

=cut

sub set_uniq {
    my ($self, @keys) = @_;
    @keys > 0 or die 'you need to specify at least one key to compare elements';
    my @to_delete;
    my $i = 0;
    foreach my $atom (@{$self->{atoms}}) {
        my %atom2;
        @atom2{@keys} = deepcopy(@{%$atom}{@keys});
        my @pos = $self->find_atom_pos(\%atom2);
        if (@pos > 1 && $pos[-1] != $i) {
            push @to_delete, $i;
        }
        $i++;
    }
    # reversed to delete down -> up, so that index are still valid
    delete_atom($self, $_) foreach reverse @to_delete;
}

=item B<toXMLString($format)>

    $template->toXMLString(1);

B<description>

toXMLFile exports the template informations into an XML string.

The optional $format parameter sets the indenting of the output. This parameter
is expected to be an integer value, that specifies that indentation
should be used. The format parameter can have three different values
if it is used:

If $format is 0, than the document is dumped as it was originally
parsed

If $format is 1, libxml2 will add ignorable whitespaces, so the
nodes content is easier to read. Existing text nodes will not be
altered

If $format is 2 (or higher), libxml2 will act as $format == 1 but
it add a leading and a trailing linebreak to each text node.

=cut

sub toXMLString {
    my ($self, $format) = @_;
    toXML($self)->toString($format);
}

=item B<toXMLFile($filename, $format)>

    $template->toXMLFile('xml_filename', 1);

B<description>

toXMLFile exports the template informations into an XML file.

The optional $format parameter sets the indenting of the output. This parameter
is expected to be an integer value, that specifies that indentation
should be used. The format parameter can have three different values
if it is used:

If $format is 0, than the document is dumped as it was originally
parsed

If $format is 1, libxml2 will add ignorable whitespaces, so the
nodes content is easier to read. Existing text nodes will not be
altered

If $format is 2 (or higher), libxml2 will act as $format == 1 but
it add a leading and a trailing linebreak to each text node.


=cut

sub toXMLFile {
    my ($self, $file, $format) = @_;
    toXML($self)->toFile($file, $format);
}

sub toXML {
    my ($self) = @_;
    require XML::LibXML;
    # new XML document
    my $doc = XML::LibXML::Document->new();
    $doc->setEncoding('UTF8');

    # element : config-file
    $doc->setDocumentElement(my $config_file = $doc->createElement('CONFIG-FILE'));
    $config_file->setAttribute(file_name => $self->{filename});
    $config_file->setAttribute(template_name => $self->{template_name});

    my @current_nodes = ($config_file);
    my $current_sections = [];


    foreach my $atom (@{$self->{atoms}}) {
        my $current_node = $current_nodes[-1];
        my ($child, $child2);
        defined $current_node or die "problem in xml generation or structure malformed";
        if      ($atom->{type} eq 'SECTION') {
            if (!compare_depth($atom->{sections}, $current_sections)) {
                @current_nodes > 1 and pop @current_nodes;
                $current_node = $current_nodes[-1];
            };
            $current_node->appendChild($child = $doc->createElement('SECTION'));
            $child->setAttribute(name => $atom->{section_name});
            push @current_nodes, $child;
        } elsif ($atom->{type} eq 'KEY_VALUE') {
            $current_node->appendChild($child = $doc->createElement('KEY_VALUE'));
            $child->setAttribute(key => $atom->{key});
            $child->appendText($atom->{value});
        } elsif ($atom->{type} eq 'KEY_VALUES') {
            $current_node->appendChild($child = $doc->createElement('KEY_VALUES'));
            $child->setAttribute(key => $atom->{key});
            while (my ($key, $value) = each %{$atom->{values}}) {
                $child->appendChild($child2 = $doc->createElement('KEY_VALUE'));
                $child2->setAttribute(key => $key);
                $child2->appendText($value);
            }
        } elsif ($atom->{type} eq 'ENDSECTION') {
            pop @current_nodes;
        } elsif ($atom->{type} eq 'KEY_LIST') {
            $current_node->appendChild($child = $doc->createElement('KEY_LIST'));
            $child->setAttribute(key => $atom->{key});
            foreach my $value (@{$atom->{list}}) {
                $child->appendChild($child2 = $doc->createElement('VALUE'));
                $child2->appendText($value);
            }
        }
        if (exists $atom->{comments} and @{$atom->{comments}}) {
            defined $child or $current_node->appendChild($child = $doc->createElement('VOID'));
            $child->appendChild($child2 = $doc->createElement('COMMENTS'));
            foreach my $comment (@{$atom->{comments}}) {
                $child2->appendChild(my $child3 = $doc->createElement('COMMENT'));
                $child3->appendText($comment->{comment});
                $comment->{inline} and $child3->setAttribute(inline => $comment->{inline} == 1 ? 'RIGHT' : 'LEFT' );
            }
        }
        exists $atom->{sections} and $current_sections = $atom->{sections};
    }
    $doc;
}

sub init_args {
    my ($self, $args, $defaults) = @_;
    put_in_hash($self, $args);
    add2hash_($self, $defaults);

#    while (my ($key,$value) = each %$defaults) {
#        exists $self->{$key} or $self->{$key} = $value;
#    }
#    $self;
}







#--------- private functions ----------------

# create an new atom

sub _new_atom {
    my ($struct) = @_;
    $struct ||= {};
    my $atom = {
                type => 'VOID',
                sections => [ ],
               };
    while (my ($key, $value) = each %$struct) {
        $atom->{$key} = $value;
    }
    $atom;
}

#--------- generic private functions --------

# generic _output_header function for children
sub _output_header { ('', 0) }

# generic _output_atom function for children
sub _output_atom {
    my ($self, $atom) = @_;
    # if the type is 'UNKNOWN' (but not undef), we produce default output
    $atom->{type} eq 'UNKNOWN' and return (($atom->{value}), $parser_env->{current_indentation});
    # if the type is 'VOID' (default atom with no value, but maybe comments), we produce void output
    $atom->{type} eq 'VOID' and return ('', $parser_env->{current_indentation});
    # else we die, we have no valid rule to handle the atom.
    die 'No valid rule to handle atom type "' . $atom->{type} . "\"\n";
}

# generic _output_footer function for children
sub _output_footer { ('', 0) }

#--------- dummy functions ------------------

# dummy functions for compatibility

sub editAtom {
    warn "editAtom is OBSOLETE, you should use edit_atom now. editAtom will be removed soon.\n";
    shift->edit_atom(@_);
}

sub findAtomPos {
    warn "findAtomPos is OBSOLETE, you should use find_atom_pos now. findAtomPos will be removed soon.\n";
    shift->find_atom_pos(@_);
}

sub appendAtom {
    warn "appendAtom is OBSOLETE, you should use append_atom now. appendAtom will be removed soon.\n";
    shift->append_atom(@_);
}

sub insertAtom {
    warn "insertAtom is OBSOLETE, you should use insert_atom now. insertAtom will be removed soon.\n";
    shift->insert_atom(@_);
}

sub getAtom {
    warn "getAtom is OBSOLETE, you should use get_atom now. getAtom will be removed soon.\n";
    shift->get_atom(@_);
}

sub readConf {
    warn "readConf is OBSOLETE, you should use read_conf now. readConf will be removed soon.\n";
    shift->read_conf(@_);
}

sub writeConf {
    warn "writeConf is OBSOLETE, you should use write_conf now. writeConf will be removed soon.\n";
    shift->write_conf(@_);
}

=back

=head1 SEE ALSO

=over 

=item .

libconf samples : L<Libconf::Samples>

=item .

templates : L<Libconf::Templates::Generic::KeyValue>, L<Libconf::Templates::Shell>, L<Libconf::Templates::Group>

=back

=cut

1;
