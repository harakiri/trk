package Net::DBus::Object;

use 5.006;
use strict;
use warnings;
use Carp;

our $VERSION = '0.0.1';

use Net::DBus::RemoteObject;
use Net::DBus::Binding::Message::Error;
use Net::DBus::Binding::Message::MethodReturn;

sub new {
    my $class = shift;
    my $self = {};
    
    $self->{object_path} = shift;

    my $methods = shift;
    $self->{methods} = {};
    map { $self->{methods}->{$_} = 1 } @{$methods};

    $self->{service} = shift;

    bless $self, $class;

    $self->{service}->{bus}->{connection}->
	register_object_path($self->{object_path},
			     sub {
				 $self->_dispatch(@_);
			     });
    
    return $self;
}


sub emit_signal {
    my $self = shift;
    my $interface = shift;
    my $signal_name = shift;
    my $signal = Net::DBus::Binding::Message::Signal->new(object_path => $self->{object_path},
							  interface => $interface, 
							  signal_name => $signal_name);
    my $iter = $signal->iterator;
    foreach my $ret (@_) {
	$iter->append($ret);
    }
    $self->{service}->get_bus()->get_connection()->send($signal);
}   

sub _dispatch {
    my $self = shift;
    my $connection = shift;
    my $message = shift;

    my $method_name = $message->get_member;
    my @args = $message->get_args_list;

    my $reply;
    if ($self->can($method_name)) {
	my @ret = eval {
	    $self->$method_name(@args);
	};
	if ($@) {
	    $reply = Net::DBus::Binding::Message::Error->new(replyto => $message,
							     name => "org.freedesktop.DBus.Error.Failed",
							     description => $@);
	} else {
	    $reply = Net::DBus::Binding::Message::MethodReturn->new(call => $message);
	    if (@ret == 1) {
		my $iter = $reply->iterator;
		$iter->append(shift @ret);
	    } elsif (@ret > 1) {
		my $iter = $reply->iterator;
		foreach my $ret (@ret) {
		    $iter->append($ret);
		}
	    }
	}
    } else {
	$reply = Net::DBus::Binding::Message::Error->new(replyto => $message,
							 name => "org.freedesktop.DBus.Error.Failed",
							 description => "No such method " . ref($self) . "->" . $method_name);
    }
    
    $self->{service}->{bus}->{connection}->send($reply);
}

1;
