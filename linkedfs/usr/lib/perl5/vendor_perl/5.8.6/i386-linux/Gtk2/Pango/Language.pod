=head1 NAME

Gtk2::Pango::Language

=head1 HIERARCHY

  Glib::Boxed
  +----Gtk2::Pango::Language


=head1 METHODS

=head2 language = Gtk2::Pango::Language-E<gt>B<from_string> ($language)

=over

=over

=item * $language (string) 

=back

=back

=head2 boolean = $language-E<gt>B<includes_script> ($script)

=over

=over

=item * $script (Gtk2::Pango::Script) 

=back

=back

=head2 boolean = $language-E<gt>B<matches> ($range_list)

=over

=over

=item * $range_list (string) 

=back

=back

=head2 string = $language-E<gt>B<to_string> 

=over

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Boxed>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

