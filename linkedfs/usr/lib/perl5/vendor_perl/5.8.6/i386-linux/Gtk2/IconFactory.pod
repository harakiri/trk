=head1 NAME

Gtk2::IconFactory

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::IconFactory


=head1 METHODS

=head2 iconfactory = Gtk2::IconFactory-E<gt>B<new> 

=over

=back

=head2 $factory-E<gt>B<add> ($stock_id, $icon_set)

=over

=over

=item * $stock_id (string) 

=item * $icon_set (Gtk2::IconSet) 

=back

=back

=head2 $factory-E<gt>B<add_default> 

=over

=back

=head2 iconset = $factory-E<gt>B<lookup> ($stock_id)

=over

=over

=item * $stock_id (string) 

=back

=back

=head2 iconset = Gtk2::IconFactory-E<gt>B<lookup_default> ($stock_id)

=over

=over

=item * $stock_id (string) 

=back

=back

=head2 $factory-E<gt>B<remove_default> 

=over

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

