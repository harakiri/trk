=head1 NAME

Gtk2::CellLayout


=head1 METHODS

=head2 $cell_layout-E<gt>B<add_attribute> ($cell, $attribute, $column)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=item * $attribute (string) 

=item * $column (integer) 

=back

=back

=head2 $cell_layout-E<gt>B<set_attributes> ($cell, ...)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=item * ... (list) 

=back

=back

=head2 $cell_layout-E<gt>B<set_cell_data_func> ($cell, $func, $func_data=undef)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=item * $func (scalar) 

=item * $func_data (scalar) 

=back

=back

=head2 $cell_layout-E<gt>B<clear> 

=over

=back

=head2 $cell_layout-E<gt>B<clear_attributes> ($cell)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=back

=back

=head2 $cell_layout-E<gt>B<pack_end> ($cell, $expand)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=item * $expand (boolean) 

=back

=back

=head2 $cell_layout-E<gt>B<pack_start> ($cell, $expand)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=item * $expand (boolean) 

=back

=back

=head2 $cell_layout-E<gt>B<reorder> ($cell, $position)

=over

=over

=item * $cell (Gtk2::CellRenderer) 

=item * $position (integer) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

