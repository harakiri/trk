=head1 NAME

Gtk2::RadioAction

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Action
        +----Gtk2::ToggleAction
              +----Gtk2::RadioAction


=head1 METHODS

=head2 integer = $action-E<gt>B<get_current_value> 

=over

=back

=head2 list = $action-E<gt>B<get_group> 

=over

=back

=head2 $action-E<gt>B<set_group> ($member_or_listref)

=over

=over

=item * $member_or_listref (scalar) 

=back

=back


=head1 PROPERTIES

=over

=item 'group' (Gtk2::RadioAction : writable)

The radio action whose group this action belongs to.

=item 'value' (integer : readable / writable)

The value returned by gtk_radio_action_get_current_value() when this action is the current action of its group.

=back


=head1 SIGNALS

=over

=item B<changed> (Gtk2::RadioAction, Gtk2::RadioAction)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Action>, L<Gtk2::ToggleAction>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

