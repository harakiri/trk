=head1 NAME

Gtk2::Layout

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Layout

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::Layout-E<gt>B<new> ($hadjustment=undef, $vadjustment=undef)

=over

=over

=item * $hadjustment (Gtk2::Adjustment or undef) 

=item * $vadjustment (Gtk2::Adjustment or undef) 

=back

=back

=head2 $layout-E<gt>B<freeze> 

=over

=back

=head2 adjustment = $layout-E<gt>B<get_hadjustment> 

=over

=back

=head2 $layout-E<gt>B<set_hadjustment> ($adjustment)

=over

=over

=item * $adjustment (Gtk2::Adjustment) 

=back

=back

=head2 $layout-E<gt>B<move> ($child_widget, $x, $y)

=over

=over

=item * $child_widget (Gtk2::Widget) 

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 $layout-E<gt>B<put> ($child_widget, $x, $y)

=over

=over

=item * $child_widget (Gtk2::Widget) 

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 (width, height) = $layout-E<gt>B<get_size> 

=over

=back

=head2 $layout-E<gt>B<set_size> ($width, $height)

=over

=over

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $layout-E<gt>B<thaw> 

=over

=back

=head2 adjustment = $layout-E<gt>B<get_vadjustment> 

=over

=back

=head2 $layout-E<gt>B<set_vadjustment> ($adjustment)

=over

=over

=item * $adjustment (Gtk2::Adjustment) 

=back

=back


=head1 PROPERTIES

=over

=item 'hadjustment' (Gtk2::Adjustment : readable / writable)

The GtkAdjustment for the horizontal position

=item 'height' (Glib::UInt : readable / writable)

The height of the layout

=item 'vadjustment' (Gtk2::Adjustment : readable / writable)

The GtkAdjustment for the vertical position

=item 'width' (Glib::UInt : readable / writable)

The width of the layout

=back


=head1 SIGNALS

=over

=item B<set-scroll-adjustments> (Gtk2::Layout, Gtk2::Adjustment, Gtk2::Adjustment)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

