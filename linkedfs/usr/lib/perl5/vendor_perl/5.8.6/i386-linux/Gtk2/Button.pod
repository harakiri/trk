=head1 NAME

Gtk2::Button

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Bin
                          +----Gtk2::Button

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface

=head1 MNEMONICS

Mnemonics are "memory aids"; in GTK+, a mnemonic is an underlined character
which corresponds to a keyboard accelerator.  For a button, that means pressing
Alt and that key activates the button.

For convenience, Gtk2-Perl uses mnemonics by default on widgets that support
them.  If characters in label string are preceded by an underscore, they are
underlined.  If you need a literal underscore character in a label, use '__'
(two underscores).  If you don't want to use mnemonics at all, use the
non-mnemonic version explicitly (e.g. C<Gtk2::Button::new_with_label>).

=cut




=head1 METHODS

=head2 widget = Gtk2::Button->B<new>

=head2 widget = Gtk2::Button->B<new> ($mnemonic)

=over

=over

=item * $mnemonic (string) used to label the widget, see L</MNEMONICS>

=back



=back

=head2 widget = Gtk2::Button-E<gt>B<new_from_stock> ($stock_id)

=over

=over

=item * $stock_id (string) creates a new button using the icon and text from the

=back


specified stock item, see L<Gtk2::Stock>


=back

=head2 widget = Gtk2::Button->B<new_with_label> ($label)

=over

=over

=item * $label (string) used to label the widget

=back



=back

=head2 widget = Gtk2::Button->B<new_with_mnemonic> ($mnemonic)

=over

=over

=item * $mnemonic (string) used to label the widget, see L</MNEMONICS>

=back



=back

=head2 (xalign, yalign) = $button-E<gt>B<get_alignment> 

=over

=back

=head2 $button-E<gt>B<set_alignment> ($xalign, $yalign)

=over

=over

=item * $xalign (double) 

=item * $yalign (double) 

=back

=back

=head2 $button-E<gt>B<clicked> 

=over

=back

=head2 $button-E<gt>B<enter> 

=over

=back

=head2 boolean = $button-E<gt>B<get_focus_on_click> 

=over

=back

=head2 $button-E<gt>B<set_focus_on_click> ($focus_on_click)

=over

=over

=item * $focus_on_click (boolean) 

=back

=back

=head2 widget or undef = $button-E<gt>B<get_image> 

=over

=back

=head2 $button-E<gt>B<set_image> ($image)

=over

=over

=item * $image (Gtk2::Widget or undef) 

=back

=back

=head2 string = $button-E<gt>B<get_label> 

=over

=back

=head2 $button-E<gt>B<set_label> ($label)

=over

=over

=item * $label (string) 

=back

=back

=head2 $button-E<gt>B<leave> 

=over

=back

=head2 $button-E<gt>B<pressed> 

=over

=back

=head2 $button-E<gt>B<released> 

=over

=back

=head2 reliefstyle = $button-E<gt>B<get_relief> 

=over

=back

=head2 $button-E<gt>B<set_relief> ($newstyle)

=over

=over

=item * $newstyle (Gtk2::ReliefStyle) 

=back

=back

=head2 boolean = $button-E<gt>B<get_use_stock> 

=over

=back

=head2 $button-E<gt>B<set_use_stock> ($use_stock)

=over

=over

=item * $use_stock (boolean) 

=back

=back

=head2 boolean = $button-E<gt>B<get_use_underline> 

=over

=back

=head2 $button-E<gt>B<set_use_underline> ($use_underline)

=over

=over

=item * $use_underline (boolean) 

=back

=back


=head1 PROPERTIES

=over

=item 'focus-on-click' (boolean : readable / writable)

Whether the button grabs focus when it is clicked with the mouse

=item 'image' (Gtk2::Widget : readable / writable)

Child widget to appear next to the button text

=item 'label' (string : readable / writable / construct)

Text of the label widget inside the button, if the button contains a label widget

=item 'relief' (Gtk2::ReliefStyle : readable / writable)

The border relief style

=item 'use-stock' (boolean : readable / writable / construct)

If set, the label is used to pick a stock item instead of being displayed

=item 'use-underline' (boolean : readable / writable / construct)

If set, an underline in the text indicates the next character should be used for the mnemonic accelerator key

=item 'xalign' (Glib::Float : readable / writable)

Horizontal position of child in available space. 0.0 is left aligned, 1.0 is right aligned

=item 'yalign' (Glib::Float : readable / writable)

Vertical position of child in available space. 0.0 is top aligned, 1.0 is bottom aligned

=back


=head1 SIGNALS

=over

=item B<activate> (Gtk2::Button)

=item B<clicked> (Gtk2::Button)

=item B<pressed> (Gtk2::Button)

=item B<released> (Gtk2::Button)

=item B<enter> (Gtk2::Button)

=item B<leave> (Gtk2::Button)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Bin>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

