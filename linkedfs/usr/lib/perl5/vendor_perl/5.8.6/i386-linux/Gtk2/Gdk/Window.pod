=head1 NAME

Gtk2::Gdk::Window

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Gdk::Drawable
        +----Gtk2::Gdk::Window


=head1 METHODS

=head2 window = Gtk2::Gdk::Window-E<gt>B<new> ($parent, $attributes_ref)

=over

=over

=item * $parent (Gtk2::Gdk::Window or undef) 

=item * $attributes_ref (scalar) 

=back

=back

=head2 $window-E<gt>B<set_accept_focus> ($accept_focus)

=over

=over

=item * $accept_focus (boolean) 

=back

=back

=head2 (window, win_x, win_y) = Gtk2::Gdk::Window->B<at_pointer>

=over

Returns window, a Gtk2::Gdk::Window and win_x and win_y, integers.

=back

=head2 $window-E<gt>B<set_back_pixmap> ($pixmap, $parent_relative=0)

=over

=over

=item * $pixmap (Gtk2::Gdk::Pixmap or undef) 

=item * $parent_relative (boolean) 

=back

=back

=head2 $window-E<gt>B<set_background> ($color)

=over

=over

=item * $color (Gtk2::Gdk::Color) 

=back

=back

=head2 $window-E<gt>B<begin_move_drag> ($button, $root_x, $root_y, $timestamp)

=over

=over

=item * $button (integer) 

=item * $root_x (integer) 

=item * $root_y (integer) 

=item * $timestamp (unsigned) 

=back

=back

=head2 $window-E<gt>B<begin_paint_rect> ($rectangle)

=over

=over

=item * $rectangle (Gtk2::Gdk::Rectangle) 

=back

=back

=head2 $window-E<gt>B<begin_paint_region> ($region)

=over

=over

=item * $region (Gtk2::Gdk::Region) 

=back

=back

=head2 $window-E<gt>B<begin_resize_drag> ($edge, $button, $root_x, $root_y, $timestamp)

=over

=over

=item * $edge (Gtk2::Gdk::WindowEdge) 

=item * $button (integer) 

=item * $root_x (integer) 

=item * $root_y (integer) 

=item * $timestamp (unsigned) 

=back

=back

=head2 $window-E<gt>B<set_child_shapes> 

=over

=back

=head2 list = $window-E<gt>B<get_children> 

=over

Returns the list of children (Gtk2::Gdk::Window's) known to gdk.

=back

=head2 $window-E<gt>B<clear> 

=over

=back

=head2 $window-E<gt>B<clear_area> ($x, $y, $width, $height)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $window-E<gt>B<clear_area_e> ($x, $y, $width, $height)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $window-E<gt>B<configure_finished> 

=over

=back

=head2 $window-E<gt>B<set_cursor> ($cursor)

=over

=over

=item * $cursor (Gtk2::Gdk::Cursor or undef) 

=back

=back

=head2 Gtk2::Gdk::Window->B<set_debug_updates> ($enable)

=head2 $window->B<set_debug_updates> ($enable)

=over

=over

=item * $enable (boolean) 

=back



=back

=head2 (boolean, decorations) = $window-E<gt>B<get_decorations> 

=over

=back

=head2 $window-E<gt>B<set_decorations> ($decorations)

=over

=over

=item * $decorations (Gtk2::Gdk::WMDecoration) 

=back

=back

=head2 $window-E<gt>B<deiconify> 

=over

=back

=head2 $window-E<gt>B<destroy> 

=over

=back

=head2 $window-E<gt>B<enable_synchronized_configure> 

=over

=back

=head2 $window-E<gt>B<end_paint> 

=over

=back

=head2 eventmask = $window-E<gt>B<get_events> 

=over

=back

=head2 $window-E<gt>B<set_events> ($event_mask)

=over

=over

=item * $event_mask (Gtk2::Gdk::EventMask) 

=back

=back

=head2 $window-E<gt>B<focus> ($timestamp)

=over

=over

=item * $timestamp (unsigned) 

=back

=back

=head2 $window-E<gt>B<set_focus_on_map> ($focus_on_map)

=over

=over

=item * $focus_on_map (boolean) 

=back

=back

=head2 window = Gtk2::Gdk::Window-E<gt>B<foreign_new> ($anid)

=over

=over

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 window = Gtk2::Gdk::Window-E<gt>B<foreign_new_for_display> ($display, $anid)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 rectangle = $window-E<gt>B<get_frame_extents> 

=over

=back

=head2 $window-E<gt>B<freeze_updates> 

=over

=back

=head2 $window-E<gt>B<fullscreen> 

=over

=back

=head2 $window-E<gt>B<set_functions> ($functions)

=over

=over

=item * $functions (Gtk2::Gdk::WMFunction) 

=back

=back

=head2 $sm_client_id-E<gt>B<gdk_set_sm_client_id> 

=over

=back

=head2 (x, y, width, height, depth) = $window-E<gt>B<get_geometry> 

=over

=back

=head2 $window->B<set_geometry_hints> ($geometry)

=head2 $window->B<set_geometry_hints> ($geometry, $geom_mask)

=over

=over

=item * $geometry_ref (scalar) 

=item * $geom_mask_sv (scalar) 

=item * $geom_mask (Gtk2::Gdk::WindowHints) optional, usually inferred from I<$geometry>

=item * $geometry (Gtk2::Gdk::Geometry) 

=back




The geom_mask argument, describing which fields in the geometry are valid, is
optional.  If omitted it will be inferred from the geometry itself.


=back

=head2 window = $window-E<gt>B<get_group> 

=over

=back

=head2 $window-E<gt>B<set_group> ($leader)

=over

=over

=item * $leader (Gtk2::Gdk::Window) 

=back

=back

=head2 $window-E<gt>B<hide> 

=over

=back

=head2 $window-E<gt>B<set_icon_list> (...)

=over

=over

=item * ... (list) of GdkPixbuf's

=back




=back

=head2 $window-E<gt>B<set_icon_name> ($name)

=over

=over

=item * $name (string) 

=back

=back

=head2 $window-E<gt>B<set_icon> ($icon_window, $pixmap, $mask)

=over

=over

=item * $icon_window (Gtk2::Gdk::Window or undef) 

=item * $pixmap (Gtk2::Gdk::Pixmap or undef) 

=item * $mask (Gtk2::Gdk::Bitmap or undef) 

=back

=back

=head2 $window-E<gt>B<iconify> 

=over

=back

=head2 (real_drawable, x_offset, y_offset) = $window-E<gt>B<get_internal_paint_info> 

=over

=back

=head2 $window-E<gt>B<invalidate_maybe_recurse> ($region, $func, $data=undef)

=over

=over

=item * $region (Gtk2::Gdk::Region) 

=item * $func (scalar) 

=item * $data (scalar) 

=back

=back

=head2 $window-E<gt>B<invalidate_rect> ($rectangle, $invalidate_children)

=over

=over

=item * $rectangle (Gtk2::Gdk::Rectangle) 

=item * $invalidate_children (boolean) 

=back

=back

=head2 $window-E<gt>B<invalidate_region> ($region, $invalidate_children)

=over

=over

=item * $region (Gtk2::Gdk::Region) 

=item * $invalidate_children (boolean) 

=back

=back

=head2 boolean = $window-E<gt>B<is_viewable> 

=over

=back

=head2 boolean = $window-E<gt>B<is_visible> 

=over

=back

=head2 $window-E<gt>B<set_keep_above> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 $window-E<gt>B<set_keep_below> ($setting)

=over

=over

=item * $setting (boolean) 

=back

=back

=head2 window = Gtk2::Gdk::Window-E<gt>B<lookup> ($anid)

=over

=over

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 window = Gtk2::Gdk::Window-E<gt>B<lookup_for_display> ($display, $anid)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $anid (Gtk2::Gdk::NativeWindow) 

=back

=back

=head2 $window-E<gt>B<lower> 

=over

=back

=head2 $window-E<gt>B<maximize> 

=over

=back

=head2 $window-E<gt>B<merge_child_shapes> 

=over

=back

=head2 $window-E<gt>B<set_modal_hint> ($modal)

=over

=over

=item * $modal (boolean) 

=back

=back

=head2 $window-E<gt>B<move> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 $window-E<gt>B<move_resize> ($x, $y, $width, $height)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 (x, y) = $window-E<gt>B<get_origin> 

=over

=back

=head2 $window-E<gt>B<set_override_redirect> ($override_redirect)

=over

=over

=item * $override_redirect (boolean) 

=back

=back

=head2 window = $window-E<gt>B<get_parent> 

=over

=back

=head2 list = $window-E<gt>B<peek_children> 

=over

An alias for get_children

=back

=head2 (window_at_pointer, x, y, mask) = $window->B<get_pointer>

=over

Returns window_at_pointer, a Gtk2::Gdk::Window or undef, x and y, integers, and
mask, a Gtk2::Gdk::ModifierType.

=back

=head2 (x, y) = $window-E<gt>B<get_position> 

=over

=back

=head2 Gtk2::Gdk::Window->B<process_all_updates>

=head2 $window->B<process_all_updates>

=over



=back

=head2 $window-E<gt>B<process_updates> ($update_children)

=over

=over

=item * $update_children (boolean) 

=back

=back

=head2 $window-E<gt>B<property_change> ($property, $type, $format, $mode, ...)

=over

=over

=item * $property (Gtk2::Gdk::Atom) 

=item * $type (Gtk2::Gdk::Atom) 

=item * $format (integer) 

=item * $mode (Gtk2::Gdk::PropMode) 

=item * ... (list) property value(s)

=back



Depending on the value of I<format>, the property value(s) can be:

  +--------------------+------------------------------------+
  |      format        |                value               |
  +--------------------+------------------------------------+
  | Gtk2::Gdk::CHARS   | a string                           |
  | Gtk2::Gdk::USHORTS | one or more unsigned short numbers |
  | Gtk2::Gdk::ULONGS  | one or more unsigned long numbers  |
  +--------------------+------------------------------------+


=back

=head2 $window-E<gt>B<property_delete> ($property)

=over

=over

=item * $property (Gtk2::Gdk::Atom) 

=back

=back

=head2 (property_type, format, data) = $window->B<property_get> ($property, $type, $offset, $length, $pdelete)

=over

=over

=item * $property (Gtk2::Gdk::Atom) 

=item * $type (Gtk2::Gdk::Atom) 

=item * $offset (unsigned) 

=item * $length (unsigned) 

=item * $pdelete (integer) 

=back



See I<property_change> for an explanation of the meaning of I<format>.


=back

=head2 $window-E<gt>B<raise> 

=over

=back

=head2 $window-E<gt>B<register_dnd> 

=over

=back

=head2 $window-E<gt>B<reparent> ($new_parent, $x, $y)

=over

=over

=item * $new_parent (Gtk2::Gdk::Window) 

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 $window-E<gt>B<resize> ($width, $height)

=over

=over

=item * $width (integer) 

=item * $height (integer) 

=back

=back

=head2 $window-E<gt>B<set_role> ($role)

=over

=over

=item * $role (string) 

=back

=back

=head2 (x, y) = $window-E<gt>B<get_root_origin> 

=over

=back

=head2 $window-E<gt>B<scroll> ($dx, $dy)

=over

=over

=item * $dx (integer) 

=item * $dy (integer) 

=back

=back

=head2 $window-E<gt>B<shape_combine_mask> ($mask, $x, $y)

=over

=over

=item * $mask (Gtk2::Gdk::Bitmap) 

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 $window-E<gt>B<shape_combine_region> ($shape_region, $offset_x, $offset_y)

=over

=over

=item * $shape_region (Gtk2::Gdk::Region) 

=item * $offset_x (integer) 

=item * $offset_y (integer) 

=back

=back

=head2 $window-E<gt>B<show> 

=over

=back

=head2 $window-E<gt>B<show_unraised> 

=over

=back

=head2 $window-E<gt>B<set_skip_pager_hint> ($skips_pager)

=over

=over

=item * $skips_pager (boolean) 

=back

=back

=head2 $window-E<gt>B<set_skip_taskbar_hint> ($skips_taskbar)

=over

=over

=item * $skips_taskbar (boolean) 

=back

=back

=head2 windowstate = $window-E<gt>B<get_state> 

=over

=back

=head2 boolean = $window-E<gt>B<set_static_gravities> ($use_static)

=over

=over

=item * $use_static (boolean) 

=back

=back

=head2 $window-E<gt>B<stick> 

=over

=back

=head2 $window-E<gt>B<thaw_updates> 

=over

=back

=head2 $window-E<gt>B<set_title> ($title)

=over

=over

=item * $title (string) 

=back

=back

=head2 window = $window-E<gt>B<get_toplevel> 

=over

=back

=head2 list = Gtk2::Gdk::Window-E<gt>B<get_toplevels> 

=over

Returns a list of top level windows (Gtk2::Gdk::Window's) known to gdk, on the 
default screen. A toplevel window is a child of the root window.

=back

=head2 $window-E<gt>B<set_transient_for> ($parent)

=over

=over

=item * $parent (Gtk2::Gdk::Window) 

=back

=back

=head2 $window-E<gt>B<set_type_hint> ($hint)

=over

=over

=item * $hint (Gtk2::Gdk::WindowTypeHint) 

=back

=back

=head2 $window-E<gt>B<unfullscreen> 

=over

=back

=head2 $window-E<gt>B<unmaximize> 

=over

=back

=head2 $window-E<gt>B<unstick> 

=over

=back

=head2 region or undef = $window-E<gt>B<get_update_area> 

=over

=back

=head2 unsigned = $window-E<gt>B<get_user_data> 

=over

=back

=head2 $window-E<gt>B<set_user_data> ($user_data)

=over

=over

=item * $user_data (unsigned) 

=back

=back

=head2 $window-E<gt>B<set_user_time> ($timestamp)

=over

=over

=item * $timestamp (unsigned) 

=back

=back

=head2 windowtype = $window-E<gt>B<get_window_type> 

=over

=back

=head2 $window-E<gt>B<withdraw> 

=over

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back


=head2 flags Gtk2::Gdk::WMDecoration

=over

=item * 'all' / 'GDK_DECOR_ALL'

=item * 'border' / 'GDK_DECOR_BORDER'

=item * 'resizeh' / 'GDK_DECOR_RESIZEH'

=item * 'title' / 'GDK_DECOR_TITLE'

=item * 'menu' / 'GDK_DECOR_MENU'

=item * 'minimize' / 'GDK_DECOR_MINIMIZE'

=item * 'maximize' / 'GDK_DECOR_MAXIMIZE'

=back


=head2 flags Gtk2::Gdk::WMFunction

=over

=item * 'all' / 'GDK_FUNC_ALL'

=item * 'resize' / 'GDK_FUNC_RESIZE'

=item * 'move' / 'GDK_FUNC_MOVE'

=item * 'minimize' / 'GDK_FUNC_MINIMIZE'

=item * 'maximize' / 'GDK_FUNC_MAXIMIZE'

=item * 'close' / 'GDK_FUNC_CLOSE'

=back


=head2 flags Gtk2::Gdk::WindowHints

=over

=item * 'pos' / 'GDK_HINT_POS'

=item * 'min-size' / 'GDK_HINT_MIN_SIZE'

=item * 'max-size' / 'GDK_HINT_MAX_SIZE'

=item * 'base-size' / 'GDK_HINT_BASE_SIZE'

=item * 'aspect' / 'GDK_HINT_ASPECT'

=item * 'resize-inc' / 'GDK_HINT_RESIZE_INC'

=item * 'win-gravity' / 'GDK_HINT_WIN_GRAVITY'

=item * 'user-pos' / 'GDK_HINT_USER_POS'

=item * 'user-size' / 'GDK_HINT_USER_SIZE'

=back


=head2 flags Gtk2::Gdk::WindowState

=over

=item * 'withdrawn' / 'GDK_WINDOW_STATE_WITHDRAWN'

=item * 'iconified' / 'GDK_WINDOW_STATE_ICONIFIED'

=item * 'maximized' / 'GDK_WINDOW_STATE_MAXIMIZED'

=item * 'sticky' / 'GDK_WINDOW_STATE_STICKY'

=item * 'fullscreen' / 'GDK_WINDOW_STATE_FULLSCREEN'

=item * 'above' / 'GDK_WINDOW_STATE_ABOVE'

=item * 'below' / 'GDK_WINDOW_STATE_BELOW'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Gdk::Drawable>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

