=head1 NAME

Gtk2::Pango::Fontset

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Pango::Fontset


=head1 METHODS

=head2 font = $fontset-E<gt>B<get_font> ($wc)

=over

=over

=item * $wc (integer) 

=back

=back

=head2 $fontset-E<gt>B<foreach> ($func, $data=undef)

=over

=over

=item * $func (scalar) 

=item * $data (scalar) 

=back

=back

=head2 fontmetrics = $fontset-E<gt>B<get_metrics> 

=over

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

