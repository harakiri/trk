=head1 NAME

Gtk2::Adjustment

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Adjustment


=head1 METHODS

=head2 object = Gtk2::Adjustment-E<gt>B<new> ($value, $lower, $upper, $step_increment, $page_increment, $page_size)

=over

=over

=item * $value (double) 

=item * $lower (double) 

=item * $upper (double) 

=item * $step_increment (double) 

=item * $page_increment (double) 

=item * $page_size (double) 

=back

=back

=head2 $adjustment-E<gt>B<changed> 

=over

=back

=head2 $adjustment-E<gt>B<clamp_page> ($lower, $upper)

=over

=over

=item * $lower (double) 

=item * $upper (double) 

=back

=back

=head2 double = $adjustment-E<gt>B<lower> ($newval)

=over

=over

=item * $newval (double) 

=back

=back

=head2 double = $adjustment-E<gt>B<page_increment> ($newval)

=over

=over

=item * $newval (double) 

=back

=back

=head2 double = $adjustment-E<gt>B<page_size> ($newval)

=over

=over

=item * $newval (double) 

=back

=back

=head2 double = $adjustment-E<gt>B<step_increment> ($newval)

=over

=over

=item * $newval (double) 

=back

=back

=head2 double = $adjustment-E<gt>B<upper> ($newval)

=over

=over

=item * $newval (double) 

=back

=back

=head2 double = $adjustment-E<gt>B<value> ($newval=0)

=over

=over

=item * $newval (double) 

=back

=back

=head2 $adjustment-E<gt>B<value_changed> 

=over

=back

=head2 double = $adjustment-E<gt>B<get_value> 

=over

=back

=head2 $adjustment-E<gt>B<set_value> ($value)

=over

=over

=item * $value (double) 

=back

=back


=head1 PROPERTIES

=over

=item 'lower' (double : readable / writable)

The minimum value of the adjustment

=item 'page-increment' (double : readable / writable)

The page increment of the adjustment

=item 'page-size' (double : readable / writable)

The page size of the adjustment

=item 'step-increment' (double : readable / writable)

The step increment of the adjustment

=item 'upper' (double : readable / writable)

The maximum value of the adjustment

=item 'value' (double : readable / writable)

The value of the adjustment

=back


=head1 SIGNALS

=over

=item B<changed> (Gtk2::Adjustment)

=item B<value-changed> (Gtk2::Adjustment)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

