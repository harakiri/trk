=head1 NAME

Gtk2::Gdk::DisplayManager

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Gdk::DisplayManager


=head1 METHODS

=head2 display = $display_manager-E<gt>B<get_default_display> 

=over

=back

=head2 $display_manager-E<gt>B<set_default_display> ($display)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=back

=back

=head2 displaymanager = Gtk2::Gdk::DisplayManager-E<gt>B<get> 

=over

=back

=head2 list = $display_manager-E<gt>B<list_displays> 

=over

Returns a list of Gtk2::Gdk::Display's.

=back


=head1 PROPERTIES

=over

=item 'default-display' (Gtk2::Gdk::Display : readable / writable)

The default display for GDK

=back


=head1 SIGNALS

=over

=item B<display-opened> (Gtk2::Gdk::DisplayManager, Gtk2::Gdk::Display)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

