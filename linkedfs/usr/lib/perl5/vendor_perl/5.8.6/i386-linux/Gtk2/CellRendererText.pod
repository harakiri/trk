=head1 NAME

Gtk2::CellRendererText

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::CellRenderer
              +----Gtk2::CellRendererText


=head1 METHODS

=head2 cellrenderer = Gtk2::CellRendererText-E<gt>B<new> 

=over

=back

=head2 $renderer-E<gt>B<set_fixed_height_from_font> ($number_of_rows)

=over

=over

=item * $number_of_rows (integer) 

=back

=back


=head1 PROPERTIES

=over

=item 'attributes' (Gtk2::Pango::AttrList : readable / writable)

A list of style attributes to apply to the text of the renderer

=item 'background' (string : writable)

Background color as a string

=item 'background-gdk' (Gtk2::Gdk::Color : readable / writable)

Background color as a GdkColor

=item 'background-set' (boolean : readable / writable)

Whether this tag affects the background color

=item 'editable' (boolean : readable / writable)

Whether the text can be modified by the user

=item 'editable-set' (boolean : readable / writable)

Whether this tag affects text editability

=item 'ellipsize' (Gtk2::Pango::EllipsizeMode : readable / writable)

The preferred place to ellipsize the string, if the cell renderer does not have enough room to display the entire string, if at all

=item 'ellipsize-set' (boolean : readable / writable)

Whether this tag affects the ellipsize mode

=item 'family' (string : readable / writable)

Name of the font family, e.g. Sans, Helvetica, Times, Monospace

=item 'family-set' (boolean : readable / writable)

Whether this tag affects the font family

=item 'font' (string : readable / writable)

Font description as a string

=item 'font-desc' (Gtk2::Pango::FontDescription : readable / writable)

Font description as a PangoFontDescription struct

=item 'foreground' (string : writable)

Foreground color as a string

=item 'foreground-gdk' (Gtk2::Gdk::Color : readable / writable)

Foreground color as a GdkColor

=item 'foreground-set' (boolean : readable / writable)

Whether this tag affects the foreground color

=item 'language' (string : readable / writable)

The language this text is in, as an ISO code. Pango can use this as a hint when rendering the text. If you don't understand this parameter, you probably don't need it

=item 'language-set' (boolean : readable / writable)

Whether this tag affects the language the text is rendered as

=item 'markup' (string : writable)

Marked up text to render

=item 'rise' (integer : readable / writable)

Offset of text above the baseline (below the baseline if rise is negative)

=item 'rise-set' (boolean : readable / writable)

Whether this tag affects the rise

=item 'scale' (double : readable / writable)

Font scaling factor

=item 'scale-set' (boolean : readable / writable)

Whether this tag scales the font size by a factor

=item 'single-paragraph-mode' (boolean : readable / writable)

Whether or not to keep all text in a single paragraph

=item 'size' (integer : readable / writable)

Font size

=item 'size-points' (double : readable / writable)

Font size in points

=item 'size-set' (boolean : readable / writable)

Whether this tag affects the font size

=item 'stretch' (Gtk2::Pango::Stretch : readable / writable)

Font stretch

=item 'stretch-set' (boolean : readable / writable)

Whether this tag affects the font stretch

=item 'strikethrough' (boolean : readable / writable)

Whether to strike through the text

=item 'strikethrough-set' (boolean : readable / writable)

Whether this tag affects strikethrough

=item 'style' (Gtk2::Pango::Style : readable / writable)

Font style

=item 'style-set' (boolean : readable / writable)

Whether this tag affects the font style

=item 'text' (string : readable / writable)

Text to render

=item 'underline' (Gtk2::Pango::Underline : readable / writable)

Style of underline for this text

=item 'underline-set' (boolean : readable / writable)

Whether this tag affects underlining

=item 'variant' (Gtk2::Pango::Variant : readable / writable)

Font variant

=item 'variant-set' (boolean : readable / writable)

Whether this tag affects the font variant

=item 'weight' (integer : readable / writable)

Font weight

=item 'weight-set' (boolean : readable / writable)

Whether this tag affects the font weight

=item 'width-chars' (integer : readable / writable)

The desired width of the label, in characters

=back


=head1 SIGNALS

=over

=item B<edited> (Gtk2::CellRendererText, string, string)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::CellRenderer>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

