=head1 NAME

Gtk2::Gdk::PixbufAnimationIter

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Gdk::PixbufAnimationIter


=head1 METHODS

=head2 boolean = $iter-E<gt>B<advance> ($current_time_seconds=0, $current_time_microseconds=0)

=over

=over

=item * $current_time_seconds (integer) 

=item * $current_time_microseconds (integer) 

=back

=back

=head2 integer = $iter-E<gt>B<get_delay_time> 

=over

=back

=head2 boolean = $iter-E<gt>B<on_currently_loading_frame> 

=over

=back

=head2 pixbuf = $iter-E<gt>B<get_pixbuf> 

=over

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

