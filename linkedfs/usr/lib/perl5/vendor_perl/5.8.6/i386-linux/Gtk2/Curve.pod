=head1 NAME

Gtk2::Curve

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::DrawingArea
                    +----Gtk2::Curve

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::Curve-E<gt>B<new> 

=over

=back

=head2 $curve-E<gt>B<set_curve_type> ($type)

=over

=over

=item * $type (Gtk2::CurveType) 

=back

=back

=head2 $curve-E<gt>B<set_gamma> ($gamma)

=over

=over

=item * $gamma (double) 

=back

=back

=head2 $curve-E<gt>B<set_range> ($min_x, $max_x, $min_y, $max_y)

=over

=over

=item * $min_x (double) 

=item * $max_x (double) 

=item * $min_y (double) 

=item * $max_y (double) 

=back

=back

=head2 $curve-E<gt>B<reset> 

=over

=back

=head2 list = $curve-E<gt>B<get_vector> ($veclen=32)

=over

=over

=item * $veclen (integer) 

=back

Returns a list of real numbers, the curve's vector.

=back

=head2 $curve-E<gt>B<set_vector> (...)

=over

=over

=item * ... (list) of float's, the points of the curve

=back



=back


=head1 PROPERTIES

=over

=item 'curve-type' (Gtk2::CurveType : readable / writable)

Is this curve linear, spline interpolated, or free-form

=item 'max-x' (Glib::Float : readable / writable)

Maximum possible X value

=item 'max-y' (Glib::Float : readable / writable)

Maximum possible value for Y

=item 'min-x' (Glib::Float : readable / writable)

Minimum possible value for X

=item 'min-y' (Glib::Float : readable / writable)

Minimum possible value for Y

=back


=head1 SIGNALS

=over

=item B<curve-type-changed> (Gtk2::Curve)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::DrawingArea>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

