=head1 NAME

Gtk2::Accelerator


=head1 METHODS

=head2 modifiertype = Gtk2::Accelerator-E<gt>B<get_default_mod_mask> 

=over

=back

=head2 Gtk2::Accelerator-E<gt>B<set_default_mod_mask> ($default_mod_mask)

=over

=over

=item * $default_mod_mask (Gtk2::Gdk::ModifierType) 

=back

=back

=head2 string = Gtk2::Accelerator-E<gt>B<get_label> ($accelerator_key, $accelerator_mods)

=over

=over

=item * $accelerator_key (integer) 

=item * $accelerator_mods (Gtk2::Gdk::ModifierType) 

=back

=back

=head2 string = Gtk2::Accelerator-E<gt>B<name> ($accelerator_key, $accelerator_mods)

=over

=over

=item * $accelerator_key (integer) 

=item * $accelerator_mods (Gtk2::Gdk::ModifierType) 

=back

=back

=head2 (accelerator_key, accelerator_mods) = Gtk2::Accelerator->B<parse> ($accelerator)

=over

=over

=item * $accelerator (string) 

=back

Returns accelerator_key, an unsigned interger and accelerator_mods, a 
Gtk2::Gdk::ModifierType.

=back

=head2 boolean = Gtk2::Accelerator-E<gt>B<valid> ($keyval, $modifiers)

=over

=over

=item * $keyval (integer) 

=item * $modifiers (Gtk2::Gdk::ModifierType) 

=back

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::ModifierType

=over

=item * 'shift-mask' / 'GDK_SHIFT_MASK'

=item * 'lock-mask' / 'GDK_LOCK_MASK'

=item * 'control-mask' / 'GDK_CONTROL_MASK'

=item * 'mod1-mask' / 'GDK_MOD1_MASK'

=item * 'mod2-mask' / 'GDK_MOD2_MASK'

=item * 'mod3-mask' / 'GDK_MOD3_MASK'

=item * 'mod4-mask' / 'GDK_MOD4_MASK'

=item * 'mod5-mask' / 'GDK_MOD5_MASK'

=item * 'button1-mask' / 'GDK_BUTTON1_MASK'

=item * 'button2-mask' / 'GDK_BUTTON2_MASK'

=item * 'button3-mask' / 'GDK_BUTTON3_MASK'

=item * 'button4-mask' / 'GDK_BUTTON4_MASK'

=item * 'button5-mask' / 'GDK_BUTTON5_MASK'

=item * 'release-mask' / 'GDK_RELEASE_MASK'

=item * 'modifier-mask' / 'GDK_MODIFIER_MASK'

=back



=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

