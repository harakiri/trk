=head1 NAME

Gtk2::Combo

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Box
                          +----Gtk2::HBox
                                +----Gtk2::Combo

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::Combo-E<gt>B<new> 

=over

=back

=head2 $combo-E<gt>B<set_case_sensitive> ($val)

=over

=over

=item * $val (boolean) 

=back

=back

=head2 $combo-E<gt>B<disable_activate> 

=over

=back

=head2 widget = $combo-E<gt>B<entry> 

=over

=back

=head2 $combo-E<gt>B<set_item_string> ($item, $item_value)

=over

=over

=item * $item (Gtk2::Item) 

=item * $item_value (string) 

=back

=back

=head2 widget = $combo-E<gt>B<list> 

=over

=back

=head2 $combo-E<gt>B<set_popdown_strings> (...)

=over

=over

=item * ... (list) of strings

=back



=back

=head2 $combo-E<gt>B<set_use_arrows_always> ($val)

=over

=over

=item * $val (boolean) 

=back

=back

=head2 $combo-E<gt>B<set_use_arrows> ($val)

=over

=over

=item * $val (boolean) 

=back

=back

=head2 $combo-E<gt>B<set_value_in_list> ($val, $ok_if_empty)

=over

=over

=item * $val (boolean) 

=item * $ok_if_empty (boolean) 

=back

=back


=head1 PROPERTIES

=over

=item 'allow-empty' (boolean : readable / writable)

Whether an empty value may be entered in this field

=item 'case-sensitive' (boolean : readable / writable)

Whether list item matching is case sensitive

=item 'enable-arrow-keys' (boolean : readable / writable)

Whether the arrow keys move through the list of items

=item 'enable-arrows-always' (boolean : readable / writable)

Obsolete property, ignored

=item 'value-in-list' (boolean : readable / writable)

Whether entered values must already be present in the list

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Box>, L<Gtk2::HBox>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

