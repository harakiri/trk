#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors : 
# Charles LONGEAU (chl@tuxfamily.org)
# Arnaud DESMONS (logarno@libconf.net)
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
# Copyright (C) 2002 Charles LONGEAU (chl@tuxfamily.org)
# Copyright (C) 2004 Arnaud DESMONS (logarno@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf;

=head1 NAME

  Libconf - Set of abstraction layers to the linux/unix configuration

=head1 SYNOPSIS

=over

=item B<Very High level>

  use Libconf::System::Network;
  my $sys_network = new Libconf::System::Network();
  $sys_network->readConf;
  $sys_network->{interfaces}{lo}{ONBOOT} = 'no';
  $sys_network->{proxy}{ftp} = 'ftp://foo.bar';

=item B<High level>

  use Libconf::Glueconf::Networking::Resolv;
  my $resolv = new Libconf::Glueconf::Networking::Resolv('/etc/resolv.conf');
  push(@{$resolv->{nameserver}}, '192.168.76.7');
  $resolv->{search}->[1] = 'foo';
  $resolv->writeConf('/etc/resolv_modified');

=item B<Low level> (don't use this unless you know what you're doing)

  $struct = new Libconf("smb.conf", 'samba', '');
  $struct->editAtom(-1, {key => 'writable', value => 'TEST', sections => [{name => 'homes'}] });
  $struct->appendAtom({key => 'K1', value => 'yo', type => 'KEY_VALUE' });
  $struct->writeConf("smb.conf");

=back

=head1 DESCRIPTION

=over

=item B<Very High level>

See L<Libconf::System> for more information on this layer

=item B<High level>

See L<Libconf::Glueconf> for more information on this layer

=item B<Low level> (don't use this unless you know what you're doing)

The low level layer is used to parse one configuration file. It gives in return
a structure that is simple, but not as easy to manipulate as the one
Libconf::Glueconf can provide. Libconf::Glueconf actually uses this structure
to map it onto perl hashes (using tie). You can modify the datas of the
structure, and write them back. The configuration informations are stored in a
sequential manner. The power of the low level layer, is that its structure is
unique for any configuration file type.

See L<Libconf::Templates> for more information on this layer

=cut

use strict;
use vars qw(@ISA %EXPORT_TAGS @EXPORT_OK $VERSION %templates
 $Indentspaces
 $Templatesdirectory
 $Trimspaces
 $Strict
 );

@ISA = qw(Exporter);
%EXPORT_TAGS = (
    functions => [ qw() ],
    settings => [ qw() ],
    debug => [ qw (debug) ],
    helpers => [ qw(cat_ output deepcopy member to_bool bool2truefalse bool2yesno text2bool map_index if_ fold_left compare_depth deepcompare_ put_in_hash add2hash add2hash_ ) ],
);
$EXPORT_TAGS{all} = [ map { @$_ } values %EXPORT_TAGS ];
@EXPORT_OK = map { @$_ } values %EXPORT_TAGS;

$VERSION='0.39.9';
$Indentspaces = '    ';
$Trimspaces = 1;
$Strict = 1;

sub new {
    warn_old_api();
    my ($pkg, $filename, $template_name, $validation_name, $template_filename, $safe_write) = @_;
#    -e $filename or die ("$filename not found");
    defined $filename && !-e $filename and system("touch", $filename);
    if ($template_filename) {
        require $template_filename;
    } else {
        eval "require Libconf::templates::$template_name" . '_template';
        $@ and die "could not require Libconf::templates::$template_name" . '_template : ' . $@;
    }
    my %out;
    $out{filename} = $filename;
    $out{template_name} = $template_name;
    $validation_name and $out{validation_name} = $validation_name;
    $out{atoms} = [];
    defined $filename && $safe_write and $out{timestamp} = getTimestamp($filename);
    readConf(\%out);
    bless \%out, $pkg;
    \%out;
}

sub getTimestamp {
    my ($filename) = @_;
    my @stat;
    @stat = stat($filename);
    return $stat[9];
}

sub readConf {
    warn_old_api();
    my ($out) = @_;
    my $filename = $out->{filename};
    my @file;
    if (-e $filename) {
        @file = cat_($filename);
    } else {
        @file = ();
    }
    parse($out, \@file);
}

sub clear {
    warn_old_api();
    my ($out) = @_;
    $out->{atoms} = [];
}

# COMMENTS TO BE REVIEWED
# $out : the structure
# $index : the atom index. If -1, then the template will try to find the matching atom
# $ref_args : a reference on a hash containing the datas
# returns : 
#   0 if ok,
#   -1 if $index bigger than @atoms,
#   -2 if the template didn't succeed finding the mathing atom ($index was -1),
#   -3 if there is an error setting the values in the atom

sub editAtom {
    warn_old_api();
    my ($out, $index, $ref_args) = @_;
    my %args=%$ref_args;
    eval($templates{$out->{template_name}}{edit_atom});
    $@ and die $@;
#    validate($out, @{$out->{atoms}}[$index]);
}

sub getAtom {
    warn_old_api();
    my ($out, $index) = @_;
    $out->{atoms}[$index];
}


sub findAtomPos {
    warn_old_api();
    my ($out, $ref_args, $first_atom, $last_atom) = @_;
    my %args=%$ref_args;
    eval($templates{$out->{template_name}}{find_atom_pos});
}

sub appendAtom {
    warn_old_api();
    my ($out, $ref_args) = @_;
    my %args = %$ref_args;
    my $index = @{$out->{atoms}};
    push(@{$out->{atoms}}, {});
    eval($templates{$out->{template_name}}{edit_atom});
#    validate($out, @{$out->{atoms}}[$index]);
}

sub insertAtom {
    warn_old_api();
    my ($out, $index, $ref_args) = @_;
    $index >= @{$out->{atoms}} and return appendAtom($out, $ref_args);
    my %args = %$ref_args;
    splice(@{$out->{atoms}}, $index, 0, {});
    $args{sections} ||= @{$out->{atoms}}[$index-1]->{sections};
    $args{sections} ||= [];
    eval($templates{$out->{template_name}}{edit_atom});
#    validate($out, @{$out->{atoms}}[$index]);
}

sub deleteAtom {
    warn_old_api();
    my ($out, $index) = @_;  
    splice(@{$out->{atoms}}, $index, 1);
#    validate($out, @{$out->{atoms}}[$index]);
}

sub compare_depth {
    my ($ref1, $ref2) = @_;
    my @sections1 = @$ref1;
    my @sections2 = @$ref2;
    @sections1 == @sections2 or return 0;
    foreach my $i (0..@sections1-1) {
        $sections2[$i]->{name} eq $sections1[$i]->{name} or return 0;
    }
    1;
}

sub size {
    warn_old_api();
    my ($out) = @_;
    scalar(@{$out->{atoms}});
}

sub getSectionSize {
    warn_old_api();
    my ($self, $start_atom) = @_;
    $self->getSectionEndAtom($start_atom) - $start_atom + 1;
}

sub deleteSection {
    warn_old_api();
    my ($out, $index, $no_end_section) = @_;
    my $atom = $out->getAtom($index);
    #$atom->sections;
    my @end_positions = $no_end_section ?
      $out->findAtomPos( { type => 'SECTION' }) :
        $out->findAtomPos( { type => 'ENDSECTION', sections => $atom->{sections} });
    my $end;
    foreach (@end_positions) {
        $_ > $index and $end = $_, last;
    }
    !defined $end && $no_end_section and $end = @{$out->{atoms}}-1;
    defined $end or die "no ENDSECTION for SECTION n� $index";
    splice(@{$out->{atoms}}, $index, $end-$index+1);
}

sub clearSection {
    warn_old_api();
    my ($out, $index, $no_end_section) = @_;
    my $atom = $out->getAtom($index);
    #$atom->sections;
    my @end_positions = $no_end_section ?
      $out->findAtomPos( { type => 'SECTION' }) :
        $out->findAtomPos( { type => 'ENDSECTION', sections => $atom->{sections} });
    my $end;
    foreach (@end_positions) {
        $_ > $index and $end = $_, last;
    }
    !defined $end && $no_end_section and $end = @{$out->{atoms}}-1;
    defined $end or die "no ENDSECTION for SECTION n� $index";
    my @ret = splice(@{$out->{atoms}}, $index+1, $end-$index);
    @ret;
}

sub matchInStructComments {
    warn_old_api();
    my ($struct, $expr, $eval_expr) = @_;
    foreach my $atom (@{$struct->{atoms}}) {
        foreach my $comment (@{$atom->{comments}}) {
            if ($eval_expr) {
                $comment->{comment} =~ /\Q$expr\E/ and return 1;
            } else {
                $comment->{comment} =~ /$expr/ and return 1;
            }
        }
    }
    0;
}

sub writeConf {
    warn_old_api();
    my ($out, $filename) = @_;
    $filename ||= $out->{filename};
    my $template_name = $out->{template_name};
    my @out_atoms = @{$out->{atoms}};
    if (defined $out->{timestamp} && (getTimestamp($filename) != $out->{timestamp})) {
        print "Warning $filename has changed !\n";
    }
    local *F;
    open F, "> $filename" or die "cannot create config file $filename (do you have proper permissions?)";
    #special rule called at the beginning
    if (exists $templates{$template_name}->{output_heading}) {
        my $output_text = '';
        my $output_indentation = $out->{current_indentation};
        eval($templates{$template_name}->{output_heading});
        print F handle_output($output_text, $output_indentation);
    }
    foreach my $atom (@out_atoms) {
#	print F "\n" x $$atom{line_feed};
#	$$atom{comments} and print F eval($templates{$template_name}{comment_output}) foreach split("\n", $$atom{comments});
        my $output_text = '';
        my $output_indentation = $out->{current_indentation};
        use Data::Dumper;
        if (defined $atom->{type}) {
            if (exists $templates{$template_name}->{output}{$atom->{type}}) {
                eval($templates{$template_name}->{output}{$atom->{type}});
            } elsif ($atom->{type} eq 'UNKNOWN') {
                $output_text = $atom->{value};# if the type is 'UNKNOWN' (but not undef), we produce default output
            } else {
                print F '## WARNING - No valid $template->{output}{' . $atom->{type} . "}\n";
                die '## WARNING - No valid $template->{output}{' . $atom->{type} . "}\n";
            }
        }
        print F handle_output($output_text, $output_indentation, $atom->{comments});
    }
    if (exists $templates{$template_name}->{output_trailing}) {
        my $output_text = '';
        my $output_indentation = $out->{current_indentation};
        eval($templates{$template_name}->{output_trailing});
        print F handle_output($output_text, $output_indentation);
    }

}

sub parse {
    warn_old_api();
    my ($out, $file) = @_;
    my @file = @$file;
    my $item = 0;
#    my $next_item = 0;
    my $template_name = $out->{template_name};
    my @out_atoms;
    my $matched;
    my $keep_atom;
    my $switch_to_next_atom = 0;
    my $switch_to_prev_atom = 0;

    $out->{current_sections} ||= [];
    my $line_idx = 1;
    foreach my $in (@file) {
	$out_atoms[$item] ||= {};
        $out_atoms[$item]->{comments} ||= [];
	$in =~ /^\s*\n$/ and push(@{$out_atoms[$item]->{comments}}, { comment => '', inline => 0 }), next;
#	$in eq "\n" and $out_atoms[$item]->{line_feed}++, next;
	chomp $in;
	#see if we are in a line previously commented
	if ($out_atoms[$item]->{current_comment}) {
	    my $c = $out_atoms[$item]->{current_comment};
	    if ($in =~ s/^(.*\Q$c\E)//) {
                push(@{$out_atoms[$item]->{comments}}, { comment => $1, inline => $in =~ /^\s*$/ ? 0 : -1 });
		undef $out_atoms[$item]->{current_comment};
	    } else {
                push(@{$out_atoms[$item]->{comments}}, { comment => $in, inline => 0 });
		$in = '';
	    }
	} else {
	    # we remove comments
	    # the comment structure : [ open_char, close_char, nested ]
	    # There is an additional comment command : disable_inline_comments, to avoid handling inline comment characters
	    my ($open, $close, $nested);
	    foreach (@{$templates{$template_name}{comments}}) {
		($open, $close, $nested) = @$_;
		if ($nested) {
		    my ($level, $start) = (0, 0);  #we don't use $out_atoms[$item]->{current_level} directly to avoid overriding it.
		    my $end = 0; #length($in);
		    for (my $i=0; $i < length($in); $i++) {
			substr($in, $i, length($open)) eq $open and $level++, $start ||= $i;
			if ($level && substr($in, $i, length($close)) eq $close) {
			    $level--;
			    $level or $end = $i-$start;
			}
		    }
		    $end and push(@{$out_atoms[$item]->{comments}}, { comment => substr($in, $start, $end + length($close)),
                                                                    inline => $start > 0 ? 1 : 0 });
		    substr($in, $start, $end + length($close)) = '';
		    $level and $out_atoms[$item]->{current_comment} = $close, $out_atoms[$item]->{current_level} = $level;
		} elsif (defined $open && defined $close) {
		    $in =~ s/(\Q$open\E.*\Q$close\E)/push(@{$out_atoms[$item]->{comments}}, { comment => $1, inline => length($') ? -1 : (length($`) ? 1 : 0) }); undef/eg;
		    #$in =~ s/(\Q$open\E.*\Q$close\E)/$out_atoms[$item]->{comments} .= "$1\n"; undef/eg;
		    $in =~ s/(\Q$open\E.*)$// and push(@{$out_atoms[$item]->{comments}}, { comment => $1,
                                                                                        inline => $in =~ /^\s*$/ ? 0 : 1 }
                                                      ), $out_atoms[$item]->{current_comment} = $close, last;
		} elsif (defined $open) {
                    if ($templates{$template_name}{disable_inline_comments}) {
                        if ($in =~ s/^\s*(\Q$open\E.*)$//) {
                            my $tmp = $1;
                            push(@{$out_atoms[$item]->{comments}}, { comment => $tmp });
		        }
		    } else {
		        if ($in =~ s/(\Q$open\E.*)$//) {
                            my $tmp = $1;
                            push(@{$out_atoms[$item]->{comments}}, { comment => $tmp,
		                                                     inline => $in =~ /^\s*$/ ? 0 : 1 });
                        }
                    }
		}
	    }
        }
	### End Comment handling ###
        $matched = 0;
        $keep_atom = 0;
        $switch_to_next_atom = 0;
        $switch_to_prev_atom = 0;
        my $atom = $out_atoms[$item];
        foreach (@{$templates{$template_name}{rules}}) {
	    eval ($_);
	    $@ and die qq(error while applying the rule '$_' to '$in' line n� $line_idx : $@);
            # the last rule asked to go to next atom
            if ($switch_to_next_atom) {
                $item++;
                $out_atoms[$item] ||= {};
                $out_atoms[$item]->{comments} ||= [];
                $atom = $out_atoms[$item];
                $switch_to_next_atom;
            }
            if ($switch_to_prev_atom) {
                $item--;
                $atom = $out_atoms[$item];
                $switch_to_prev_atom = 0;
            }
#            $next_item = $matched && !$keep_atom;
	    $matched and last;
	}
#	validate($out, $out_atoms[$item]);
        if ($in !~ /^\s*$/) {
            if ($matched || $Strict) {
                push @{$out_atoms[$item]->{invalid}}, $in;
                print "\nLibconf.pm warning: can't parse line n�" . $line_idx . " | template $template_name | file $out->{filename} | line was : \n$in\n";
            } else {
                $atom->{type} = 'UNKNOWN';
                $atom->{value} = $in;
                $matched=1;
            }
        }
#       $keep_atom or $item++;
        exists $atom->{sections} or $atom->{sections} = [];
        $matched && !$keep_atom and $item++;
#	if (!$keep_atom) {
#	    $item++;
#	    $next_item = 0;
#	}
    } continue {
        $line_idx++;
    }
    $out->{atoms} = [@out_atoms];
}

sub handle_output {
    warn_old_api();
    my ($text, $indentation, $comments) = @_;
    my $ret = '';
    my $flag = 1;
    my $same_line = 0;
    if ($Trimspaces) {
        $text =~ s/( )+/$1/g; #SMALL BUG : when the string contains "\t   " it returns " ". Should we keep \t ?
        $text =~ s/^\s*//;
        $text =~ s/\s*$//;
    }
    $indentation = $Indentspaces x $indentation;
    if (defined $comments) {
        for (my $i; $i < @$comments; $i++) {
            my $comment = $comments->[$i];
            if ($comment->{inline} == -1) {
                $ret .= $indentation . $comment->{comment};
                $same_line = 1;
                if ($i == @$comments-1) {
                    $ret .= " $text" . "\n";
                    $flag = 0;
                }
            } elsif ($comment->{inline} == 0) {
                $ret .= $indentation . $comment->{comment} . "\n";
            } elsif ($comment->{inline} == 1) {
                if ($same_line) {
                    $same_line = 0;
                } else {
                    $ret .= $indentation;
                }
                if ($i == @$comments-1) {
                    $ret .= "$text " . $comment->{comment} . "\n";
                    $flag = 0;
                } else {
                    $ret .= $comment->{comment} . "\n";
                }
            }
        }
    }
    $flag && length($text) and $ret .= $indentation . $text . "\n";
    $ret;
}

#validation stuff

# BUG : setUniq is too specific to Samba
sub setUniq {
    warn_old_api();
    my ($obj) = @_;
    my @to_delete;
    my $i = 0;
    foreach my $atom (@{$obj->{atoms}}) {
        my %atom2 = %$atom;
        delete @atom2{qw(comments value list)}; #BUG : hard coded
        my @pos = findAtomPos($obj, \%atom2);
        if (@pos > 1 && $pos[-1] != $i) {
            push @to_delete, $i;
        }
        $i++;
    }
    # reversed to delete down -> up, so that index are still valid
    deleteAtom($obj, $_) foreach reverse @to_delete;
}

sub debug {
    $ENV{DEBUG} or return;
    my $i = 0;
    while (caller($i)) { $i++ }
    my ($_package, $_filename, $_line, $subroutine, $_hasargs, $_wantarray, $_evaltext, $_is_require, $_hints, $_bitmask) = caller(2);
    print '   ' x ($i-3);
    print "\033[35m";
    print $subroutine, "\t\033[36m", @_;
    print "\033[0m\n";
}

# sub validate {
#     my ($out, $atom) = @_;
#     $out->{validation_name} and eval($validation{$out->{validation_name}}->{rules});
# }

sub toXML {
    my ($self) = @_;
    require XML::LibXML;
    # new XML document
    my $doc = XML::LibXML::Document->new();
    $doc->setEncoding('UTF8');

    # element : config-file
    $doc->setDocumentElement(my $config_file = $doc->createElement('CONFIG-FILE'));
    $config_file->setAttribute(file_name => $self->{filename});
    $config_file->setAttribute(template_name => $self->{template_name});

    my @current_nodes = ($config_file);
    my $current_sections = [];


    foreach my $atom (@{$self->{atoms}}) {
        my $current_node = $current_nodes[-1];
        my ($child, $child2);
        defined $current_node or die "problem in xml generation or structure malformed";
        if      ($atom->{type} eq 'SECTION') {
            if (!compare_depth($atom->{sections}, $current_sections)) {
                @current_nodes > 1 and pop @current_nodes;
                $current_node = $current_nodes[-1];
            };
            $current_node->appendChild($child = $doc->createElement('SECTION'));
            $child->setAttribute(name => $atom->{section_name});
            push @current_nodes, $child;
        } elsif ($atom->{type} eq 'KEY_VALUE') {
            $current_node->appendChild($child = $doc->createElement('KEY_VALUE'));
            $child->setAttribute(key => $atom->{key});
            $child->appendText($atom->{value});
        } elsif ($atom->{type} eq 'KEY_VALUES') {
            $current_node->appendChild($child = $doc->createElement('KEY_VALUES'));
            $child->setAttribute(key => $atom->{key});
            while (my ($key, $value) = each %{$atom->{values}}) {
                $child->appendChild($child2 = $doc->createElement('KEY_VALUE'));
                $child2->setAttribute(key => $key);
                $child2->appendText($value);
            }
        } elsif ($atom->{type} eq 'ENDSECTION') {
            pop @current_nodes;
        } elsif ($atom->{type} eq 'KEY_LIST') {
            $current_node->appendChild($child = $doc->createElement('KEY_LIST'));
            $child->setAttribute(key => $atom->{key});
            foreach my $value (@{$atom->{list}}) {
                $child->appendChild($child2 = $doc->createElement('VALUE'));
                $child2->appendText($value);
            }
        }
        if (exists $atom->{comments} and @{$atom->{comments}}) {
            defined $child or $current_node->appendChild($child = $doc->createElement('VOID'));
            $child->appendChild($child2 = $doc->createElement('COMMENTS'));
            foreach my $comment (@{$atom->{comments}}) {
                $child2->appendChild(my $child3 = $doc->createElement('COMMENT'));
                $child3->appendText($comment->{comment});
                $comment->{inline} and $child3->setAttribute(inline => $comment->{inline} == 1 ? 'RIGHT' : 'LEFT' );
            }
        }
        exists $atom->{sections} and $current_sections = $atom->{sections};
    }
    $doc;
}

sub toXMLString {
    my ($self, $format) = @_;
    toXML($self)->toString($format);
}

sub toXMLFile {
    my ($self, $file, $format) = @_;
    toXML($self)->toFile($file, $format);
}


sub	comment {
  my ($elt, $array) = @_;

    for ($_->children('COMMENTS')) {
      for ($_->children('COMMENT')) {
	push @$array, {comment => $_->text};
      }
    }
}

sub	newFromXML {
  my ($pkg, $xml) = @_;

  my %conf;
  my @current_sections;
  require XML::Twig;
  my $twig= new XML::Twig(start_tag_handlers =>
			   { SECTION => \&section,
			     'CONFIG-FILE' => \&config_file,
			     KEY_VALUES => \&key_values
			   },
			  twig_roots => {
					 KEY_LIST => \&key_list,
					 KEY_VALUE => \&key_value,
					 COMMENTS => \&comments
					},
			  end_tag_handlers   => { SECTION => \&end_section
						}
			 );
  $twig->parse($xml);

  sub	comments {
    for ($_->children('COMMENT')) {
      push @{${@{$conf{atoms}}[$#{$conf{atoms}}]}{comments}}, {comment => $_->text,
							       inline => ($_->att('inline') ?
									  ($_->att('inline') eq 'RIGHT' ? 1 : -1) : 0)};
    }
  }

  sub	section {
    my ($t, $gi, %att) = @_;
    push @{$conf{atoms}}, {type => 'SECTION',
			  sections => [@current_sections],
			  section_name => $att{name}};
    push @current_sections, { name => $att{name}};
  }
  
  sub	end_section {
    push @{$conf{atoms}}, {type => 'ENDSECTION'};
    pop @current_sections;
  }
  
  sub config_file {
    my ($t, $gi, %att) = @_;
    $conf{template_name} = $att{template_name};
    $conf{filename} = $att{file_name};
  }

  sub	key_list {
    my %key_list;
    $key_list{type} = 'KEY_LIST';
    $key_list{key} = $_->{att}->{'key'};
    $key_list{sections} = [@current_sections];
    for ($_->children('VALUE')) {
      push @{$key_list{list}}, $_->text;
    }
    push @{$conf{atoms}}, \%key_list;
  }

  sub	key_value {
    my $key_value = { type => 'KEY_VALUE',
		      key => $_->{att}->{'key'},
		      value => $_->text
		    };
    if (${@{$conf{atoms}}[$#{$conf{atoms}}]}{type} eq 'KEY_VALUES') {
      push @{${@{$conf{atoms}}[$#{$conf{atoms}}]}{values}}, {$_->{att}->{'key'} => $_->text};
    }
  }

  sub	key_values {
    my ($t, $gi, %att) = @_;
    push @{$conf{atoms}}, { type => 'KEY_VALUES',
			    key => $att{key}};
  }
  bless \%conf, $pkg;
  \%conf;
}
#
#sub newFromXML {
#  my ($xml) = @_;
#
#  require XML::Twig;
#  my $twig= XML::Twig->new();
#  $twig->parse($xml);
#  my $root = $twig->root;
#  my $elt = $root->first_child('CONFIG-FILE');
#  my %conf;
#  $conf{template_name} = $elt->{'att'}->{'template_name'};
#  $conf{filename} = $elt->{'att'}->{'file_name'};
#  for ($elt->children('SECTION')) {
#    my %section;
#    $section{type} = 'SECTION';
#    comment($_, \@{$section{comments}});
#    for ($_->children('KEY_LIST')) {
#      my %key_list;
#      comment($_, \@{$key_list{comments}});
#      $key_list{type} = 'KEY_LIST';
#      $key_list{key} = $_->{'att'}->{'key'};
#      for ($_->children('VALUE')) {
#	push @{$key_list{list}}, $_->text;
#      }
#      push @{$conf{atoms}}, \%key_list;
#    }
#    push @{$conf{atoms}}, \%section;
#  }
#  \%conf;
#}

# Helper functions

#           open(my $fh,'<:encoding(Big5)', 'anything');

sub cat_ { my ($f, $e) = @_;
           local *F;
           open F, defined $e ? ("<:encoding($e)", $f) : $f or die "reading file $f failed: $!\n";
           my @l = <F>;
           wantarray() ? @l : join '', @l
       }
sub output { my $f = shift; local *F; open F, ">$f" or die "output in file $f failed: $!\n"; print F foreach @_; 1 }
sub member { my $e = shift; foreach (@_) { $e eq $_ and return 1 } 0 }
sub to_bool { $_[0] ? 1 : 0 }
sub bool2truefalse { $_[0] ? "true" : "false" }
sub bool2yesno { $_[0] ? "yes" : "no" }
sub text2bool { my $t = lc($_[0]); $t eq "true" || $t eq "yes" ? 1 : 0 }
sub put_in_hash { my ($a, $b) = @_; while (my ($k, $v) = each %{$b || {}}) { $a->{$k} = $v } $a }
sub add2hash    { my ($a, $b) = @_; while (my ($k, $v) = each %{$b || {}}) { $a->{$k} ||= $v } $a }
sub add2hash_   { my ($a, $b) = @_; while (my ($k, $v) = each %{$b || {}}) { exists $a->{$k} or $a->{$k} = $v } $a }
sub deepcopy {
    my $s = shift;
    ref($s) eq 'ARRAY' and return [ map deepcopy($_), @$s ];
    ref($s) eq 'HASH' and return { map { $_ => deepcopy($s->{$_}) } keys %$s };
    $s;
}
sub deepcompare_ {
    my ($search_struct, $struct) = @_;
    if (ref($search_struct) eq 'HASH') {
        ref($struct) eq 'HASH' or return 0;
#        scalar(keys %$search_struct) == scalar(keys %$struct) or return 0;
        foreach (keys %$search_struct) {
            deepcompare_($search_struct->{$_}, $struct->{$_}) or return 0;
        }
        return 1;
    }
    if (ref($search_struct) eq 'ARRAY') {
        ref($struct) eq 'ARRAY' or return 0;
        scalar(@$search_struct) == scalar(@$struct) or return 0;
        foreach (0..$#{$search_struct}) {
            deepcompare_($search_struct->[$_], $struct->[$_]) or return 0;
        } 
        return 1;
    }
    $search_struct eq $struct;
}
sub map_index(&@) {
    my $f = shift;
    my @v; local $::i = 0;
    map { @v = &$f($::i); $::i++; @v } @_;
}
sub if_($@) { 
    my $b = shift;
    $b or return ();
    wantarray() || @_ <= 1 or die("if_ called in scalar context with more than one argument " . join(":", caller()));
    wantarray() ? @_ : $_[0];
}       
sub fold_left(&@) {
    my ($f, $initial, @l) = @_;
    local ($::a, $::b);
    $::a = $initial;
    foreach (@l) { $::b = $_; $::a = &$f() }
    $::a
}       

sub warn_old_api {
    warn("   *** Warning ***\n");
    warn("   * you are using the old template system\n");
    warn("   * This will be unsupported soon\n");
    warn("   * you should switch to Template API\n");
}

=back

=head1 SEE ALSO

L<Libconf::System>, L<Libconf::Glueconf>, L<Libconf::Templates>

=cut

1;
