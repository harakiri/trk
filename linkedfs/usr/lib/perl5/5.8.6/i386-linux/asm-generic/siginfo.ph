require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_GENERIC_SIGINFO_H)) {
    eval 'sub _ASM_GENERIC_SIGINFO_H () {1;}' unless defined(&_ASM_GENERIC_SIGINFO_H);
    require 'linux/compiler.ph';
    require 'linux/types.ph';
    require 'linux/resource.ph';
    unless(defined(&__ARCH_SI_PREAMBLE_SIZE)) {
	eval 'sub __ARCH_SI_PREAMBLE_SIZE () {(3* $sizeof{\'int\'});}' unless defined(&__ARCH_SI_PREAMBLE_SIZE);
    }
    eval 'sub SI_MAX_SIZE () {128;}' unless defined(&SI_MAX_SIZE);
    unless(defined(&SI_PAD_SIZE)) {
	eval 'sub SI_PAD_SIZE () {(( &SI_MAX_SIZE -  &__ARCH_SI_PREAMBLE_SIZE) / $sizeof{\'int\'});}' unless defined(&SI_PAD_SIZE);
    }
    unless(defined(&__ARCH_SI_UID_T)) {
	eval 'sub __ARCH_SI_UID_T () { &uid_t;}' unless defined(&__ARCH_SI_UID_T);
    }
    unless(defined(&__ARCH_SI_BAND_T)) {
	eval 'sub __ARCH_SI_BAND_T () {\'long\';}' unless defined(&__ARCH_SI_BAND_T);
    }
    unless(defined(&HAVE_ARCH_SIGINFO_T)) {
	if(defined(&__ARCH_SI_TRAPNO)) {
	}
    }
    eval 'sub si_pid () { ($_sifields->{_kill}->{_pid});}' unless defined(&si_pid);
    eval 'sub si_uid () { ($_sifields->{_kill}->{_uid});}' unless defined(&si_uid);
    eval 'sub si_tid () { ($_sifields->{_timer}->{_tid});}' unless defined(&si_tid);
    eval 'sub si_overrun () { ($_sifields->{_timer}->{_overrun});}' unless defined(&si_overrun);
    eval 'sub si_sys_private () { ($_sifields->{_timer}->{_sys_private});}' unless defined(&si_sys_private);
    eval 'sub si_status () { ($_sifields->{_sigchld}->{_status});}' unless defined(&si_status);
    eval 'sub si_utime () { ($_sifields->{_sigchld}->{_utime});}' unless defined(&si_utime);
    eval 'sub si_stime () { ($_sifields->{_sigchld}->{_stime});}' unless defined(&si_stime);
    eval 'sub si_value () { ($_sifields->{_rt}->{_sigval});}' unless defined(&si_value);
    eval 'sub si_int () { ($_sifields->{_rt}->{_sigval}->{sival_int});}' unless defined(&si_int);
    eval 'sub si_ptr () { ($_sifields->{_rt}->{_sigval}->{sival_ptr});}' unless defined(&si_ptr);
    eval 'sub si_addr () { ($_sifields->{_sigfault}->{_addr});}' unless defined(&si_addr);
    if(defined(&__ARCH_SI_TRAPNO)) {
	eval 'sub si_trapno () { ($_sifields->{_sigfault}->{_trapno});}' unless defined(&si_trapno);
    }
    eval 'sub si_band () { ($_sifields->{_sigpoll}->{_band});}' unless defined(&si_band);
    eval 'sub si_fd () { ($_sifields->{_sigpoll}->{_fd});}' unless defined(&si_fd);
    if(defined(&__KERNEL__)) {
	eval 'sub __SI_MASK () {0xffff0000;}' unless defined(&__SI_MASK);
	eval 'sub __SI_KILL () {(0<< 16);}' unless defined(&__SI_KILL);
	eval 'sub __SI_TIMER () {(1<< 16);}' unless defined(&__SI_TIMER);
	eval 'sub __SI_POLL () {(2<< 16);}' unless defined(&__SI_POLL);
	eval 'sub __SI_FAULT () {(3<< 16);}' unless defined(&__SI_FAULT);
	eval 'sub __SI_CHLD () {(4<< 16);}' unless defined(&__SI_CHLD);
	eval 'sub __SI_RT () {(5<< 16);}' unless defined(&__SI_RT);
	eval 'sub __SI_MESGQ () {(6<< 16);}' unless defined(&__SI_MESGQ);
	eval 'sub __SI_CODE {
	    local($T,$N) = @_;
    	    eval q((($T) | (($N) & 0xffff)));
	}' unless defined(&__SI_CODE);
    } else {
	eval 'sub __SI_KILL () {0;}' unless defined(&__SI_KILL);
	eval 'sub __SI_TIMER () {0;}' unless defined(&__SI_TIMER);
	eval 'sub __SI_POLL () {0;}' unless defined(&__SI_POLL);
	eval 'sub __SI_FAULT () {0;}' unless defined(&__SI_FAULT);
	eval 'sub __SI_CHLD () {0;}' unless defined(&__SI_CHLD);
	eval 'sub __SI_RT () {0;}' unless defined(&__SI_RT);
	eval 'sub __SI_MESGQ () {0;}' unless defined(&__SI_MESGQ);
	eval 'sub __SI_CODE {
	    local($T,$N) = @_;
    	    eval q(($N));
	}' unless defined(&__SI_CODE);
    }
    eval 'sub SI_USER () {0;}' unless defined(&SI_USER);
    eval 'sub SI_KERNEL () {0x80;}' unless defined(&SI_KERNEL);
    eval 'sub SI_QUEUE () {-1;}' unless defined(&SI_QUEUE);
    eval 'sub SI_TIMER () { &__SI_CODE( &__SI_TIMER,-2);}' unless defined(&SI_TIMER);
    eval 'sub SI_MESGQ () { &__SI_CODE( &__SI_MESGQ,-3);}' unless defined(&SI_MESGQ);
    eval 'sub SI_ASYNCIO () {-4;}' unless defined(&SI_ASYNCIO);
    eval 'sub SI_SIGIO () {-5;}' unless defined(&SI_SIGIO);
    eval 'sub SI_TKILL () {-6;}' unless defined(&SI_TKILL);
    eval 'sub SI_DETHREAD () {-7;}' unless defined(&SI_DETHREAD);
    eval 'sub SI_FROMUSER {
        local($siptr) = @_;
	    eval q((($siptr)-> &si_code <= 0));
    }' unless defined(&SI_FROMUSER);
    eval 'sub SI_FROMKERNEL {
        local($siptr) = @_;
	    eval q((($siptr)-> &si_code > 0));
    }' unless defined(&SI_FROMKERNEL);
    unless(defined(&HAVE_ARCH_SI_CODES)) {
	eval 'sub ILL_ILLOPC () {( &__SI_FAULT|1);}' unless defined(&ILL_ILLOPC);
	eval 'sub ILL_ILLOPN () {( &__SI_FAULT|2);}' unless defined(&ILL_ILLOPN);
	eval 'sub ILL_ILLADR () {( &__SI_FAULT|3);}' unless defined(&ILL_ILLADR);
	eval 'sub ILL_ILLTRP () {( &__SI_FAULT|4);}' unless defined(&ILL_ILLTRP);
	eval 'sub ILL_PRVOPC () {( &__SI_FAULT|5);}' unless defined(&ILL_PRVOPC);
	eval 'sub ILL_PRVREG () {( &__SI_FAULT|6);}' unless defined(&ILL_PRVREG);
	eval 'sub ILL_COPROC () {( &__SI_FAULT|7);}' unless defined(&ILL_COPROC);
	eval 'sub ILL_BADSTK () {( &__SI_FAULT|8);}' unless defined(&ILL_BADSTK);
	eval 'sub NSIGILL () {8;}' unless defined(&NSIGILL);
	eval 'sub FPE_INTDIV () {( &__SI_FAULT|1);}' unless defined(&FPE_INTDIV);
	eval 'sub FPE_INTOVF () {( &__SI_FAULT|2);}' unless defined(&FPE_INTOVF);
	eval 'sub FPE_FLTDIV () {( &__SI_FAULT|3);}' unless defined(&FPE_FLTDIV);
	eval 'sub FPE_FLTOVF () {( &__SI_FAULT|4);}' unless defined(&FPE_FLTOVF);
	eval 'sub FPE_FLTUND () {( &__SI_FAULT|5);}' unless defined(&FPE_FLTUND);
	eval 'sub FPE_FLTRES () {( &__SI_FAULT|6);}' unless defined(&FPE_FLTRES);
	eval 'sub FPE_FLTINV () {( &__SI_FAULT|7);}' unless defined(&FPE_FLTINV);
	eval 'sub FPE_FLTSUB () {( &__SI_FAULT|8);}' unless defined(&FPE_FLTSUB);
	eval 'sub NSIGFPE () {8;}' unless defined(&NSIGFPE);
	eval 'sub SEGV_MAPERR () {( &__SI_FAULT|1);}' unless defined(&SEGV_MAPERR);
	eval 'sub SEGV_ACCERR () {( &__SI_FAULT|2);}' unless defined(&SEGV_ACCERR);
	eval 'sub NSIGSEGV () {2;}' unless defined(&NSIGSEGV);
	eval 'sub BUS_ADRALN () {( &__SI_FAULT|1);}' unless defined(&BUS_ADRALN);
	eval 'sub BUS_ADRERR () {( &__SI_FAULT|2);}' unless defined(&BUS_ADRERR);
	eval 'sub BUS_OBJERR () {( &__SI_FAULT|3);}' unless defined(&BUS_OBJERR);
	eval 'sub NSIGBUS () {3;}' unless defined(&NSIGBUS);
	eval 'sub TRAP_BRKPT () {( &__SI_FAULT|1);}' unless defined(&TRAP_BRKPT);
	eval 'sub TRAP_TRACE () {( &__SI_FAULT|2);}' unless defined(&TRAP_TRACE);
	eval 'sub NSIGTRAP () {2;}' unless defined(&NSIGTRAP);
	eval 'sub CLD_EXITED () {( &__SI_CHLD|1);}' unless defined(&CLD_EXITED);
	eval 'sub CLD_KILLED () {( &__SI_CHLD|2);}' unless defined(&CLD_KILLED);
	eval 'sub CLD_DUMPED () {( &__SI_CHLD|3);}' unless defined(&CLD_DUMPED);
	eval 'sub CLD_TRAPPED () {( &__SI_CHLD|4);}' unless defined(&CLD_TRAPPED);
	eval 'sub CLD_STOPPED () {( &__SI_CHLD|5);}' unless defined(&CLD_STOPPED);
	eval 'sub CLD_CONTINUED () {( &__SI_CHLD|6);}' unless defined(&CLD_CONTINUED);
	eval 'sub NSIGCHLD () {6;}' unless defined(&NSIGCHLD);
	eval 'sub POLL_IN () {( &__SI_POLL|1);}' unless defined(&POLL_IN);
	eval 'sub POLL_OUT () {( &__SI_POLL|2);}' unless defined(&POLL_OUT);
	eval 'sub POLL_MSG () {( &__SI_POLL|3);}' unless defined(&POLL_MSG);
	eval 'sub POLL_ERR () {( &__SI_POLL|4);}' unless defined(&POLL_ERR);
	eval 'sub POLL_PRI () {( &__SI_POLL|5);}' unless defined(&POLL_PRI);
	eval 'sub POLL_HUP () {( &__SI_POLL|6);}' unless defined(&POLL_HUP);
	eval 'sub NSIGPOLL () {6;}' unless defined(&NSIGPOLL);
    }
    eval 'sub SIGEV_SIGNAL () {0;}' unless defined(&SIGEV_SIGNAL);
    eval 'sub SIGEV_NONE () {1;}' unless defined(&SIGEV_NONE);
    eval 'sub SIGEV_THREAD () {2;}' unless defined(&SIGEV_THREAD);
    eval 'sub SIGEV_THREAD_ID () {4;}' unless defined(&SIGEV_THREAD_ID);
    eval 'sub SIGEV_MAX_SIZE () {64;}' unless defined(&SIGEV_MAX_SIZE);
    unless(defined(&SIGEV_PAD_SIZE)) {
	eval 'sub SIGEV_PAD_SIZE () {(( &SIGEV_MAX_SIZE/$sizeof{\'int\'}) - 3);}' unless defined(&SIGEV_PAD_SIZE);
    }
    unless(defined(&HAVE_ARCH_SIGEVENT_T)) {
    }
    eval 'sub sigev_notify_function () { ($_sigev_un->{_sigev_thread}->{_function});}' unless defined(&sigev_notify_function);
    eval 'sub sigev_notify_attributes () { ($_sigev_un->{_sigev_thread}->{_attribute});}' unless defined(&sigev_notify_attributes);
    eval 'sub sigev_notify_thread_id () { ($_sigev_un->{_tid});}' unless defined(&sigev_notify_thread_id);
    if(defined(&__KERNEL__)) {
	unless(defined(&HAVE_ARCH_COPY_SIGINFO)) {
	    require 'linux/string.ph';
	    eval 'sub copy_siginfo {
	        local($to,$from) = @_;
    		eval q({  &if ( ($from->{si_code}) < 0)  &memcpy($to, $from, $sizeof{$to});  &else  &memcpy($to, $from,  &__ARCH_SI_PREAMBLE_SIZE + $sizeof{ ($from->{_sifields}->{_sigchld})}); });
	    }' unless defined(&copy_siginfo);
	}
    }
}
1;
