require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_GENERIC_TOPOLOGY_H)) {
    eval 'sub _ASM_GENERIC_TOPOLOGY_H () {1;}' unless defined(&_ASM_GENERIC_TOPOLOGY_H);
    unless(defined(&cpu_to_node)) {
	eval 'sub cpu_to_node {
	    local($cpu) = @_;
    	    eval q((0));
	}' unless defined(&cpu_to_node);
    }
    unless(defined(&parent_node)) {
	eval 'sub parent_node {
	    local($node) = @_;
    	    eval q((0));
	}' unless defined(&parent_node);
    }
    unless(defined(&node_to_cpumask)) {
	eval 'sub node_to_cpumask {
	    local($node) = @_;
    	    eval q(( &cpu_online_map));
	}' unless defined(&node_to_cpumask);
    }
    unless(defined(&node_to_first_cpu)) {
	eval 'sub node_to_first_cpu {
	    local($node) = @_;
    	    eval q((0));
	}' unless defined(&node_to_first_cpu);
    }
    unless(defined(&pcibus_to_cpumask)) {
	eval 'sub pcibus_to_cpumask {
	    local($bus) = @_;
    	    eval q(( &cpu_online_map));
	}' unless defined(&pcibus_to_cpumask);
    }
}
1;
