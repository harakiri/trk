require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_I386_TOPOLOGY_H)) {
    eval 'sub _ASM_I386_TOPOLOGY_H () {1;}' unless defined(&_ASM_I386_TOPOLOGY_H);
    if(defined(&CONFIG_NUMA)) {
	require 'asm/mpspec.ph';
	require 'linux/cpumask.ph';
	eval 'sub parent_node {
	    local($node) = @_;
    	    eval q(($node));
	}' unless defined(&parent_node);
	eval 'sub node_distance {
	    local($from, $to) = @_;
    	    eval q((($from) != ($to)));
	}' unless defined(&node_distance);
	eval 'sub SD_NODE_INIT () { { . &span =  &CPU_MASK_NONE, . &parent =  &NULL, . &groups =  &NULL, . &min_interval = 8, . &max_interval = 32, . &busy_factor = 32, . &imbalance_pct = 125, . &cache_hot_time = (10*1000), . &cache_nice_tries = 1, . &per_cpu_gain = 100, . &flags =  &SD_LOAD_BALANCE |  &SD_BALANCE_EXEC |  &SD_WAKE_BALANCE, . &last_balance =  &jiffies, . &balance_interval = 1, . &nr_balance_failed = 0, };}' unless defined(&SD_NODE_INIT);
    } else {
	require 'asm-generic/topology.ph';
    }
}
1;
