require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_RWSEM_H)) {
    eval 'sub _I386_RWSEM_H () {1;}' unless defined(&_I386_RWSEM_H);
    unless(defined(&_LINUX_RWSEM_H)) {
	die("please don't include asm/rwsem.h directly, use linux/rwsem.h instead");
    }
    if(defined(&__KERNEL__)) {
	require 'linux/list.ph';
	require 'linux/spinlock.ph';
	eval 'sub RWSEM_UNLOCKED_VALUE () {0x;}' unless defined(&RWSEM_UNLOCKED_VALUE);
	eval 'sub RWSEM_ACTIVE_BIAS () {0x1;}' unless defined(&RWSEM_ACTIVE_BIAS);
	eval 'sub RWSEM_ACTIVE_MASK () {0xffff;}' unless defined(&RWSEM_ACTIVE_MASK);
	eval 'sub RWSEM_WAITING_BIAS () {(-0x10000);}' unless defined(&RWSEM_WAITING_BIAS);
	eval 'sub RWSEM_ACTIVE_READ_BIAS () { &RWSEM_ACTIVE_BIAS;}' unless defined(&RWSEM_ACTIVE_READ_BIAS);
	eval 'sub RWSEM_ACTIVE_WRITE_BIAS () {( &RWSEM_WAITING_BIAS +  &RWSEM_ACTIVE_BIAS);}' unless defined(&RWSEM_ACTIVE_WRITE_BIAS);
	if((defined(&RWSEM_DEBUG) ? &RWSEM_DEBUG : 0)) {
	}
	if((defined(&RWSEM_DEBUG) ? &RWSEM_DEBUG : 0)) {
	    eval 'sub __RWSEM_DEBUG_INIT () {, 0;}' unless defined(&__RWSEM_DEBUG_INIT);
	} else {
	    eval 'sub __RWSEM_DEBUG_INIT () {1;}' unless defined(&__RWSEM_DEBUG_INIT);
	}
	eval 'sub __RWSEM_INITIALIZER {
	    local($name) = @_;
    	    eval q({  &RWSEM_UNLOCKED_VALUE,  &SPIN_LOCK_UNLOCKED,  &LIST_HEAD_INIT(($name). &wait_list)  &__RWSEM_DEBUG_INIT });
	}' unless defined(&__RWSEM_INITIALIZER);
	eval 'sub DECLARE_RWSEM {
	    local($name) = @_;
    	    eval q(\'struct rw_semaphore\' $name =  &__RWSEM_INITIALIZER($name));
	}' unless defined(&DECLARE_RWSEM);
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub init_rwsem {
	    local($sem) = @_;
    	    eval q({  ($sem->{count}) =  &RWSEM_UNLOCKED_VALUE;  &spin_lock_init( ($sem->{wait_lock}));  &INIT_LIST_HEAD( ($sem->{wait_list})); });
	}' unless defined(&init_rwsem);
    }
}
1;
