require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ARCH_I386_ATOMIC__)) {
    eval 'sub __ARCH_I386_ATOMIC__ () {1;}' unless defined(&__ARCH_I386_ATOMIC__);
    require 'linux/compiler.ph';
    require 'asm/processor.ph';
    eval 'sub LOCK () {"lock ; ";}' unless defined(&LOCK);
    eval 'sub ATOMIC_INIT {
        local($i) = @_;
	    eval q({ ($i) });
    }' unless defined(&ATOMIC_INIT);
    eval 'sub atomic_read {
        local($v) = @_;
	    eval q((($v)-> &counter));
    }' unless defined(&atomic_read);
    eval 'sub atomic_set {
        local($v,$i) = @_;
	    eval q(((($v)-> &counter) = ($i)));
    }' unless defined(&atomic_set);
    if(defined(&CONFIG_M386)) {
    }
    if(defined(&CONFIG_M386)) {
    }
    eval 'sub atomic_sub_return {
        local($i,$v) = @_;
	    eval q({  &atomic_add_return(-$i,$v); });
    }' unless defined(&atomic_sub_return);
    eval 'sub atomic_inc_return {
        local($v) = @_;
	    eval q(( &atomic_add_return(1,$v)));
    }' unless defined(&atomic_inc_return);
    eval 'sub atomic_dec_return {
        local($v) = @_;
	    eval q(( &atomic_sub_return(1,$v)));
    }' unless defined(&atomic_dec_return);
    eval 'sub atomic_clear_mask {
        local($mask, $addr) = @_;
	    eval q( &__asm__  &__volatile__( &LOCK \\"andl %0,%1\\" : : \\"r\\" (~($mask)),\\"m\\" (*$addr) : \\"memory\\"));
    }' unless defined(&atomic_clear_mask);
    eval 'sub atomic_set_mask {
        local($mask, $addr) = @_;
	    eval q( &__asm__  &__volatile__( &LOCK \\"orl %0,%1\\" : : \\"r\\" ($mask),\\"m\\" (*($addr)) : \\"memory\\"));
    }' unless defined(&atomic_set_mask);
    eval 'sub smp_mb__before_atomic_dec () {
        eval q( &barrier());
    }' unless defined(&smp_mb__before_atomic_dec);
    eval 'sub smp_mb__after_atomic_dec () {
        eval q( &barrier());
    }' unless defined(&smp_mb__after_atomic_dec);
    eval 'sub smp_mb__before_atomic_inc () {
        eval q( &barrier());
    }' unless defined(&smp_mb__before_atomic_inc);
    eval 'sub smp_mb__after_atomic_inc () {
        eval q( &barrier());
    }' unless defined(&smp_mb__after_atomic_inc);
}
1;
