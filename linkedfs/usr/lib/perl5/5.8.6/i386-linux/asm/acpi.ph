require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_ACPI_H)) {
    eval 'sub _ASM_ACPI_H () {1;}' unless defined(&_ASM_ACPI_H);
    if(defined(&__KERNEL__)) {
	require 'asm/system.ph';
	eval 'sub COMPILER_DEPENDENT_INT64 () {\'long long\';}' unless defined(&COMPILER_DEPENDENT_INT64);
	eval 'sub COMPILER_DEPENDENT_UINT64 () {\'unsigned long long\';}' unless defined(&COMPILER_DEPENDENT_UINT64);
	eval 'sub ACPI_SYSTEM_XFACE () {1;}' unless defined(&ACPI_SYSTEM_XFACE);
	eval 'sub ACPI_EXTERNAL_XFACE () {1;}' unless defined(&ACPI_EXTERNAL_XFACE);
	eval 'sub ACPI_INTERNAL_XFACE () {1;}' unless defined(&ACPI_INTERNAL_XFACE);
	eval 'sub ACPI_INTERNAL_VAR_XFACE () {1;}' unless defined(&ACPI_INTERNAL_VAR_XFACE);
	eval 'sub ACPI_ASM_MACROS () {1;}' unless defined(&ACPI_ASM_MACROS);
	eval 'sub BREAKPOINT3 () {1;}' unless defined(&BREAKPOINT3);
	eval 'sub ACPI_DISABLE_IRQS () {
	    eval q( &local_irq_disable());
	}' unless defined(&ACPI_DISABLE_IRQS);
	eval 'sub ACPI_ENABLE_IRQS () {
	    eval q( &local_irq_enable());
	}' unless defined(&ACPI_ENABLE_IRQS);
	eval 'sub ACPI_FLUSH_CPU_CACHE () {
	    eval q( &wbinvd());
	}' unless defined(&ACPI_FLUSH_CPU_CACHE);
	eval 'sub __acpi_acquire_global_lock {
	    local($lock) = @_;
    	    eval q({ my $old,  &new,  &val;  &do {  $old = *$lock;  &new = ((( $old & ~0x3) + 2) + (( $old >> 1) & 0x1));  &val =  &cmpxchg($lock,  $old,  &new); }  &while ( &unlikely ( &val !=  $old)); ( &new < 3) ? -1: 0; });
	}' unless defined(&__acpi_acquire_global_lock);
	eval 'sub __acpi_release_global_lock {
	    local($lock) = @_;
    	    eval q({ my $old,  &new,  &val;  &do {  $old = *$lock;  &new =  $old & ~0x3;  &val =  &cmpxchg($lock,  $old,  &new); }  &while ( &unlikely ( &val !=  $old));  $old & 0x1; });
	}' unless defined(&__acpi_release_global_lock);
	eval 'sub ACPI_ACQUIRE_GLOBAL_LOCK {
	    local($GLptr, $Acq) = @_;
    	    eval q((($Acq) =  &__acpi_acquire_global_lock( $GLptr)));
	}' unless defined(&ACPI_ACQUIRE_GLOBAL_LOCK);
	eval 'sub ACPI_RELEASE_GLOBAL_LOCK {
	    local($GLptr, $Acq) = @_;
    	    eval q((($Acq) =  &__acpi_release_global_lock( $GLptr)));
	}' unless defined(&ACPI_RELEASE_GLOBAL_LOCK);
	eval 'sub ACPI_DIV_64_BY_32 {
	    local($n_hi, $n_lo, $d32, $q32, $r32) = @_;
    	    eval q( &asm(\\"divl %2;\\" :\\"=a\\"($q32), \\"=d\\"($r32) :\\"r\\"($d32), \\"0\\"($n_lo), \\"1\\"($n_hi)));
	}' unless defined(&ACPI_DIV_64_BY_32);
	eval 'sub ACPI_SHIFT_RIGHT_64 {
	    local($n_hi, $n_lo) = @_;
    	    eval q( &asm(\\"shrl   $1,%2;\\" \\"rcrl   $1,%3;\\" :\\"=r\\"($n_hi), \\"=r\\"($n_lo) :\\"0\\"($n_hi), \\"1\\"($n_lo)));
	}' unless defined(&ACPI_SHIFT_RIGHT_64);
	eval 'sub ACPI_PDC_EST_CAPABILITY_SMP () {0xa;}' unless defined(&ACPI_PDC_EST_CAPABILITY_SMP);
	eval 'sub ACPI_PDC_EST_CAPABILITY_MSR () {0x1;}' unless defined(&ACPI_PDC_EST_CAPABILITY_MSR);
	eval 'sub ACPI_PDC_EST_CAPABILITY_SMP () {0xa;}' unless defined(&ACPI_PDC_EST_CAPABILITY_SMP);
	eval 'sub ACPI_PDC_EST_CAPABILITY_MSR () {0x1;}' unless defined(&ACPI_PDC_EST_CAPABILITY_MSR);
	if(defined(&CONFIG_ACPI_BOOT)) {
	    eval 'sub disable_acpi {
	        local($void) = @_;
    		eval q({  &acpi_disabled = 1;  &acpi_ht = 0;  &acpi_pci_disabled = 1;  &acpi_noirq = 1; });
	    }' unless defined(&disable_acpi);
	    eval 'sub FIX_ACPI_PAGES () {4;}' unless defined(&FIX_ACPI_PAGES);
	    if(defined(&CONFIG_X86_IO_APIC)) {
		eval 'sub disable_ioapic_setup {
		    local($void) = @_;
    		    eval q({  &skip_ioapic_setup = 1; });
		}' unless defined(&disable_ioapic_setup);
		eval 'sub ioapic_setup_disabled {
		    local($void) = @_;
    		    eval q({  &skip_ioapic_setup; });
		}' unless defined(&ioapic_setup_disabled);
	    } else {
	    }
	} else {
	    eval 'sub acpi_lapic () {0;}' unless defined(&acpi_lapic);
	    eval 'sub acpi_ioapic () {0;}' unless defined(&acpi_ioapic);
	}
	if(defined(&CONFIG_ACPI_PCI)) {
	} else {
	}
    }
}
1;
