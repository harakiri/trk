require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_ERRNO_H)) {
    eval 'sub _I386_ERRNO_H () {1;}' unless defined(&_I386_ERRNO_H);
    require 'asm-generic/errno.ph';
}
1;
