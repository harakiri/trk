require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_APICDEF_H)) {
    eval 'sub __ASM_APICDEF_H () {1;}' unless defined(&__ASM_APICDEF_H);
    eval 'sub APIC_DEFAULT_PHYS_BASE () {0xfee00000;}' unless defined(&APIC_DEFAULT_PHYS_BASE);
    eval 'sub APIC_ID () {0x20;}' unless defined(&APIC_ID);
    eval 'sub APIC_LVR () {0x30;}' unless defined(&APIC_LVR);
    eval 'sub APIC_LVR_MASK () {0xff00ff;}' unless defined(&APIC_LVR_MASK);
    eval 'sub GET_APIC_VERSION {
        local($x) = @_;
	    eval q((($x)&0xff));
    }' unless defined(&GET_APIC_VERSION);
    eval 'sub GET_APIC_MAXLVT {
        local($x) = @_;
	    eval q(((($x)>>16)&0xff));
    }' unless defined(&GET_APIC_MAXLVT);
    eval 'sub APIC_INTEGRATED {
        local($x) = @_;
	    eval q((($x)&0xf0));
    }' unless defined(&APIC_INTEGRATED);
    eval 'sub APIC_TASKPRI () {0x80;}' unless defined(&APIC_TASKPRI);
    eval 'sub APIC_TPRI_MASK () {0xff;}' unless defined(&APIC_TPRI_MASK);
    eval 'sub APIC_ARBPRI () {0x90;}' unless defined(&APIC_ARBPRI);
    eval 'sub APIC_ARBPRI_MASK () {0xff;}' unless defined(&APIC_ARBPRI_MASK);
    eval 'sub APIC_PROCPRI () {0xa0;}' unless defined(&APIC_PROCPRI);
    eval 'sub APIC_EOI () {0xb0;}' unless defined(&APIC_EOI);
    eval 'sub APIC_EIO_ACK () {0x;}' unless defined(&APIC_EIO_ACK);
    eval 'sub APIC_RRR () {0xc0;}' unless defined(&APIC_RRR);
    eval 'sub APIC_LDR () {0xd0;}' unless defined(&APIC_LDR);
    eval 'sub APIC_LDR_MASK () {(0xff<<24);}' unless defined(&APIC_LDR_MASK);
    eval 'sub GET_APIC_LOGICAL_ID {
        local($x) = @_;
	    eval q(((($x)>>24)&0xff));
    }' unless defined(&GET_APIC_LOGICAL_ID);
    eval 'sub SET_APIC_LOGICAL_ID {
        local($x) = @_;
	    eval q(((($x)<<24)));
    }' unless defined(&SET_APIC_LOGICAL_ID);
    eval 'sub APIC_ALL_CPUS () {0xff;}' unless defined(&APIC_ALL_CPUS);
    eval 'sub APIC_DFR () {0xe0;}' unless defined(&APIC_DFR);
    eval 'sub APIC_DFR_CLUSTER () {0xfffffff;}' unless defined(&APIC_DFR_CLUSTER);
    eval 'sub APIC_DFR_FLAT () {0xffffffff;}' unless defined(&APIC_DFR_FLAT);
    eval 'sub APIC_SPIV () {0xf0;}' unless defined(&APIC_SPIV);
    eval 'sub APIC_SPIV_FOCUS_DISABLED () {(1<<9);}' unless defined(&APIC_SPIV_FOCUS_DISABLED);
    eval 'sub APIC_SPIV_APIC_ENABLED () {(1<<8);}' unless defined(&APIC_SPIV_APIC_ENABLED);
    eval 'sub APIC_ISR () {0x100;}' unless defined(&APIC_ISR);
    eval 'sub APIC_TMR () {0x180;}' unless defined(&APIC_TMR);
    eval 'sub APIC_IRR () {0x200;}' unless defined(&APIC_IRR);
    eval 'sub APIC_ESR () {0x280;}' unless defined(&APIC_ESR);
    eval 'sub APIC_ESR_SEND_CS () {0x1;}' unless defined(&APIC_ESR_SEND_CS);
    eval 'sub APIC_ESR_RECV_CS () {0x2;}' unless defined(&APIC_ESR_RECV_CS);
    eval 'sub APIC_ESR_SEND_ACC () {0x4;}' unless defined(&APIC_ESR_SEND_ACC);
    eval 'sub APIC_ESR_RECV_ACC () {0x8;}' unless defined(&APIC_ESR_RECV_ACC);
    eval 'sub APIC_ESR_SENDILL () {0x20;}' unless defined(&APIC_ESR_SENDILL);
    eval 'sub APIC_ESR_RECVILL () {0x40;}' unless defined(&APIC_ESR_RECVILL);
    eval 'sub APIC_ESR_ILLREGA () {0x80;}' unless defined(&APIC_ESR_ILLREGA);
    eval 'sub APIC_ICR () {0x300;}' unless defined(&APIC_ICR);
    eval 'sub APIC_DEST_SELF () {0x40000;}' unless defined(&APIC_DEST_SELF);
    eval 'sub APIC_DEST_ALLINC () {0x80000;}' unless defined(&APIC_DEST_ALLINC);
    eval 'sub APIC_DEST_ALLBUT () {0xc0000;}' unless defined(&APIC_DEST_ALLBUT);
    eval 'sub APIC_ICR_RR_MASK () {0x30000;}' unless defined(&APIC_ICR_RR_MASK);
    eval 'sub APIC_ICR_RR_INVALID () {0x;}' unless defined(&APIC_ICR_RR_INVALID);
    eval 'sub APIC_ICR_RR_INPROG () {0x10000;}' unless defined(&APIC_ICR_RR_INPROG);
    eval 'sub APIC_ICR_RR_VALID () {0x20000;}' unless defined(&APIC_ICR_RR_VALID);
    eval 'sub APIC_INT_LEVELTRIG () {0x8000;}' unless defined(&APIC_INT_LEVELTRIG);
    eval 'sub APIC_INT_ASSERT () {0x4000;}' unless defined(&APIC_INT_ASSERT);
    eval 'sub APIC_ICR_BUSY () {0x1000;}' unless defined(&APIC_ICR_BUSY);
    eval 'sub APIC_DEST_LOGICAL () {0x800;}' unless defined(&APIC_DEST_LOGICAL);
    eval 'sub APIC_DM_FIXED () {0x;}' unless defined(&APIC_DM_FIXED);
    eval 'sub APIC_DM_LOWEST () {0x100;}' unless defined(&APIC_DM_LOWEST);
    eval 'sub APIC_DM_SMI () {0x200;}' unless defined(&APIC_DM_SMI);
    eval 'sub APIC_DM_REMRD () {0x300;}' unless defined(&APIC_DM_REMRD);
    eval 'sub APIC_DM_NMI () {0x400;}' unless defined(&APIC_DM_NMI);
    eval 'sub APIC_DM_INIT () {0x500;}' unless defined(&APIC_DM_INIT);
    eval 'sub APIC_DM_STARTUP () {0x600;}' unless defined(&APIC_DM_STARTUP);
    eval 'sub APIC_DM_EXTINT () {0x700;}' unless defined(&APIC_DM_EXTINT);
    eval 'sub APIC_VECTOR_MASK () {0xff;}' unless defined(&APIC_VECTOR_MASK);
    eval 'sub APIC_ICR2 () {0x310;}' unless defined(&APIC_ICR2);
    eval 'sub GET_APIC_DEST_FIELD {
        local($x) = @_;
	    eval q(((($x)>>24)&0xff));
    }' unless defined(&GET_APIC_DEST_FIELD);
    eval 'sub SET_APIC_DEST_FIELD {
        local($x) = @_;
	    eval q((($x)<<24));
    }' unless defined(&SET_APIC_DEST_FIELD);
    eval 'sub APIC_LVTT () {0x320;}' unless defined(&APIC_LVTT);
    eval 'sub APIC_LVTTHMR () {0x330;}' unless defined(&APIC_LVTTHMR);
    eval 'sub APIC_LVTPC () {0x340;}' unless defined(&APIC_LVTPC);
    eval 'sub APIC_LVT0 () {0x350;}' unless defined(&APIC_LVT0);
    eval 'sub APIC_LVT_TIMER_BASE_MASK () {(0x3<<18);}' unless defined(&APIC_LVT_TIMER_BASE_MASK);
    eval 'sub GET_APIC_TIMER_BASE {
        local($x) = @_;
	    eval q(((($x)>>18)&0x3));
    }' unless defined(&GET_APIC_TIMER_BASE);
    eval 'sub SET_APIC_TIMER_BASE {
        local($x) = @_;
	    eval q(((($x)<<18)));
    }' unless defined(&SET_APIC_TIMER_BASE);
    eval 'sub APIC_TIMER_BASE_CLKIN () {0x;}' unless defined(&APIC_TIMER_BASE_CLKIN);
    eval 'sub APIC_TIMER_BASE_TMBASE () {0x1;}' unless defined(&APIC_TIMER_BASE_TMBASE);
    eval 'sub APIC_TIMER_BASE_DIV () {0x2;}' unless defined(&APIC_TIMER_BASE_DIV);
    eval 'sub APIC_LVT_TIMER_PERIODIC () {(1<<17);}' unless defined(&APIC_LVT_TIMER_PERIODIC);
    eval 'sub APIC_LVT_MASKED () {(1<<16);}' unless defined(&APIC_LVT_MASKED);
    eval 'sub APIC_LVT_LEVEL_TRIGGER () {(1<<15);}' unless defined(&APIC_LVT_LEVEL_TRIGGER);
    eval 'sub APIC_LVT_REMOTE_IRR () {(1<<14);}' unless defined(&APIC_LVT_REMOTE_IRR);
    eval 'sub APIC_INPUT_POLARITY () {(1<<13);}' unless defined(&APIC_INPUT_POLARITY);
    eval 'sub APIC_SEND_PENDING () {(1<<12);}' unless defined(&APIC_SEND_PENDING);
    eval 'sub GET_APIC_DELIVERY_MODE {
        local($x) = @_;
	    eval q(((($x)>>8)&0x7));
    }' unless defined(&GET_APIC_DELIVERY_MODE);
    eval 'sub SET_APIC_DELIVERY_MODE {
        local($x,$y) = @_;
	    eval q(((($x)&~0x700)|(($y)<<8)));
    }' unless defined(&SET_APIC_DELIVERY_MODE);
    eval 'sub APIC_MODE_FIXED () {0x;}' unless defined(&APIC_MODE_FIXED);
    eval 'sub APIC_MODE_NMI () {0x4;}' unless defined(&APIC_MODE_NMI);
    eval 'sub APIC_MODE_EXINT () {0x7;}' unless defined(&APIC_MODE_EXINT);
    eval 'sub APIC_LVT1 () {0x360;}' unless defined(&APIC_LVT1);
    eval 'sub APIC_LVTERR () {0x370;}' unless defined(&APIC_LVTERR);
    eval 'sub APIC_TMICT () {0x380;}' unless defined(&APIC_TMICT);
    eval 'sub APIC_TMCCT () {0x390;}' unless defined(&APIC_TMCCT);
    eval 'sub APIC_TDCR () {0x3e0;}' unless defined(&APIC_TDCR);
    eval 'sub APIC_TDR_DIV_TMBASE () {(1<<2);}' unless defined(&APIC_TDR_DIV_TMBASE);
    eval 'sub APIC_TDR_DIV_1 () {0xb;}' unless defined(&APIC_TDR_DIV_1);
    eval 'sub APIC_TDR_DIV_2 () {0x;}' unless defined(&APIC_TDR_DIV_2);
    eval 'sub APIC_TDR_DIV_4 () {0x1;}' unless defined(&APIC_TDR_DIV_4);
    eval 'sub APIC_TDR_DIV_8 () {0x2;}' unless defined(&APIC_TDR_DIV_8);
    eval 'sub APIC_TDR_DIV_16 () {0x3;}' unless defined(&APIC_TDR_DIV_16);
    eval 'sub APIC_TDR_DIV_32 () {0x8;}' unless defined(&APIC_TDR_DIV_32);
    eval 'sub APIC_TDR_DIV_64 () {0x9;}' unless defined(&APIC_TDR_DIV_64);
    eval 'sub APIC_TDR_DIV_128 () {0xa;}' unless defined(&APIC_TDR_DIV_128);
    eval 'sub APIC_BASE () {( &fix_to_virt( &FIX_APIC_BASE));}' unless defined(&APIC_BASE);
    if(defined(&CONFIG_NUMA)) {
	eval 'sub MAX_IO_APICS () {32;}' unless defined(&MAX_IO_APICS);
    } else {
	eval 'sub MAX_IO_APICS () {8;}' unless defined(&MAX_IO_APICS);
    }
    eval 'sub u32 () {\'unsigned int\';}' unless defined(&u32);
    eval 'sub lapic () {(( &volatile \'struct local_apic\' *) &APIC_BASE);}' unless defined(&lapic);
    undef(&u32) if defined(&u32);
}
1;
