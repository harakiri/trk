require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_MPSPEC_H)) {
    eval 'sub __ASM_MPSPEC_H () {1;}' unless defined(&__ASM_MPSPEC_H);
    require 'linux/cpumask.ph';
    require 'asm/mpspec_def.ph';
    require 'mach_mpspec.ph';
    if(defined(&CONFIG_ACPI_BOOT)) {
    }
    eval 'sub PHYSID_ARRAY_SIZE () { &BITS_TO_LONGS( &MAX_APICS);}' unless defined(&PHYSID_ARRAY_SIZE);
    eval 'sub physid_set {
        local($physid, $map) = @_;
	    eval q( &set_bit($physid, ($map). &mask));
    }' unless defined(&physid_set);
    eval 'sub physid_clear {
        local($physid, $map) = @_;
	    eval q( &clear_bit($physid, ($map). &mask));
    }' unless defined(&physid_clear);
    eval 'sub physid_isset {
        local($physid, $map) = @_;
	    eval q( &test_bit($physid, ($map). &mask));
    }' unless defined(&physid_isset);
    eval 'sub physid_test_and_set {
        local($physid, $map) = @_;
	    eval q( &test_and_set_bit($physid, ($map). &mask));
    }' unless defined(&physid_test_and_set);
    eval 'sub physids_and {
        local($dst, $src1, $src2) = @_;
	    eval q( &bitmap_and(($dst). &mask, ($src1). &mask, ($src2). &mask,  &MAX_APICS));
    }' unless defined(&physids_and);
    eval 'sub physids_or {
        local($dst, $src1, $src2) = @_;
	    eval q( &bitmap_or(($dst). &mask, ($src1). &mask, ($src2). &mask,  &MAX_APICS));
    }' unless defined(&physids_or);
    eval 'sub physids_clear {
        local($map) = @_;
	    eval q( &bitmap_zero(($map). &mask,  &MAX_APICS));
    }' unless defined(&physids_clear);
    eval 'sub physids_complement {
        local($dst, $src) = @_;
	    eval q( &bitmap_complement(($dst). &mask,($src). &mask,  &MAX_APICS));
    }' unless defined(&physids_complement);
    eval 'sub physids_empty {
        local($map) = @_;
	    eval q( &bitmap_empty(($map). &mask,  &MAX_APICS));
    }' unless defined(&physids_empty);
    eval 'sub physids_equal {
        local($map1, $map2) = @_;
	    eval q( &bitmap_equal(($map1). &mask, ($map2). &mask,  &MAX_APICS));
    }' unless defined(&physids_equal);
    eval 'sub physids_weight {
        local($map) = @_;
	    eval q( &bitmap_weight(($map). &mask,  &MAX_APICS));
    }' unless defined(&physids_weight);
    eval 'sub physids_shift_right {
        local($d, $s, $n) = @_;
	    eval q( &bitmap_shift_right(($d). &mask, ($s). &mask, $n,  &MAX_APICS));
    }' unless defined(&physids_shift_right);
    eval 'sub physids_shift_left {
        local($d, $s, $n) = @_;
	    eval q( &bitmap_shift_left(($d). &mask, ($s). &mask, $n,  &MAX_APICS));
    }' unless defined(&physids_shift_left);
    eval 'sub physids_coerce {
        local($map) = @_;
	    eval q((($map). $mask[0]));
    }' unless defined(&physids_coerce);
    eval 'sub physids_promote {
        local($physids) = @_;
	    eval q(({  &physid_mask_t  &__physid_mask =  &PHYSID_MASK_NONE;  ($__physid_mask->{mask[0]}) = $physids;  &__physid_mask; }));
    }' unless defined(&physids_promote);
    eval 'sub physid_mask_of_physid {
        local($physid) = @_;
	    eval q(({  &physid_mask_t  &__physid_mask =  &PHYSID_MASK_NONE;  &physid_set($physid,  &__physid_mask);  &__physid_mask; }));
    }' unless defined(&physid_mask_of_physid);
    eval 'sub PHYSID_MASK_ALL () {{ {[0...  &PHYSID_ARRAY_SIZE-1] = ~0} };}' unless defined(&PHYSID_MASK_ALL);
    eval 'sub PHYSID_MASK_NONE () {{ {[0...  &PHYSID_ARRAY_SIZE-1] = 0} };}' unless defined(&PHYSID_MASK_NONE);
}
1;
