require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_BUG_H)) {
    eval 'sub _I386_BUG_H () {1;}' unless defined(&_I386_BUG_H);
    require 'linux/config.ph';
    if(1) {
	eval 'sub BUG () {
	    eval q( &__asm__  &__volatile__( \\"ud2\\\\n\\" \\"\\\\t.word %c0\\\\n\\" \\"\\\\t.long %c1\\\\n\\" : : \\"i\\" ( &__LINE__), \\"i\\" ( &__FILE__)));
	}' unless defined(&BUG);
    } else {
	eval 'sub BUG () {
	    eval q( &__asm__  &__volatile__(\\"ud2\\\\n\\"));
	}' unless defined(&BUG);
    }
    eval 'sub HAVE_ARCH_BUG () {1;}' unless defined(&HAVE_ARCH_BUG);
    require 'asm-generic/bug.ph';
}
1;
