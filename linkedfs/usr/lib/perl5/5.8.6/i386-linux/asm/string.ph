require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_STRING_H_)) {
    eval 'sub _I386_STRING_H_ () {1;}' unless defined(&_I386_STRING_H_);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	if(!defined( &IN_STRING_C)) {
	    eval 'sub __HAVE_ARCH_STRCPY () {1;}' unless defined(&__HAVE_ARCH_STRCPY);
	    eval 'sub strcpy {
	        local($dest,$src) = @_;
    		eval q({ \'int\'  &d0,  &d1,  &d2;  &__asm__  &__volatile__( \\"1:\\\\tlodsb\\\\n\\\\t\\" \\"stosb\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\" : \\"=&S\\" ( &d0), \\"=&D\\" ( &d1), \\"=&a\\" ( &d2) :\\"0\\" ($src),\\"1\\" ($dest) : \\"memory\\"); $dest; });
	    }' unless defined(&strcpy);
	    eval 'sub __HAVE_ARCH_STRNCPY () {1;}' unless defined(&__HAVE_ARCH_STRNCPY);
	    eval 'sub strncpy {
	        local($dest,$src,$count) = @_;
    		eval q({ \'int\'  &d0,  &d1,  &d2,  &d3;  &__asm__  &__volatile__( \\"1:\\\\tdecl %2\\\\n\\\\t\\" \\"js 2f\\\\n\\\\t\\" \\"lodsb\\\\n\\\\t\\" \\"stosb\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\\\n\\\\t\\" \\"rep\\\\n\\\\t\\" \\"stosb\\\\n\\" \\"2:\\" : \\"=&S\\" ( &d0), \\"=&D\\" ( &d1), \\"=&c\\" ( &d2), \\"=&a\\" ( &d3) :\\"0\\" ($src),\\"1\\" ($dest),\\"2\\" ($count) : \\"memory\\"); $dest; });
	    }' unless defined(&strncpy);
	    eval 'sub __HAVE_ARCH_STRCAT () {1;}' unless defined(&__HAVE_ARCH_STRCAT);
	    eval 'sub strcat {
	        local($dest,$src) = @_;
    		eval q({ \'int\'  &d0,  &d1,  &d2,  &d3;  &__asm__  &__volatile__( \\"repne\\\\n\\\\t\\" \\"scasb\\\\n\\\\t\\" \\"decl %1\\\\n\\" \\"1:\\\\tlodsb\\\\n\\\\t\\" \\"stosb\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\" : \\"=&S\\" ( &d0), \\"=&D\\" ( &d1), \\"=&a\\" ( &d2), \\"=&c\\" ( &d3) : \\"0\\" ($src), \\"1\\" ($dest), \\"2\\" (0), \\"3\\" (0xffffffff):\\"memory\\"); $dest; });
	    }' unless defined(&strcat);
	    eval 'sub __HAVE_ARCH_STRNCAT () {1;}' unless defined(&__HAVE_ARCH_STRNCAT);
	    eval 'sub strncat {
	        local($dest,$src,$count) = @_;
    		eval q({ \'int\'  &d0,  &d1,  &d2,  &d3;  &__asm__  &__volatile__( \\"repne\\\\n\\\\t\\" \\"scasb\\\\n\\\\t\\" \\"decl %1\\\\n\\\\t\\" \\"movl %8,%3\\\\n\\" \\"1:\\\\tdecl %3\\\\n\\\\t\\" \\"js 2f\\\\n\\\\t\\" \\"lodsb\\\\n\\\\t\\" \\"stosb\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\\\n\\" \\"2:\\\\txorl %2,%2\\\\n\\\\t\\" \\"stosb\\" : \\"=&S\\" ( &d0), \\"=&D\\" ( &d1), \\"=&a\\" ( &d2), \\"=&c\\" ( &d3) : \\"0\\" ($src),\\"1\\" ($dest),\\"2\\" (0),\\"3\\" (0xffffffff), \\"g\\" ($count) : \\"memory\\"); $dest; });
	    }' unless defined(&strncat);
	    eval 'sub __HAVE_ARCH_STRCMP () {1;}' unless defined(&__HAVE_ARCH_STRCMP);
	    eval 'sub strcmp {
	        local($cs,$ct) = @_;
    		eval q({ \'int\'  &d0,  &d1;  &register \'int\'  &__res;  &__asm__  &__volatile__( \\"1:\\\\tlodsb\\\\n\\\\t\\" \\"scasb\\\\n\\\\t\\" \\"jne 2f\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\\\n\\\\t\\" \\"xorl %%eax,%%eax\\\\n\\\\t\\" \\"jmp 3f\\\\n\\" \\"2:\\\\tsbbl %%eax,%%eax\\\\n\\\\t\\" \\"orb $1,%%al\\\\n\\" \\"3:\\" :\\"=a\\" ( &__res), \\"=&S\\" ( &d0), \\"=&D\\" ( &d1) :\\"1\\" ($cs),\\"2\\" ($ct));  &__res; });
	    }' unless defined(&strcmp);
	    eval 'sub __HAVE_ARCH_STRNCMP () {1;}' unless defined(&__HAVE_ARCH_STRNCMP);
	    eval 'sub strncmp {
	        local($cs,$ct,$count) = @_;
    		eval q({  &register \'int\'  &__res; \'int\'  &d0,  &d1,  &d2;  &__asm__  &__volatile__( \\"1:\\\\tdecl %3\\\\n\\\\t\\" \\"js 2f\\\\n\\\\t\\" \\"lodsb\\\\n\\\\t\\" \\"scasb\\\\n\\\\t\\" \\"jne 3f\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\\\n\\" \\"2:\\\\txorl %%eax,%%eax\\\\n\\\\t\\" \\"jmp 4f\\\\n\\" \\"3:\\\\tsbbl %%eax,%%eax\\\\n\\\\t\\" \\"orb $1,%%al\\\\n\\" \\"4:\\" :\\"=a\\" ( &__res), \\"=&S\\" ( &d0), \\"=&D\\" ( &d1), \\"=&c\\" ( &d2) :\\"1\\" ($cs),\\"2\\" ($ct),\\"3\\" ($count));  &__res; });
	    }' unless defined(&strncmp);
	    eval 'sub __HAVE_ARCH_STRCHR () {1;}' unless defined(&__HAVE_ARCH_STRCHR);
	    eval 'sub strchr {
	        local($s,$c) = @_;
    		eval q({ \'int\'  &d0;  &register \'char\' *  &__res;  &__asm__  &__volatile__( \\"movb %%al,%%ah\\\\n\\" \\"1:\\\\tlodsb\\\\n\\\\t\\" \\"cmpb %%ah,%%al\\\\n\\\\t\\" \\"je 2f\\\\n\\\\t\\" \\"testb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\\\n\\\\t\\" \\"movl $1,%1\\\\n\\" \\"2:\\\\tmovl %1,%0\\\\n\\\\t\\" \\"decl %0\\" :\\"=a\\" ( &__res), \\"=&S\\" ( &d0) : \\"1\\" ($s),\\"0\\" ($c));  &__res; });
	    }' unless defined(&strchr);
	    eval 'sub __HAVE_ARCH_STRRCHR () {1;}' unless defined(&__HAVE_ARCH_STRRCHR);
	    eval 'sub strrchr {
	        local($s,$c) = @_;
    		eval q({ \'int\'  &d0,  &d1;  &register \'char\' *  &__res;  &__asm__  &__volatile__( \\"movb %%al,%%ah\\\\n\\" \\"1:\\\\tlodsb\\\\n\\\\t\\" \\"cmpb %%ah,%%al\\\\n\\\\t\\" \\"jne 2f\\\\n\\\\t\\" \\"leal -1(%%esi),%0\\\\n\\" \\"2:\\\\ttestb %%al,%%al\\\\n\\\\t\\" \\"jne 1b\\" :\\"=g\\" ( &__res), \\"=&S\\" ( &d0), \\"=&a\\" ( &d1) :\\"0\\" (0),\\"1\\" ($s),\\"2\\" ($c));  &__res; });
	    }' unless defined(&strrchr);
	}
	eval 'sub __HAVE_ARCH_STRLEN () {1;}' unless defined(&__HAVE_ARCH_STRLEN);
	eval 'sub strlen {
	    local($s) = @_;
    	    eval q({ \'int\'  &d0;  &register \'int\'  &__res;  &__asm__  &__volatile__( \\"repne\\\\n\\\\t\\" \\"scasb\\\\n\\\\t\\" \\"notl %0\\\\n\\\\t\\" \\"decl %0\\" :\\"=c\\" ( &__res), \\"=&D\\" ( &d0) :\\"1\\" ($s),\\"a\\" (0), \\"0\\" (0xffffffff));  &__res; });
	}' unless defined(&strlen);
	eval 'sub __memcpy {
	    local($to,$from,$n) = @_;
    	    eval q({ \'int\'  &d0,  &d1,  &d2;  &__asm__  &__volatile__( \\"rep ; movsl\\\\n\\\\t\\" \\"testb $2,%b4\\\\n\\\\t\\" \\"je 1f\\\\n\\\\t\\" \\"movsw\\\\n\\" \\"1:\\\\ttestb $1,%b4\\\\n\\\\t\\" \\"je 2f\\\\n\\\\t\\" \\"movsb\\\\n\\" \\"2:\\" : \\"=&c\\" , \\"=&D\\" , \\"=&S\\" :\\"0\\" ($n/4), \\"q\\" ,\\"1\\" ,\\"2\\" ( $from) : \\"memory\\"); ($to); });
	}' unless defined(&__memcpy);
	eval 'sub COMMON {
	    local($x) = @_;
    	    eval q( &__asm__  &__volatile__( \\"rep ; movsl\\" $x : \\"=&c\\" , \\"=&D\\" , \\"=&S\\" : \\"0\\" ( &n/4),\\"1\\" ,\\"2\\" (  &from) : \\"memory\\"););
	}' unless defined(&COMMON);
	undef(&COMMON) if defined(&COMMON);
	eval 'sub __HAVE_ARCH_MEMCPY () {1;}' unless defined(&__HAVE_ARCH_MEMCPY);
	if(defined(&CONFIG_X86_USE_3DNOW)) {
	    require 'asm/mmx.ph';
	    eval 'sub void {
	        eval q(* &__memcpy3d( &void * &to,  &const  &void * &from, \'size_t\'  &len) {  &if ( &len < 512)  &__memcpy( &to,  &from,  &len);  &_mmx_memcpy( &to,  &from,  &len); });
	    }' unless defined(&void);
	    eval 'sub memcpy {
	        local($t, $f, $n) = @_;
    		eval q(( &__builtin_constant_p($n) ?  &__constant_memcpy3d(($t),($f),($n)) :  &__memcpy3d(($t),($f),($n))));
	    }' unless defined(&memcpy);
	} else {
	    eval 'sub memcpy {
	        local($t, $f, $n) = @_;
    		eval q(( &__builtin_constant_p($n) ?  &__constant_memcpy(($t),($f),($n)) :  &__memcpy(($t),($f),($n))));
	    }' unless defined(&memcpy);
	}
	eval 'sub __HAVE_ARCH_MEMMOVE () {1;}' unless defined(&__HAVE_ARCH_MEMMOVE);
	if(defined( &__OPTIMIZE_SIZE__)) {
	    eval 'sub __HAVE_ARCH_MEMCMP () {1;}' unless defined(&__HAVE_ARCH_MEMCMP);
	    eval 'sub __attribute__ {
	        eval q((( &always_inline)) \'int\'  &memcmp( &const  &void *  &cs, &const  &void *  &ct,\'size_t\'  &count) {  &const \'unsigned char\' * &su1, * &su2; \'int\'  &res = 0;  &for(  &su1 =  &cs,  &su2 =  &ct; 0<  &count; ++ &su1, ++ &su2,  &count--)  &if (( &res = * &su1 - * &su2) != 0)  &break;  &res; });
	    }' unless defined(&__attribute__);
	} else {
	    eval 'sub memcmp () { &__builtin_memcmp;}' unless defined(&memcmp);
	}
	eval 'sub __HAVE_ARCH_MEMCHR () {1;}' unless defined(&__HAVE_ARCH_MEMCHR);
	eval 'sub memchr {
	    local($cs,$c,$count) = @_;
    	    eval q({ \'int\'  &d0;  &register  &void *  &__res;  &if (!$count)  &NULL;  &__asm__  &__volatile__( \\"repne\\\\n\\\\t\\" \\"scasb\\\\n\\\\t\\" \\"je 1f\\\\n\\\\t\\" \\"movl $1,%0\\\\n\\" \\"1:\\\\tdecl %0\\" :\\"=D\\" ( &__res), \\"=&c\\" ( &d0) : \\"a\\" ($c),\\"0\\" ($cs),\\"1\\" ($count));  &__res; });
	}' unless defined(&memchr);
	eval 'sub __memset_generic {
	    local($s,$c,$count) = @_;
    	    eval q({ \'int\'  &d0,  &d1;  &__asm__  &__volatile__( \\"rep\\\\n\\\\t\\" \\"stosb\\" : \\"=&c\\" ( &d0), \\"=&D\\" ( &d1) :\\"a\\" ($c),\\"1\\" ($s),\\"0\\" ($count) :\\"memory\\"); $s; });
	}' unless defined(&__memset_generic);
	eval 'sub __constant_count_memset {
	    local($s,$c,$count) = @_;
    	    eval q( &__memset_generic(($s),($c),($count)));
	}' unless defined(&__constant_count_memset);
	eval 'sub __HAVE_ARCH_STRNLEN () {1;}' unless defined(&__HAVE_ARCH_STRNLEN);
	eval 'sub strnlen {
	    local($s,$count) = @_;
    	    eval q({ \'int\'  &d0;  &register \'int\'  &__res;  &__asm__  &__volatile__( \\"movl %2,%0\\\\n\\\\t\\" \\"jmp 2f\\\\n\\" \\"1:\\\\tcmpb $0,(%0)\\\\n\\\\t\\" \\"je 3f\\\\n\\\\t\\" \\"incl %0\\\\n\\" \\"2:\\\\tdecl %1\\\\n\\\\t\\" \\"cmpl $-1,%1\\\\n\\\\t\\" \\"jne 1b\\\\n\\" \\"3:\\\\tsubl %2,%0\\" :\\"=a\\" ( &__res), \\"=&d\\" ( &d0) :\\"c\\" ($s),\\"1\\" ($count));  &__res; });
	}' unless defined(&strnlen);
	eval 'sub __HAVE_ARCH_STRSTR () {1;}' unless defined(&__HAVE_ARCH_STRSTR);
	eval 'sub COMMON {
	    local($x) = @_;
    	    eval q( &__asm__  &__volatile__( \\"rep ; stosl\\" $x : \\"=&c\\" , \\"=&D\\" : \\"a\\" ,\\"0\\" ( &count/4),\\"1\\" (  &s) : \\"memory\\"));
	}' unless defined(&COMMON);
	undef(&COMMON) if defined(&COMMON);
	eval 'sub __constant_c_x_memset {
	    local($s, $c, $count) = @_;
    	    eval q(( &__builtin_constant_p($count) ?  &__constant_c_and_count_memset(($s),($c),($count)) :  &__constant_c_memset(($s),($c),($count))));
	}' unless defined(&__constant_c_x_memset);
	eval 'sub __memset {
	    local($s, $c, $count) = @_;
    	    eval q(( &__builtin_constant_p($count) ?  &__constant_count_memset(($s),($c),($count)) :  &__memset_generic(($s),($c),($count))));
	}' unless defined(&__memset);
	eval 'sub __HAVE_ARCH_MEMSET () {1;}' unless defined(&__HAVE_ARCH_MEMSET);
	eval 'sub memset {
	    local($s, $c, $count) = @_;
    	    eval q(( &__builtin_constant_p($c) ?  &__constant_c_x_memset(($s),(0x1010101*($c)),($count)) :  &__memset(($s),($c),($count))));
	}' unless defined(&memset);
	eval 'sub __HAVE_ARCH_MEMSCAN () {1;}' unless defined(&__HAVE_ARCH_MEMSCAN);
	eval 'sub memscan {
	    local($addr,$c,$size) = @_;
    	    eval q({  &if (!$size) $addr;  &__asm__(\\"repnz; scasb\\\\n\\\\t\\" \\"jnz 1f\\\\n\\\\t\\" \\"dec %%edi\\\\n\\" \\"1:\\" : \\"=D\\" ($addr), \\"=c\\" ($size) : \\"0\\" ($addr), \\"1\\" ($size), \\"a\\" ($c)); $addr; });
	}' unless defined(&memscan);
    }
}
1;
