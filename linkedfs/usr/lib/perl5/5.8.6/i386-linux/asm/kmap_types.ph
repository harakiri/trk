require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_KMAP_TYPES_H)) {
    eval 'sub _ASM_KMAP_TYPES_H () {1;}' unless defined(&_ASM_KMAP_TYPES_H);
    require 'linux/config.ph';
    if(defined(&CONFIG_DEBUG_HIGHMEM)) {
	eval 'sub D {
	    local($n) = @_;
    	    eval q( &__KM_FENCE_$n ,);
	}' unless defined(&D);
    } else {
	eval 'sub D {
	    local($n) = @_;
    	    eval q();
	}' unless defined(&D);
    }
    eval("sub  () { 0; }") unless defined(&);
    eval("sub  () { 1; }") unless defined(&);
    eval("sub  () { 2; }") unless defined(&);
    eval("sub  () { 3; }") unless defined(&);
    eval("sub  () { 4; }") unless defined(&);
    eval("sub  () { 5; }") unless defined(&);
    eval("sub  () { 6; }") unless defined(&);
    eval("sub  () { 7; }") unless defined(&);
    eval("sub  () { 8; }") unless defined(&);
    eval("sub  () { 9; }") unless defined(&);
    eval("sub  () { 10; }") unless defined(&);
    eval("sub  () { 11; }") unless defined(&);
    eval("sub  () { 12; }") unless defined(&);
    eval("sub  () { 13; }") unless defined(&);
    eval("sub  () { 14; }") unless defined(&);
    undef(&D) if defined(&D);
}
1;
