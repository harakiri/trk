require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_BITOPS_H)) {
    eval 'sub _I386_BITOPS_H () {1;}' unless defined(&_I386_BITOPS_H);
    require 'linux/config.ph';
    require 'linux/compiler.ph';
    if(defined(&CONFIG_SMP)) {
	eval 'sub LOCK_PREFIX () {"lock ; ";}' unless defined(&LOCK_PREFIX);
    } else {
	eval 'sub LOCK_PREFIX () {"";}' unless defined(&LOCK_PREFIX);
    }
    eval 'sub ADDR () {(*( &volatile \'long\' *)  &addr);}' unless defined(&ADDR);
    eval 'sub __clear_bit {
        local($nr,$addr) = @_;
	    eval q({  &__asm__  &__volatile__( \\"btrl %1,%0\\" :\\"=m\\" ( &ADDR) :\\"Ir\\" ($nr)); });
    }' unless defined(&__clear_bit);
    eval 'sub smp_mb__before_clear_bit () {
        eval q( &barrier());
    }' unless defined(&smp_mb__before_clear_bit);
    eval 'sub smp_mb__after_clear_bit () {
        eval q( &barrier());
    }' unless defined(&smp_mb__after_clear_bit);
    if(0) {
    }
    eval 'sub constant_test_bit {
        local($nr,$addr) = @_;
	    eval q({ ((1 << ($nr & 31)) & ($addr->[$nr >> 5])) != 0; });
    }' unless defined(&constant_test_bit);
    eval 'sub variable_test_bit {
        local($nr,$addr) = @_;
	    eval q({ \'int\'  &oldbit;  &__asm__  &__volatile__( \\"btl %2,%1\\\\n\\\\tsbbl %0,%0\\" :\\"=r\\" ( &oldbit) :\\"m\\" ( &ADDR),\\"Ir\\" ($nr));  &oldbit; });
    }' unless defined(&variable_test_bit);
    eval 'sub test_bit {
        local($nr,$addr) = @_;
	    eval q(( &__builtin_constant_p($nr) ?  &constant_test_bit(($nr),($addr)) :  &variable_test_bit(($nr),($addr))));
    }' unless defined(&test_bit);
    undef(&ADDR) if defined(&ADDR);
    eval 'sub fls {
        local($x) = @_;
	    eval q( &generic_fls($x));
    }' unless defined(&fls);
    if(defined(&__KERNEL__)) {
	eval 'sub hweight32 {
	    local($x) = @_;
    	    eval q( &generic_hweight32($x));
	}' unless defined(&hweight32);
	eval 'sub hweight16 {
	    local($x) = @_;
    	    eval q( &generic_hweight16($x));
	}' unless defined(&hweight16);
	eval 'sub hweight8 {
	    local($x) = @_;
    	    eval q( &generic_hweight8($x));
	}' unless defined(&hweight8);
    }
    if(defined(&__KERNEL__)) {
	eval 'sub ext2_set_bit {
	    local($nr,$addr) = @_;
    	    eval q( &__test_and_set_bit(($nr),$addr));
	}' unless defined(&ext2_set_bit);
	eval 'sub ext2_set_bit_atomic {
	    local($lock,$nr,$addr) = @_;
    	    eval q( &test_and_set_bit(($nr),$addr));
	}' unless defined(&ext2_set_bit_atomic);
	eval 'sub ext2_clear_bit {
	    local($nr, $addr) = @_;
    	    eval q( &__test_and_clear_bit(($nr),$addr));
	}' unless defined(&ext2_clear_bit);
	eval 'sub ext2_clear_bit_atomic {
	    local($lock,$nr, $addr) = @_;
    	    eval q( &test_and_clear_bit(($nr),$addr));
	}' unless defined(&ext2_clear_bit_atomic);
	eval 'sub ext2_test_bit {
	    local($nr, $addr) = @_;
    	    eval q( &test_bit(($nr),$addr));
	}' unless defined(&ext2_test_bit);
	eval 'sub ext2_find_first_zero_bit {
	    local($addr, $size) = @_;
    	    eval q( &find_first_zero_bit($addr, $size));
	}' unless defined(&ext2_find_first_zero_bit);
	eval 'sub ext2_find_next_zero_bit {
	    local($addr, $size, $off) = @_;
    	    eval q( &find_next_zero_bit($addr, $size, $off));
	}' unless defined(&ext2_find_next_zero_bit);
	eval 'sub minix_test_and_set_bit {
	    local($nr,$addr) = @_;
    	    eval q( &__test_and_set_bit($nr,( &void*)$addr));
	}' unless defined(&minix_test_and_set_bit);
	eval 'sub minix_set_bit {
	    local($nr,$addr) = @_;
    	    eval q( &__set_bit($nr,( &void*)$addr));
	}' unless defined(&minix_set_bit);
	eval 'sub minix_test_and_clear_bit {
	    local($nr,$addr) = @_;
    	    eval q( &__test_and_clear_bit($nr,( &void*)$addr));
	}' unless defined(&minix_test_and_clear_bit);
	eval 'sub minix_test_bit {
	    local($nr,$addr) = @_;
    	    eval q( &test_bit($nr,( &void*)$addr));
	}' unless defined(&minix_test_bit);
	eval 'sub minix_find_first_zero_bit {
	    local($addr,$size) = @_;
    	    eval q( &find_first_zero_bit(( &void*)$addr,$size));
	}' unless defined(&minix_find_first_zero_bit);
    }
}
1;
