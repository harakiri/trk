require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_FIXMAP_H)) {
    eval 'sub _ASM_FIXMAP_H () {1;}' unless defined(&_ASM_FIXMAP_H);
    require 'linux/config.ph';
    eval 'sub __FIXADDR_TOP () {0xfffff000;}' unless defined(&__FIXADDR_TOP);
    unless(defined(&__ASSEMBLY__)) {
	require 'linux/kernel.ph';
	require 'asm/acpi.ph';
	require 'asm/apicdef.ph';
	require 'asm/page.ph';
	if(defined(&CONFIG_HIGHMEM)) {
	    require 'linux/threads.ph';
	    require 'asm/kmap_types.ph';
	}
	eval("sub FIX_HOLE () { 0; }") unless defined(&FIX_HOLE);
	eval("sub FIX_VSYSCALL () { 1; }") unless defined(&FIX_VSYSCALL);
	eval("sub __end_of_permanent_fixed_addresses () { 2; }") unless defined(&__end_of_permanent_fixed_addresses);
	eval("sub  () { 3; }") unless defined(&);
	eval("sub FIX_BTMAP_BEGIN () { FIX_BTMAP_END+NR_FIX_BTMAPS-1; }") unless defined(&FIX_BTMAP_BEGIN);
	eval("sub FIX_WP_TEST () { 1; }") unless defined(&FIX_WP_TEST);
	eval("sub __end_of_fixed_addresses () { 2; }") unless defined(&__end_of_fixed_addresses);
	eval 'sub set_fixmap {
	    local($idx, $phys) = @_;
    	    eval q( &__set_fixmap($idx, $phys,  &PAGE_KERNEL));
	}' unless defined(&set_fixmap);
	eval 'sub set_fixmap_nocache {
	    local($idx, $phys) = @_;
    	    eval q( &__set_fixmap($idx, $phys,  &PAGE_KERNEL_NOCACHE));
	}' unless defined(&set_fixmap_nocache);
	eval 'sub clear_fixmap {
	    local($idx) = @_;
    	    eval q( &__set_fixmap($idx, 0,  &__pgprot(0)));
	}' unless defined(&clear_fixmap);
	eval 'sub FIXADDR_TOP () {( &__FIXADDR_TOP);}' unless defined(&FIXADDR_TOP);
	eval 'sub __FIXADDR_SIZE () {( &__end_of_permanent_fixed_addresses <<  &PAGE_SHIFT);}' unless defined(&__FIXADDR_SIZE);
	eval 'sub FIXADDR_START () {( &FIXADDR_TOP -  &__FIXADDR_SIZE);}' unless defined(&FIXADDR_START);
	eval 'sub __fix_to_virt {
	    local($x) = @_;
    	    eval q(( &FIXADDR_TOP - (($x) <<  &PAGE_SHIFT)));
	}' unless defined(&__fix_to_virt);
	eval 'sub __virt_to_fix {
	    local($x) = @_;
    	    eval q((( &FIXADDR_TOP - (($x) &PAGE_MASK)) >>  &PAGE_SHIFT));
	}' unless defined(&__virt_to_fix);
	eval 'sub FIXADDR_USER_START () {( &__fix_to_virt( &FIX_VSYSCALL));}' unless defined(&FIXADDR_USER_START);
	eval 'sub FIXADDR_USER_END () {( &FIXADDR_USER_START +  &PAGE_SIZE);}' unless defined(&FIXADDR_USER_END);
	eval 'sub virt_to_fix {
	    local($vaddr) = @_;
    	    eval q({  &BUG_ON($vaddr >=  &FIXADDR_TOP || $vaddr <  &FIXADDR_START);  &__virt_to_fix($vaddr); });
	}' unless defined(&virt_to_fix);
    }
}
1;
