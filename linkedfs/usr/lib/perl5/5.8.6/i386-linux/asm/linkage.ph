require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_LINKAGE_H)) {
    eval 'sub __ASM_LINKAGE_H () {1;}' unless defined(&__ASM_LINKAGE_H);
    eval 'sub asmlinkage () { &CPP_ASMLINKAGE  &__attribute__(( &regparm(0)));}' unless defined(&asmlinkage);
    eval 'sub FASTCALL {
        local($x) = @_;
	    eval q($x  &__attribute__(( &regparm(3))));
    }' unless defined(&FASTCALL);
    eval 'sub fastcall () { &__attribute__(( &regparm(3)));}' unless defined(&fastcall);
    if(defined(&CONFIG_REGPARM)) {
	eval 'sub prevent_tail_call {
	    local($ret) = @_;
    	    eval q( &__asm__ (\\"\\" : \\"=r\\" ($ret) : \\"0\\" ($ret)));
	}' unless defined(&prevent_tail_call);
    }
    if(defined(&CONFIG_X86_ALIGNMENT_16)) {
	eval 'sub __ALIGN () {. &align 16,0x90;}' unless defined(&__ALIGN);
	eval 'sub __ALIGN_STR () {".align 16,0x90";}' unless defined(&__ALIGN_STR);
    }
}
1;
