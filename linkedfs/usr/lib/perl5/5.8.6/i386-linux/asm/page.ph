require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_PAGE_H)) {
    eval 'sub _I386_PAGE_H () {1;}' unless defined(&_I386_PAGE_H);
    eval 'sub PAGE_SHIFT () {12;}' unless defined(&PAGE_SHIFT);
    eval 'sub PAGE_SIZE () {(1 <<  &PAGE_SHIFT);}' unless defined(&PAGE_SIZE);
    eval 'sub PAGE_MASK () {(~( &PAGE_SIZE-1));}' unless defined(&PAGE_MASK);
    eval 'sub LARGE_PAGE_MASK () {(~( &LARGE_PAGE_SIZE-1));}' unless defined(&LARGE_PAGE_MASK);
    eval 'sub LARGE_PAGE_SIZE () {(1 <<  &PMD_SHIFT);}' unless defined(&LARGE_PAGE_SIZE);
    if(defined(&__KERNEL__)) {
	unless(defined(&__ASSEMBLY__)) {
	    require 'linux/config.ph';
	    if(defined(&CONFIG_X86_USE_3DNOW)) {
		require 'asm/mmx.ph';
		eval 'sub clear_page {
		    local($page) = @_;
    		    eval q( &mmx_clear_page(( &void *)($page)));
		}' unless defined(&clear_page);
		eval 'sub copy_page {
		    local($to,$from) = @_;
    		    eval q( &mmx_copy_page($to,$from));
		}' unless defined(&copy_page);
	    } else {
		eval 'sub clear_page {
		    local($page) = @_;
    		    eval q( &memset(( &void *)($page), 0,  &PAGE_SIZE));
		}' unless defined(&clear_page);
		eval 'sub copy_page {
		    local($to,$from) = @_;
    		    eval q( &memcpy(( &void *)($to), ( &void *)($from),  &PAGE_SIZE));
		}' unless defined(&copy_page);
	    }
	    eval 'sub clear_user_page {
	        local($page, $vaddr, $pg) = @_;
    		eval q( &clear_page($page));
	    }' unless defined(&clear_user_page);
	    eval 'sub copy_user_page {
	        local($to, $from, $vaddr, $pg) = @_;
    		eval q( &copy_page($to, $from));
	    }' unless defined(&copy_user_page);
	    if(defined(&CONFIG_X86_PAE)) {
		eval 'sub pte_val {
		    local($x) = @_;
    		    eval q((($x). &pte_low | (($x). &pte_high << 32)));
		}' unless defined(&pte_val);
		eval 'sub HPAGE_SHIFT () {21;}' unless defined(&HPAGE_SHIFT);
	    } else {
		eval 'sub boot_pte_t () { &pte_t;}' unless defined(&boot_pte_t);
		eval 'sub pte_val {
		    local($x) = @_;
    		    eval q((($x). &pte_low));
		}' unless defined(&pte_val);
		eval 'sub HPAGE_SHIFT () {22;}' unless defined(&HPAGE_SHIFT);
	    }
	    eval 'sub PTE_MASK () { &PAGE_MASK;}' unless defined(&PTE_MASK);
	    if(defined(&CONFIG_HUGETLB_PAGE)) {
		eval 'sub HPAGE_SIZE () {((1) <<  &HPAGE_SHIFT);}' unless defined(&HPAGE_SIZE);
		eval 'sub HPAGE_MASK () {(~( &HPAGE_SIZE - 1));}' unless defined(&HPAGE_MASK);
		eval 'sub HUGETLB_PAGE_ORDER () {( &HPAGE_SHIFT -  &PAGE_SHIFT);}' unless defined(&HUGETLB_PAGE_ORDER);
		eval 'sub HAVE_ARCH_HUGETLB_UNMAPPED_AREA () {1;}' unless defined(&HAVE_ARCH_HUGETLB_UNMAPPED_AREA);
	    }
	    eval 'sub pmd_val {
	        local($x) = @_;
    		eval q((($x). &pmd));
	    }' unless defined(&pmd_val);
	    eval 'sub pgd_val {
	        local($x) = @_;
    		eval q((($x). &pgd));
	    }' unless defined(&pgd_val);
	    eval 'sub pgprot_val {
	        local($x) = @_;
    		eval q((($x). &pgprot));
	    }' unless defined(&pgprot_val);
	    eval 'sub __pte {
	        local($x) = @_;
    		eval q((( &pte_t) { ($x) } ));
	    }' unless defined(&__pte);
	    eval 'sub __pmd {
	        local($x) = @_;
    		eval q((( &pmd_t) { ($x) } ));
	    }' unless defined(&__pmd);
	    eval 'sub __pgd {
	        local($x) = @_;
    		eval q((( &pgd_t) { ($x) } ));
	    }' unless defined(&__pgd);
	    eval 'sub __pgprot {
	        local($x) = @_;
    		eval q((( &pgprot_t) { ($x) } ));
	    }' unless defined(&__pgprot);
	}
	eval 'sub PAGE_ALIGN {
	    local($addr) = @_;
    	    eval q(((($addr)+ &PAGE_SIZE-1) &PAGE_MASK));
	}' unless defined(&PAGE_ALIGN);
	unless(defined(&__ASSEMBLY__)) {
	}
	if(defined(&__ASSEMBLY__)) {
	    eval 'sub __PAGE_OFFSET () {(0xc0000000);}' unless defined(&__PAGE_OFFSET);
	} else {
	    eval 'sub __PAGE_OFFSET () {(0xc0000000);}' unless defined(&__PAGE_OFFSET);
	}
	eval 'sub PAGE_OFFSET () {( &__PAGE_OFFSET);}' unless defined(&PAGE_OFFSET);
	eval 'sub VMALLOC_RESERVE () {( &__VMALLOC_RESERVE);}' unless defined(&VMALLOC_RESERVE);
	eval 'sub MAXMEM () {(- &__PAGE_OFFSET- &__VMALLOC_RESERVE);}' unless defined(&MAXMEM);
	eval 'sub __pa {
	    local($x) = @_;
    	    eval q((($x)- &PAGE_OFFSET));
	}' unless defined(&__pa);
	eval 'sub __va {
	    local($x) = @_;
    	    eval q((( &void *)(($x)+ &PAGE_OFFSET)));
	}' unless defined(&__va);
	eval 'sub pfn_to_kaddr {
	    local($pfn) = @_;
    	    eval q( &__va(($pfn) <<  &PAGE_SHIFT));
	}' unless defined(&pfn_to_kaddr);
	unless(defined(&CONFIG_DISCONTIGMEM)) {
	    eval 'sub pfn_to_page {
	        local($pfn) = @_;
    		eval q(( &mem_map + ($pfn)));
	    }' unless defined(&pfn_to_page);
	    eval 'sub page_to_pfn {
	        local($page) = @_;
    		eval q(((($page) -  &mem_map)));
	    }' unless defined(&page_to_pfn);
	    eval 'sub pfn_valid {
	        local($pfn) = @_;
    		eval q((($pfn) <  &max_mapnr));
	    }' unless defined(&pfn_valid);
	}
	eval 'sub virt_to_page {
	    local($kaddr) = @_;
    	    eval q( &pfn_to_page( &__pa($kaddr) >>  &PAGE_SHIFT));
	}' unless defined(&virt_to_page);
	eval 'sub phys_to_page {
	    local($x) = @_;
    	    eval q(( &mem_map + (($x) >>  &PAGE_SHIFT)));
	}' unless defined(&phys_to_page);
	eval 'sub virt_addr_valid {
	    local($kaddr) = @_;
    	    eval q( &pfn_valid( &__pa($kaddr) >>  &PAGE_SHIFT));
	}' unless defined(&virt_addr_valid);
	eval 'sub VM_DATA_DEFAULT_FLAGS () {( &VM_READ |  &VM_WRITE | (( ($current->{personality}) &  &READ_IMPLIES_EXEC) ?  &VM_EXEC : 0) |  &VM_MAYREAD |  &VM_MAYWRITE |  &VM_MAYEXEC);}' unless defined(&VM_DATA_DEFAULT_FLAGS);
    }
}
1;
