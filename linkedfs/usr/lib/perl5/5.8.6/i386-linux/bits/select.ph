require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_SYS_SELECT_H)) {
    die("Never use <bits/select.h> directly; include <sys/select.h> instead.");
}
if(defined (defined(&__GNUC__) ? &__GNUC__ : 0)  && (defined(&__GNUC__) ? &__GNUC__ : 0) >= 2) {
    eval 'sub __FD_ZERO {
        local($fdsp) = @_;
	    eval q( &do { \'int\'  &__d0,  &__d1;  &__asm__  &__volatile__ (\\"cld; rep; stosl\\" : \\"=c\\" ( &__d0), \\"=D\\" ( &__d1) : \\"a\\" (0), \\"0\\" ($sizeof{ &fd_set} / $sizeof{ &__fd_mask}), \\"1\\" (& &__FDS_BITS ($fdsp)[0]) : \\"memory\\"); }  &while (0));
    }' unless defined(&__FD_ZERO);
    eval 'sub __FD_SET {
        local($fd, $fdsp) = @_;
	    eval q( &__asm__  &__volatile__ (\\"btsl %1,%0\\" : \\"=m\\" ( &__FDS_BITS [ &__FDELT ]) : \\"r\\" (( ($fd)) %  &__NFDBITS) : \\"cc\\",\\"memory\\"));
    }' unless defined(&__FD_SET);
    eval 'sub __FD_CLR {
        local($fd, $fdsp) = @_;
	    eval q( &__asm__  &__volatile__ (\\"btrl %1,%0\\" : \\"=m\\" ( &__FDS_BITS [ &__FDELT ]) : \\"r\\" (( ($fd)) %  &__NFDBITS) : \\"cc\\",\\"memory\\"));
    }' unless defined(&__FD_CLR);
    eval 'sub __FD_ISSET {
        local($fd, $fdsp) = @_;
	    eval q(( &__extension__ ({ &register \'char\'  &__result;  &__asm__  &__volatile__ (\\"btl %1,%2 ; setcb %b0\\" : \\"=q\\" : \\"r\\" (( ($fd)) %  &__NFDBITS), \\"m\\" ( &__FDS_BITS ($fdsp)[ &__FDELT ($fd)]) : \\"cc\\");  &__result; })));
    }' unless defined(&__FD_ISSET);
} else {
    eval 'sub __FD_ZERO {
        local($set) = @_;
	    eval q( &do { \'unsigned int __i\';  &fd_set * &__arr = ($set);  &for ( &__i = 0;  &__i < $sizeof{ &fd_set} / $sizeof{ &__fd_mask}; ++ &__i)  &__FDS_BITS ( &__arr)[ &__i] = 0; }  &while (0));
    }' unless defined(&__FD_ZERO);
    eval 'sub __FD_SET {
        local($d, $set) = @_;
	    eval q(( &__FDS_BITS ($set)[ &__FDELT ($d)] |=  &__FDMASK ($d)));
    }' unless defined(&__FD_SET);
    eval 'sub __FD_CLR {
        local($d, $set) = @_;
	    eval q(( &__FDS_BITS ($set)[ &__FDELT ($d)] &= ~ &__FDMASK ($d)));
    }' unless defined(&__FD_CLR);
    eval 'sub __FD_ISSET {
        local($d, $set) = @_;
	    eval q(( &__FDS_BITS ($set)[ &__FDELT ($d)] &  &__FDMASK ($d)));
    }' unless defined(&__FD_ISSET);
}
1;
