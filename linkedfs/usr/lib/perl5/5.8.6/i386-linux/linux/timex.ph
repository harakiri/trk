require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_TIMEX_H)) {
    eval 'sub _LINUX_TIMEX_H () {1;}' unless defined(&_LINUX_TIMEX_H);
    require 'linux/config.ph';
    require 'linux/compiler.ph';
    require 'linux/time.ph';
    require 'asm/param.ph';
    require 'asm/timex.ph';
    eval 'sub SHIFT_KG () {6;}' unless defined(&SHIFT_KG);
    eval 'sub SHIFT_KF () {16;}' unless defined(&SHIFT_KF);
    eval 'sub SHIFT_KH () {2;}' unless defined(&SHIFT_KH);
    eval 'sub MAXTC () {6;}' unless defined(&MAXTC);
    eval 'sub SHIFT_SCALE () {22;}' unless defined(&SHIFT_SCALE);
    eval 'sub SHIFT_UPDATE () {( &SHIFT_KG +  &MAXTC);}' unless defined(&SHIFT_UPDATE);
    eval 'sub SHIFT_USEC () {16;}' unless defined(&SHIFT_USEC);
    eval 'sub FINENSEC () {(1 << ( &SHIFT_SCALE - 10));}' unless defined(&FINENSEC);
    eval 'sub MAXPHASE () {512000;}' unless defined(&MAXPHASE);
    eval 'sub MAXFREQ () {(512 <<  &SHIFT_USEC);}' unless defined(&MAXFREQ);
    eval 'sub MAXTIME () {(200 <<  &PPS_AVG);}' unless defined(&MAXTIME);
    eval 'sub MINSEC () {16;}' unless defined(&MINSEC);
    eval 'sub MAXSEC () {1200;}' unless defined(&MAXSEC);
    eval 'sub NTP_PHASE_LIMIT () {( &MAXPHASE << 5);}' unless defined(&NTP_PHASE_LIMIT);
    eval 'sub PPS_AVG () {2;}' unless defined(&PPS_AVG);
    eval 'sub PPS_SHIFT () {2;}' unless defined(&PPS_SHIFT);
    eval 'sub PPS_SHIFTMAX () {8;}' unless defined(&PPS_SHIFTMAX);
    eval 'sub PPS_VALID () {120;}' unless defined(&PPS_VALID);
    eval 'sub MAXGLITCH () {30;}' unless defined(&MAXGLITCH);
    eval 'sub ADJ_OFFSET () {0x1;}' unless defined(&ADJ_OFFSET);
    eval 'sub ADJ_FREQUENCY () {0x2;}' unless defined(&ADJ_FREQUENCY);
    eval 'sub ADJ_MAXERROR () {0x4;}' unless defined(&ADJ_MAXERROR);
    eval 'sub ADJ_ESTERROR () {0x8;}' unless defined(&ADJ_ESTERROR);
    eval 'sub ADJ_STATUS () {0x10;}' unless defined(&ADJ_STATUS);
    eval 'sub ADJ_TIMECONST () {0x20;}' unless defined(&ADJ_TIMECONST);
    eval 'sub ADJ_TICK () {0x4000;}' unless defined(&ADJ_TICK);
    eval 'sub ADJ_OFFSET_SINGLESHOT () {0x8001;}' unless defined(&ADJ_OFFSET_SINGLESHOT);
    eval 'sub MOD_OFFSET () { &ADJ_OFFSET;}' unless defined(&MOD_OFFSET);
    eval 'sub MOD_FREQUENCY () { &ADJ_FREQUENCY;}' unless defined(&MOD_FREQUENCY);
    eval 'sub MOD_MAXERROR () { &ADJ_MAXERROR;}' unless defined(&MOD_MAXERROR);
    eval 'sub MOD_ESTERROR () { &ADJ_ESTERROR;}' unless defined(&MOD_ESTERROR);
    eval 'sub MOD_STATUS () { &ADJ_STATUS;}' unless defined(&MOD_STATUS);
    eval 'sub MOD_TIMECONST () { &ADJ_TIMECONST;}' unless defined(&MOD_TIMECONST);
    eval 'sub MOD_CLKB () { &ADJ_TICK;}' unless defined(&MOD_CLKB);
    eval 'sub MOD_CLKA () { &ADJ_OFFSET_SINGLESHOT;}' unless defined(&MOD_CLKA);
    eval 'sub STA_PLL () {0x1;}' unless defined(&STA_PLL);
    eval 'sub STA_PPSFREQ () {0x2;}' unless defined(&STA_PPSFREQ);
    eval 'sub STA_PPSTIME () {0x4;}' unless defined(&STA_PPSTIME);
    eval 'sub STA_FLL () {0x8;}' unless defined(&STA_FLL);
    eval 'sub STA_INS () {0x10;}' unless defined(&STA_INS);
    eval 'sub STA_DEL () {0x20;}' unless defined(&STA_DEL);
    eval 'sub STA_UNSYNC () {0x40;}' unless defined(&STA_UNSYNC);
    eval 'sub STA_FREQHOLD () {0x80;}' unless defined(&STA_FREQHOLD);
    eval 'sub STA_PPSSIGNAL () {0x100;}' unless defined(&STA_PPSSIGNAL);
    eval 'sub STA_PPSJITTER () {0x200;}' unless defined(&STA_PPSJITTER);
    eval 'sub STA_PPSWANDER () {0x400;}' unless defined(&STA_PPSWANDER);
    eval 'sub STA_PPSERROR () {0x800;}' unless defined(&STA_PPSERROR);
    eval 'sub STA_CLOCKERR () {0x1000;}' unless defined(&STA_CLOCKERR);
    eval 'sub STA_RONLY () {( &STA_PPSSIGNAL |  &STA_PPSJITTER |  &STA_PPSWANDER |  &STA_PPSERROR |  &STA_CLOCKERR);}' unless defined(&STA_RONLY);
    eval 'sub TIME_OK () {0;}' unless defined(&TIME_OK);
    eval 'sub TIME_INS () {1;}' unless defined(&TIME_INS);
    eval 'sub TIME_DEL () {2;}' unless defined(&TIME_DEL);
    eval 'sub TIME_OOP () {3;}' unless defined(&TIME_OOP);
    eval 'sub TIME_WAIT () {4;}' unless defined(&TIME_WAIT);
    eval 'sub TIME_ERROR () {5;}' unless defined(&TIME_ERROR);
    eval 'sub TIME_BAD () { &TIME_ERROR;}' unless defined(&TIME_BAD);
    if(defined(&__KERNEL__)) {
	if(defined(&CONFIG_TIME_INTERPOLATION)) {
	    eval 'sub TIME_SOURCE_CPU () {0;}' unless defined(&TIME_SOURCE_CPU);
	    eval 'sub TIME_SOURCE_MMIO64 () {1;}' unless defined(&TIME_SOURCE_MMIO64);
	    eval 'sub TIME_SOURCE_MMIO32 () {2;}' unless defined(&TIME_SOURCE_MMIO32);
	    eval 'sub TIME_SOURCE_FUNCTION () {3;}' unless defined(&TIME_SOURCE_FUNCTION);
	} else {
	    eval 'sub time_interpolator_reset {
	        local($void) = @_;
    		eval q({ });
	    }' unless defined(&time_interpolator_reset);
	}
    }
}
1;
