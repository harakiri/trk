require '_h2ph_pre.ph';

no warnings 'redefine';

require 'linux/compiler-gcc.ph';
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) < 96) {
    eval 'sub __builtin_expect {
        local($x, $expected_value) = @_;
	    eval q(($x));
    }' unless defined(&__builtin_expect);
}
unless(defined(&__attribute_used__)) {
    sub __attribute_used__ () {	 &__attribute__(( &__unused__));}
}
if((defined(&__GNUC_MINOR__) ? &__GNUC_MINOR__ : 0) >= 96) {
    eval 'sub __attribute_pure__ () { &__attribute__(( &pure));}' unless defined(&__attribute_pure__);
    eval 'sub __attribute_const__ () { &__attribute__(( &__const__));}' unless defined(&__attribute_const__);
}
1;
