require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_IPC_H)) {
    eval 'sub _LINUX_IPC_H () {1;}' unless defined(&_LINUX_IPC_H);
    require 'linux/types.ph';
    eval 'sub IPC_PRIVATE () {(( &__kernel_key_t) 0);}' unless defined(&IPC_PRIVATE);
    require 'asm/ipcbuf.ph';
    eval 'sub IPC_CREAT () {00001000;}' unless defined(&IPC_CREAT);
    eval 'sub IPC_EXCL () {00002000;}' unless defined(&IPC_EXCL);
    eval 'sub IPC_NOWAIT () {00004000;}' unless defined(&IPC_NOWAIT);
    eval 'sub IPC_DIPC () {00010000;}' unless defined(&IPC_DIPC);
    eval 'sub IPC_OWN () {00020000;}' unless defined(&IPC_OWN);
    eval 'sub IPC_RMID () {0;}' unless defined(&IPC_RMID);
    eval 'sub IPC_SET () {1;}' unless defined(&IPC_SET);
    eval 'sub IPC_STAT () {2;}' unless defined(&IPC_STAT);
    eval 'sub IPC_INFO () {3;}' unless defined(&IPC_INFO);
    eval 'sub IPC_OLD () {0;}' unless defined(&IPC_OLD);
    eval 'sub IPC_64 () {0x100;}' unless defined(&IPC_64);
    if(defined(&__KERNEL__)) {
	eval 'sub IPCMNI () {32768;}' unless defined(&IPCMNI);
    }
}
1;
