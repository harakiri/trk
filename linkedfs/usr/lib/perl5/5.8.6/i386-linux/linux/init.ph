require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_INIT_H)) {
    eval 'sub _LINUX_INIT_H () {1;}' unless defined(&_LINUX_INIT_H);
    require 'linux/config.ph';
    require 'linux/compiler.ph';
    eval 'sub __init () { &__attribute__ (( &__section__ (".init.text")));}' unless defined(&__init);
    eval 'sub __initdata () { &__attribute__ (( &__section__ (".init.data")));}' unless defined(&__initdata);
    eval 'sub __exitdata () { &__attribute__ (( &__section__(".exit.data")));}' unless defined(&__exitdata);
    eval 'sub __exit_call () { &__attribute_used__  &__attribute__ (( &__section__ (".exitcall.exit")));}' unless defined(&__exit_call);
    if(defined(&MODULE)) {
	eval 'sub __exit () { &__attribute__ (( &__section__(".exit.text")));}' unless defined(&__exit);
    } else {
	eval 'sub __exit () { &__attribute_used__  &__attribute__ (( &__section__(".exit.text")));}' unless defined(&__exit);
    }
    eval 'sub __INIT () {. &section ".init.text","ax";}' unless defined(&__INIT);
    eval 'sub __FINIT () {. &previous;}' unless defined(&__FINIT);
    eval 'sub __INITDATA () {. &section ".init.data","aw";}' unless defined(&__INITDATA);
    unless(defined(&__ASSEMBLY__)) {
    }
    unless(defined(&MODULE)) {
	unless(defined(&__ASSEMBLY__)) {
	    eval 'sub __define_initcall {
	        local($level,$fn) = @_;
    		eval q( &static  &initcall_t  &__initcall_$fn  &__attribute_used__  &__attribute__(( &__section__(\\".initcall\\" $level \\".init\\"))) = $fn);
	    }' unless defined(&__define_initcall);
	    eval 'sub core_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"1\\",$fn));
	    }' unless defined(&core_initcall);
	    eval 'sub postcore_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"2\\",$fn));
	    }' unless defined(&postcore_initcall);
	    eval 'sub arch_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"3\\",$fn));
	    }' unless defined(&arch_initcall);
	    eval 'sub subsys_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"4\\",$fn));
	    }' unless defined(&subsys_initcall);
	    eval 'sub fs_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"5\\",$fn));
	    }' unless defined(&fs_initcall);
	    eval 'sub device_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"6\\",$fn));
	    }' unless defined(&device_initcall);
	    eval 'sub late_initcall {
	        local($fn) = @_;
    		eval q( &__define_initcall(\\"7\\",$fn));
	    }' unless defined(&late_initcall);
	    eval 'sub __initcall {
	        local($fn) = @_;
    		eval q( &device_initcall($fn));
	    }' unless defined(&__initcall);
	    eval 'sub __exitcall {
	        local($fn) = @_;
    		eval q( &static  &exitcall_t  &__exitcall_$fn  &__exit_call = $fn);
	    }' unless defined(&__exitcall);
	    eval 'sub console_initcall {
	        local($fn) = @_;
    		eval q( &static  &initcall_t  &__initcall_$fn  &__attribute_used__  &__attribute__(( &__section__(\\".con_initcall.init\\")))=$fn);
	    }' unless defined(&console_initcall);
	    eval 'sub security_initcall {
	        local($fn) = @_;
    		eval q( &static  &initcall_t  &__initcall_$fn  &__attribute_used__  &__attribute__(( &__section__(\\".security_initcall.init\\"))) = $fn);
	    }' unless defined(&security_initcall);
	    eval 'sub __setup_param {
	        local($str, $unique_id, $fn, $early) = @_;
    		eval q( &static \'char\'  &__setup_str_$unique_id->[]  &__initdata = $str;  &static \'struct obs_kernel_param\'  &__setup_$unique_id  &__attribute_used__  &__attribute__(( &__section__(\\".init.setup\\")))  &__attribute__(( &aligned(($sizeof{\'long\'})))) = {  &__setup_str_$unique_id, $fn, $early });
	    }' unless defined(&__setup_param);
	    eval 'sub __setup_null_param {
	        local($str, $unique_id) = @_;
    		eval q( &__setup_param($str, $unique_id,  &NULL, 0));
	    }' unless defined(&__setup_null_param);
	    eval 'sub __setup {
	        local($str, $fn) = @_;
    		eval q( &__setup_param($str, $fn, $fn, 0));
	    }' unless defined(&__setup);
	    eval 'sub __obsolete_setup {
	        local($str) = @_;
    		eval q( &__setup_null_param($str,  &__LINE__));
	    }' unless defined(&__obsolete_setup);
	    eval 'sub early_param {
	        local($str, $fn) = @_;
    		eval q( &__setup_param($str, $fn, $fn, 1));
	    }' unless defined(&early_param);
	}
	eval 'sub module_init {
	    local($x) = @_;
    	    eval q( &__initcall($x););
	}' unless defined(&module_init);
	eval 'sub module_exit {
	    local($x) = @_;
    	    eval q( &__exitcall($x););
	}' unless defined(&module_exit);
    } else {
	eval 'sub core_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&core_initcall);
	eval 'sub postcore_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&postcore_initcall);
	eval 'sub arch_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&arch_initcall);
	eval 'sub subsys_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&subsys_initcall);
	eval 'sub fs_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&fs_initcall);
	eval 'sub device_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&device_initcall);
	eval 'sub late_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&late_initcall);
	eval 'sub security_initcall {
	    local($fn) = @_;
    	    eval q( &module_init($fn));
	}' unless defined(&security_initcall);
	eval 'sub module_init {
	    local($initfn) = @_;
    	    eval q( &static  &inline  &initcall_t  &__inittest( &void) {  &return $initfn; } \'int\'  &init_module( &void)  &__attribute__(( &alias($initfn))););
	}' unless defined(&module_init);
	eval 'sub module_exit {
	    local($exitfn) = @_;
    	    eval q( &static  &inline  &exitcall_t  &__exittest( &void) {  &return $exitfn; }  &void  &cleanup_module( &void)  &__attribute__(( &alias($exitfn))););
	}' unless defined(&module_exit);
	eval 'sub __setup_param {
	    local($str, $unique_id, $fn) = @_;
    	    eval q();
	}' unless defined(&__setup_param);
	eval 'sub __setup_null_param {
	    local($str, $unique_id) = @_;
    	    eval q();
	}' unless defined(&__setup_null_param);
	eval 'sub __setup {
	    local($str, $func) = @_;
    	    eval q();
	}' unless defined(&__setup);
	eval 'sub __obsolete_setup {
	    local($str) = @_;
    	    eval q();
	}' unless defined(&__obsolete_setup);
    }
    eval 'sub __nosavedata () { &__attribute__ (( &__section__ (".data.nosave")));}' unless defined(&__nosavedata);
    if(defined(&CONFIG_MODULES)) {
	eval 'sub __init_or_module () {1;}' unless defined(&__init_or_module);
	eval 'sub __initdata_or_module () {1;}' unless defined(&__initdata_or_module);
    } else {
	eval 'sub __init_or_module () { &__init;}' unless defined(&__init_or_module);
	eval 'sub __initdata_or_module () { &__initdata;}' unless defined(&__initdata_or_module);
    }
    if(defined(&CONFIG_HOTPLUG)) {
	eval 'sub __devinit () {1;}' unless defined(&__devinit);
	eval 'sub __devinitdata () {1;}' unless defined(&__devinitdata);
	eval 'sub __devexit () {1;}' unless defined(&__devexit);
	eval 'sub __devexitdata () {1;}' unless defined(&__devexitdata);
    } else {
	eval 'sub __devinit () { &__init;}' unless defined(&__devinit);
	eval 'sub __devinitdata () { &__initdata;}' unless defined(&__devinitdata);
	eval 'sub __devexit () { &__exit;}' unless defined(&__devexit);
	eval 'sub __devexitdata () { &__exitdata;}' unless defined(&__devexitdata);
    }
    if(defined( &MODULE) || defined( &CONFIG_HOTPLUG)) {
	eval 'sub __devexit_p {
	    local($x) = @_;
    	    eval q($x);
	}' unless defined(&__devexit_p);
    } else {
	eval 'sub __devexit_p {
	    local($x) = @_;
    	    eval q( &NULL);
	}' unless defined(&__devexit_p);
    }
    if(defined(&MODULE)) {
	eval 'sub __exit_p {
	    local($x) = @_;
    	    eval q($x);
	}' unless defined(&__exit_p);
    } else {
	eval 'sub __exit_p {
	    local($x) = @_;
    	    eval q( &NULL);
	}' unless defined(&__exit_p);
    }
}
1;
