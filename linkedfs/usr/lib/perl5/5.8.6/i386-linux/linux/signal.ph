require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_SIGNAL_H)) {
    eval 'sub _LINUX_SIGNAL_H () {1;}' unless defined(&_LINUX_SIGNAL_H);
    require 'linux/list.ph';
    require 'linux/spinlock.ph';
    require 'asm/signal.ph';
    require 'asm/siginfo.ph';
    if(defined(&__KERNEL__)) {
	eval 'sub MAX_SIGPENDING () {1024;}' unless defined(&MAX_SIGPENDING);
	eval 'sub SIGQUEUE_PREALLOC () {1;}' unless defined(&SIGQUEUE_PREALLOC);
	unless(defined(&__HAVE_ARCH_SIG_BITOPS)) {
	    require 'linux/bitops.ph';
	    eval 'sub sigdelset {
	        local($set,$_sig) = @_;
    		eval q({ my $sig = $_sig - 1;  &if ( &_NSIG_WORDS == 1)  ($set->{sig[0]}) &= ~(1 <<  $sig);  &else  ($set->{sig[$sig/_NSIG_BPW]}) &= ~(1 << ( $sig %  &_NSIG_BPW)); });
	    }' unless defined(&sigdelset);
	    eval 'sub sigismember {
	        local($set,$_sig) = @_;
    		eval q({ my $sig = $_sig - 1;  &if ( &_NSIG_WORDS == 1) 1& ( ($set->{sig[0]}) >>  $sig);  &else 1& ( ($set->{sig[$sig/_NSIG_BPW]}) >> ( $sig %  &_NSIG_BPW)); });
	    }' unless defined(&sigismember);
	    eval 'sub sigfindinword {
	        local($word) = @_;
    		eval q({  &ffz(~$word); });
	    }' unless defined(&sigfindinword);
	}
	eval 'sub sigmask {
	    local($sig) = @_;
    	    eval q((1 << (($sig) - 1)));
	}' unless defined(&sigmask);
	unless(defined(&__HAVE_ARCH_SIG_SETOPS)) {
	    require 'linux/string.ph';
	    eval 'sub _SIG_SET_BINOP {
	        local($name, $op) = @_;
    		eval q( &static  &inline  &void $name( &sigset_t * &r,  &const  &sigset_t * &a,  &const  &sigset_t * &b) {  &extern  &void  &_NSIG_WORDS_is_unsupported_size( &void); \'unsigned long a0\',  &a1,  &a2,  &a3,  &b0,  &b1,  &b2,  &b3;  &switch ( &_NSIG_WORDS) {  &case 4:  &a3 =  ($a->{sig[3]});  &a2 =  ($a->{sig[2]});  &b3 =  ($b->{sig[3]});  &b2 =  ($b->{sig[2]});  ($r->{sig[3]}) = $op( &a3,  &b3);  ($r->{sig[2]}) = $op( &a2,  &b2);  &case 2:  &a1 =  ($a->{sig[1]});  &b1 =  ($b->{sig[1]});  ($r->{sig[1]}) = $op( &a1,  &b1);  &case 1:  &a0 =  ($a->{sig[0]});  &b0 =  ($b->{sig[0]});  ($r->{sig[0]}) = $op( &a0,  &b0);  &break;  &default:  &_NSIG_WORDS_is_unsupported_size(); } });
	    }' unless defined(&_SIG_SET_BINOP);
	    eval 'sub _sig_or {
	        local($x,$y) = @_;
    		eval q((($x) | ($y)));
	    }' unless defined(&_sig_or);
	    eval 'sub _sig_and {
	        local($x,$y) = @_;
    		eval q((($x) & ($y)));
	    }' unless defined(&_sig_and);
	    eval 'sub _sig_nand {
	        local($x,$y) = @_;
    		eval q((($x) & ~($y)));
	    }' unless defined(&_sig_nand);
	    undef(&_SIG_SET_BINOP) if defined(&_SIG_SET_BINOP);
	    undef(&_sig_or) if defined(&_sig_or);
	    undef(&_sig_and) if defined(&_sig_and);
	    undef(&_sig_nand) if defined(&_sig_nand);
	    eval 'sub _SIG_SET_OP {
	        local($name, $op) = @_;
    		eval q( &static  &inline  &void $name( &sigset_t * &set) {  &extern  &void  &_NSIG_WORDS_is_unsupported_size( &void);  &switch ( &_NSIG_WORDS) {  &case 4:  ($set->{sig[3]}) = $op( ($set->{sig[3]}));  ($set->{sig[2]}) = $op( ($set->{sig[2]}));  &case 2:  ($set->{sig[1]}) = $op( ($set->{sig[1]}));  &case 1:  ($set->{sig[0]}) = $op( ($set->{sig[0]}));  &break;  &default:  &_NSIG_WORDS_is_unsupported_size(); } });
	    }' unless defined(&_SIG_SET_OP);
	    eval 'sub _sig_not {
	        local($x) = @_;
    		eval q((~($x)));
	    }' unless defined(&_sig_not);
	    undef(&_SIG_SET_OP) if defined(&_SIG_SET_OP);
	    undef(&_sig_not) if defined(&_sig_not);
	    eval 'sub sigemptyset {
	        local($set) = @_;
    		eval q({  &switch ( &_NSIG_WORDS) {  &default:  &memset($set, 0, $sizeof{ &sigset_t});  &break;  &case 2:  ($set->{sig[1]}) = 0;  &case 1:  ($set->{sig[0]}) = 0;  &break; } });
	    }' unless defined(&sigemptyset);
	    eval 'sub sigfillset {
	        local($set) = @_;
    		eval q({  &switch ( &_NSIG_WORDS) {  &default:  &memset($set, -1, $sizeof{ &sigset_t});  &break;  &case 2:  ($set->{sig[1]}) = -1;  &case 1:  ($set->{sig[0]}) = -1;  &break; } });
	    }' unless defined(&sigfillset);
	    eval 'sub sigdelsetmask {
	        local($set,$mask) = @_;
    		eval q({  ($set->{sig[0]}) &= ~$mask; });
	    }' unless defined(&sigdelsetmask);
	    eval 'sub sigtestsetmask {
	        local($set,$mask) = @_;
    		eval q({ ( ($set->{sig[0]}) & $mask) != 0; });
	    }' unless defined(&sigtestsetmask);
	    eval 'sub siginitset {
	        local($set,$mask) = @_;
    		eval q({  ($set->{sig[0]}) = $mask;  &switch ( &_NSIG_WORDS) {  &default:  &memset( ($set->{sig[1]}), 0, $sizeof{\'long\'}*( &_NSIG_WORDS-1));  &break;  &case 2:  ($set->{sig[1]}) = 0;  &case 1: ; } });
	    }' unless defined(&siginitset);
	    eval 'sub siginitsetinv {
	        local($set,$mask) = @_;
    		eval q({  ($set->{sig[0]}) = ~$mask;  &switch ( &_NSIG_WORDS) {  &default:  &memset( ($set->{sig[1]}), -1, $sizeof{\'long\'}*( &_NSIG_WORDS-1));  &break;  &case 2:  ($set->{sig[1]}) = -1;  &case 1: ; } });
	    }' unless defined(&siginitsetinv);
	}
	eval 'sub init_sigpending {
	    local($sig) = @_;
    	    eval q({  &sigemptyset( ($sig->{signal}));  &INIT_LIST_HEAD( ($sig->{list})); });
	}' unless defined(&init_sigpending);
	unless(defined(&HAVE_ARCH_GET_SIGNAL_TO_DELIVER)) {
	}
    }
}
1;
