require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_JIFFIES_H)) {
    eval 'sub _LINUX_JIFFIES_H () {1;}' unless defined(&_LINUX_JIFFIES_H);
    require 'linux/kernel.ph';
    require 'linux/types.ph';
    require 'linux/time.ph';
    require 'linux/timex.ph';
    require 'asm/param.ph';
    require 'asm/div64.ph';
    unless(defined(&div_long_long_rem)) {
	eval 'sub div_long_long_rem {
	    local($dividend,$divisor,$remainder) = @_;
    	    eval q(({  &u64  &result = $dividend; *$remainder =  &do_div( &result,$divisor);  &result; }));
	}' unless defined(&div_long_long_rem);
    }
    if((defined(&HZ) ? &HZ : 0) >= 12 && (defined(&HZ) ? &HZ : 0) < 24) {
	eval 'sub SHIFT_HZ () {4;}' unless defined(&SHIFT_HZ);
    }
 elsif((defined(&HZ) ? &HZ : 0) >= 24 && (defined(&HZ) ? &HZ : 0) < 48) {
	eval 'sub SHIFT_HZ () {5;}' unless defined(&SHIFT_HZ);
    }
 elsif((defined(&HZ) ? &HZ : 0) >= 48 && (defined(&HZ) ? &HZ : 0) < 96) {
	eval 'sub SHIFT_HZ () {6;}' unless defined(&SHIFT_HZ);
    }
 elsif((defined(&HZ) ? &HZ : 0) >= 96 && (defined(&HZ) ? &HZ : 0) < 192) {
	eval 'sub SHIFT_HZ () {7;}' unless defined(&SHIFT_HZ);
    }
 elsif((defined(&HZ) ? &HZ : 0) >= 192 && (defined(&HZ) ? &HZ : 0) < 384) {
	eval 'sub SHIFT_HZ () {8;}' unless defined(&SHIFT_HZ);
    }
 elsif((defined(&HZ) ? &HZ : 0) >= 384 && (defined(&HZ) ? &HZ : 0) < 768) {
	eval 'sub SHIFT_HZ () {9;}' unless defined(&SHIFT_HZ);
    }
 elsif((defined(&HZ) ? &HZ : 0) >= 768 && (defined(&HZ) ? &HZ : 0) < 1536) {
	eval 'sub SHIFT_HZ () {10;}' unless defined(&SHIFT_HZ);
    } else {
	die("You\ lose\.");
    }
    eval 'sub LATCH () {(( &CLOCK_TICK_RATE +  &HZ/2) /  &HZ);}' unless defined(&LATCH);
    eval 'sub SH_DIV {
        local($NOM,$DEN,$LSH) = @_;
	    eval q(( (($NOM / $DEN) << $LSH) + ((($NOM % $DEN) << $LSH) + $DEN / 2) / $DEN));
    }' unless defined(&SH_DIV);
    eval 'sub ACTHZ () {( &SH_DIV ( &CLOCK_TICK_RATE,  &LATCH, 8));}' unless defined(&ACTHZ);
    eval 'sub TICK_NSEC () {( &SH_DIV (1000000 * 1000,  &ACTHZ, 8));}' unless defined(&TICK_NSEC);
    eval 'sub TICK_USEC () {((1000000 +  &USER_HZ/2) /  &USER_HZ);}' unless defined(&TICK_USEC);
    eval 'sub TICK_USEC_TO_NSEC {
        local($TUSEC) = @_;
	    eval q(( &SH_DIV ($TUSEC *  &USER_HZ * 1000,  &ACTHZ, 8)));
    }' unless defined(&TICK_USEC_TO_NSEC);
    if(((defined(&BITS_PER_LONG) ? &BITS_PER_LONG : 0) < 64)) {
    } else {
	eval 'sub get_jiffies_64 {
	    local($void) = @_;
    	    eval q({ ( &u64) &jiffies; });
	}' unless defined(&get_jiffies_64);
    }
    eval 'sub time_after {
        local($a,$b) = @_;
	    eval q(( &typecheck(\'unsigned long\', $a)  &&  &typecheck(\'unsigned long\', $b)  && ( - ($a) < 0)));
    }' unless defined(&time_after);
    eval 'sub time_before {
        local($a,$b) = @_;
	    eval q( &time_after($b,$a));
    }' unless defined(&time_before);
    eval 'sub time_after_eq {
        local($a,$b) = @_;
	    eval q(( &typecheck(\'unsigned long\', $a)  &&  &typecheck(\'unsigned long\', $b)  && ( - ($b) >= 0)));
    }' unless defined(&time_after_eq);
    eval 'sub time_before_eq {
        local($a,$b) = @_;
	    eval q( &time_after_eq($b,$a));
    }' unless defined(&time_before_eq);
    eval 'sub INITIAL_JIFFIES () {( (-300* &HZ));}' unless defined(&INITIAL_JIFFIES);
    eval 'sub MAX_JIFFY_OFFSET () {((~0 >> 1)-1);}' unless defined(&MAX_JIFFY_OFFSET);
    eval 'sub SEC_JIFFIE_SC () {(31-  &SHIFT_HZ);}' unless defined(&SEC_JIFFIE_SC);
    if(!(((((defined(&NSEC_PER_SEC) ? &NSEC_PER_SEC : 0) << 2) / (defined(&TICK_NSEC) ? &TICK_NSEC : 0)) << ((defined(&SEC_JIFFIE_SC) ? &SEC_JIFFIE_SC : 0) - 2)) & 0x80000000)) {
	undef(&SEC_JIFFIE_SC) if defined(&SEC_JIFFIE_SC);
	eval 'sub SEC_JIFFIE_SC () {(32-  &SHIFT_HZ);}' unless defined(&SEC_JIFFIE_SC);
    }
    eval 'sub NSEC_JIFFIE_SC () {( &SEC_JIFFIE_SC + 29);}' unless defined(&NSEC_JIFFIE_SC);
    eval 'sub USEC_JIFFIE_SC () {( &SEC_JIFFIE_SC + 19);}' unless defined(&USEC_JIFFIE_SC);
    eval 'sub SEC_CONVERSION () {((((( &u64) &NSEC_PER_SEC <<  &SEC_JIFFIE_SC) +  &TICK_NSEC -1) / ( &u64) &TICK_NSEC));}' unless defined(&SEC_CONVERSION);
    eval 'sub NSEC_CONVERSION () {((((( &u64)1<<  &NSEC_JIFFIE_SC) +  &TICK_NSEC -1) / ( &u64) &TICK_NSEC));}' unless defined(&NSEC_CONVERSION);
    eval 'sub USEC_CONVERSION () {((((( &u64) &NSEC_PER_USEC <<  &USEC_JIFFIE_SC) +  &TICK_NSEC -1) / ( &u64) &TICK_NSEC));}' unless defined(&USEC_CONVERSION);
    eval 'sub USEC_ROUND () {( &u64)((( &u64)1<<  &USEC_JIFFIE_SC) - 1);}' unless defined(&USEC_ROUND);
    if((defined(&BITS_PER_LONG) ? &BITS_PER_LONG : 0) < 64) {
	eval 'sub MAX_SEC_IN_JIFFIES () {(( &u64)(( &u64) &MAX_JIFFY_OFFSET *  &TICK_NSEC) /  &NSEC_PER_SEC);}' unless defined(&MAX_SEC_IN_JIFFIES);
    } else {
	eval 'sub MAX_SEC_IN_JIFFIES () {( &SH_DIV(( &MAX_JIFFY_OFFSET >>  &SEC_JIFFIE_SC) *  &TICK_NSEC,  &NSEC_PER_SEC, 1) - 1);}' unless defined(&MAX_SEC_IN_JIFFIES);
    }
    if((defined(&HZ) ? &HZ : 0) <= 1000 && !(1000% (defined(&HZ) ? &HZ : 0))) {
    }
 elsif((defined(&HZ) ? &HZ : 0) > 1000 && !((defined(&HZ) ? &HZ : 0) % 1000)) {
    } else {
    }
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub jiffies_to_usecs {
        local($j) = @_;
	    eval q({ });
    }' unless defined(&jiffies_to_usecs);
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub msecs_to_jiffies {
        local($m) = @_;
	    eval q({  &if ($m >  &jiffies_to_msecs( &MAX_JIFFY_OFFSET))  &MAX_JIFFY_OFFSET; });
    }' unless defined(&msecs_to_jiffies);
    eval 'sub jiffies_to_timespec {
        local($jiffies,$value) = @_;
	    eval q({  &u64  &nsec = ( &u64)$jiffies *  &TICK_NSEC;  ($value->{tv_sec}) =  &div_long_long_rem( &nsec,  &NSEC_PER_SEC,  ($value->{tv_nsec})); });
    }' unless defined(&jiffies_to_timespec);
    eval 'sub jiffies_to_timeval {
        local($jiffies,$value) = @_;
	    eval q({  &u64  &nsec = ( &u64)$jiffies *  &TICK_NSEC;  ($value->{tv_sec}) =  &div_long_long_rem( &nsec,  &NSEC_PER_SEC,  ($value->{tv_usec}));  ($value->{tv_usec}) /=  &NSEC_PER_USEC; });
    }' unless defined(&jiffies_to_timeval);
    if(((defined(&TICK_NSEC) ? &TICK_NSEC : 0) % ((defined(&NSEC_PER_SEC) ? &NSEC_PER_SEC : 0) / (defined(&USER_HZ) ? &USER_HZ : 0))) == 0) {
    } else {
    }
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub clock_t_to_jiffies {
        local($x) = @_;
	    eval q({ });
    }' unless defined(&clock_t_to_jiffies);
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub jiffies_64_to_clock_t {
        local($x) = @_;
	    eval q({ $x; });
    }' unless defined(&jiffies_64_to_clock_t);
# some #ifdef were dropped here -- fill in the blanks
    eval 'sub nsec_to_clock_t {
        local($x) = @_;
	    eval q({ $x; });
    }' unless defined(&nsec_to_clock_t);
}
1;
