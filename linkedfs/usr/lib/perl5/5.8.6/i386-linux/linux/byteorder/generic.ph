require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_BYTEORDER_GENERIC_H)) {
    eval 'sub _LINUX_BYTEORDER_GENERIC_H () {1;}' unless defined(&_LINUX_BYTEORDER_GENERIC_H);
    if(defined( &__KERNEL__)) {
	eval 'sub cpu_to_le64 () { &__cpu_to_le64;}' unless defined(&cpu_to_le64);
	eval 'sub le64_to_cpu () { &__le64_to_cpu;}' unless defined(&le64_to_cpu);
	eval 'sub cpu_to_le32 () { &__cpu_to_le32;}' unless defined(&cpu_to_le32);
	eval 'sub le32_to_cpu () { &__le32_to_cpu;}' unless defined(&le32_to_cpu);
	eval 'sub cpu_to_le16 () { &__cpu_to_le16;}' unless defined(&cpu_to_le16);
	eval 'sub le16_to_cpu () { &__le16_to_cpu;}' unless defined(&le16_to_cpu);
	eval 'sub cpu_to_be64 () { &__cpu_to_be64;}' unless defined(&cpu_to_be64);
	eval 'sub be64_to_cpu () { &__be64_to_cpu;}' unless defined(&be64_to_cpu);
	eval 'sub cpu_to_be32 () { &__cpu_to_be32;}' unless defined(&cpu_to_be32);
	eval 'sub be32_to_cpu () { &__be32_to_cpu;}' unless defined(&be32_to_cpu);
	eval 'sub cpu_to_be16 () { &__cpu_to_be16;}' unless defined(&cpu_to_be16);
	eval 'sub be16_to_cpu () { &__be16_to_cpu;}' unless defined(&be16_to_cpu);
	eval 'sub cpu_to_le64p () { &__cpu_to_le64p;}' unless defined(&cpu_to_le64p);
	eval 'sub le64_to_cpup () { &__le64_to_cpup;}' unless defined(&le64_to_cpup);
	eval 'sub cpu_to_le32p () { &__cpu_to_le32p;}' unless defined(&cpu_to_le32p);
	eval 'sub le32_to_cpup () { &__le32_to_cpup;}' unless defined(&le32_to_cpup);
	eval 'sub cpu_to_le16p () { &__cpu_to_le16p;}' unless defined(&cpu_to_le16p);
	eval 'sub le16_to_cpup () { &__le16_to_cpup;}' unless defined(&le16_to_cpup);
	eval 'sub cpu_to_be64p () { &__cpu_to_be64p;}' unless defined(&cpu_to_be64p);
	eval 'sub be64_to_cpup () { &__be64_to_cpup;}' unless defined(&be64_to_cpup);
	eval 'sub cpu_to_be32p () { &__cpu_to_be32p;}' unless defined(&cpu_to_be32p);
	eval 'sub be32_to_cpup () { &__be32_to_cpup;}' unless defined(&be32_to_cpup);
	eval 'sub cpu_to_be16p () { &__cpu_to_be16p;}' unless defined(&cpu_to_be16p);
	eval 'sub be16_to_cpup () { &__be16_to_cpup;}' unless defined(&be16_to_cpup);
	eval 'sub cpu_to_le64s () { &__cpu_to_le64s;}' unless defined(&cpu_to_le64s);
	eval 'sub le64_to_cpus () { &__le64_to_cpus;}' unless defined(&le64_to_cpus);
	eval 'sub cpu_to_le32s () { &__cpu_to_le32s;}' unless defined(&cpu_to_le32s);
	eval 'sub le32_to_cpus () { &__le32_to_cpus;}' unless defined(&le32_to_cpus);
	eval 'sub cpu_to_le16s () { &__cpu_to_le16s;}' unless defined(&cpu_to_le16s);
	eval 'sub le16_to_cpus () { &__le16_to_cpus;}' unless defined(&le16_to_cpus);
	eval 'sub cpu_to_be64s () { &__cpu_to_be64s;}' unless defined(&cpu_to_be64s);
	eval 'sub be64_to_cpus () { &__be64_to_cpus;}' unless defined(&be64_to_cpus);
	eval 'sub cpu_to_be32s () { &__cpu_to_be32s;}' unless defined(&cpu_to_be32s);
	eval 'sub be32_to_cpus () { &__be32_to_cpus;}' unless defined(&be32_to_cpus);
	eval 'sub cpu_to_be16s () { &__cpu_to_be16s;}' unless defined(&cpu_to_be16s);
	eval 'sub be16_to_cpus () { &__be16_to_cpus;}' unless defined(&be16_to_cpus);
    }
    if(defined( &__KERNEL__)) {
	undef(&ntohl) if defined(&ntohl);
	undef(&ntohs) if defined(&ntohs);
	undef(&htonl) if defined(&htonl);
	undef(&htons) if defined(&htons);
	if(defined( &__GNUC__)  && ((defined(&__GNUC__) ? &__GNUC__ : 0) >= 2)  && defined( &__OPTIMIZE__)) {
	    eval 'sub ___htonl {
	        local($x) = @_;
    		eval q( &__cpu_to_be32($x));
	    }' unless defined(&___htonl);
	    eval 'sub ___htons {
	        local($x) = @_;
    		eval q( &__cpu_to_be16($x));
	    }' unless defined(&___htons);
	    eval 'sub ___ntohl {
	        local($x) = @_;
    		eval q( &__be32_to_cpu($x));
	    }' unless defined(&___ntohl);
	    eval 'sub ___ntohs {
	        local($x) = @_;
    		eval q( &__be16_to_cpu($x));
	    }' unless defined(&___ntohs);
	    eval 'sub htonl {
	        local($x) = @_;
    		eval q( &___htonl($x));
	    }' unless defined(&htonl);
	    eval 'sub ntohl {
	        local($x) = @_;
    		eval q( &___ntohl($x));
	    }' unless defined(&ntohl);
	    eval 'sub htons {
	        local($x) = @_;
    		eval q( &___htons($x));
	    }' unless defined(&htons);
	    eval 'sub ntohs {
	        local($x) = @_;
    		eval q( &___ntohs($x));
	    }' unless defined(&ntohs);
	}
    }
}
1;
