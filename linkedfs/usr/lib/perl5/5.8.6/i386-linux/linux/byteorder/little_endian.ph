require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_BYTEORDER_LITTLE_ENDIAN_H)) {
    eval 'sub _LINUX_BYTEORDER_LITTLE_ENDIAN_H () {1;}' unless defined(&_LINUX_BYTEORDER_LITTLE_ENDIAN_H);
    unless(defined(&__LITTLE_ENDIAN)) {
	eval 'sub __LITTLE_ENDIAN () {1234;}' unless defined(&__LITTLE_ENDIAN);
    }
    unless(defined(&__LITTLE_ENDIAN_BITFIELD)) {
	eval 'sub __LITTLE_ENDIAN_BITFIELD () {1;}' unless defined(&__LITTLE_ENDIAN_BITFIELD);
    }
    require 'linux/types.ph';
    require 'linux/byteorder/swab.ph';
    eval 'sub __constant_htonl {
        local($x) = @_;
	    eval q((( &__force  &__be32) &___constant_swab32(($x))));
    }' unless defined(&__constant_htonl);
    eval 'sub __constant_ntohl {
        local($x) = @_;
	    eval q( &___constant_swab32(( &__force  &__be32)($x)));
    }' unless defined(&__constant_ntohl);
    eval 'sub __constant_htons {
        local($x) = @_;
	    eval q((( &__force  &__be16) &___constant_swab16(($x))));
    }' unless defined(&__constant_htons);
    eval 'sub __constant_ntohs {
        local($x) = @_;
	    eval q( &___constant_swab16(( &__force  &__be16)($x)));
    }' unless defined(&__constant_ntohs);
    eval 'sub __constant_cpu_to_le64 {
        local($x) = @_;
	    eval q((( &__force  &__le64)( &__u64)($x)));
    }' unless defined(&__constant_cpu_to_le64);
    eval 'sub __constant_le64_to_cpu {
        local($x) = @_;
	    eval q((( &__force  &__u64)( &__le64)($x)));
    }' unless defined(&__constant_le64_to_cpu);
    eval 'sub __constant_cpu_to_le32 {
        local($x) = @_;
	    eval q((( &__force  &__le32)( &__u32)($x)));
    }' unless defined(&__constant_cpu_to_le32);
    eval 'sub __constant_le32_to_cpu {
        local($x) = @_;
	    eval q((( &__force  &__u32)( &__le32)($x)));
    }' unless defined(&__constant_le32_to_cpu);
    eval 'sub __constant_cpu_to_le16 {
        local($x) = @_;
	    eval q((( &__force  &__le16)( &__u16)($x)));
    }' unless defined(&__constant_cpu_to_le16);
    eval 'sub __constant_le16_to_cpu {
        local($x) = @_;
	    eval q((( &__force  &__u16)( &__le16)($x)));
    }' unless defined(&__constant_le16_to_cpu);
    eval 'sub __constant_cpu_to_be64 {
        local($x) = @_;
	    eval q((( &__force  &__be64) &___constant_swab64(($x))));
    }' unless defined(&__constant_cpu_to_be64);
    eval 'sub __constant_be64_to_cpu {
        local($x) = @_;
	    eval q( &___constant_swab64(( &__force  &__u64)( &__be64)($x)));
    }' unless defined(&__constant_be64_to_cpu);
    eval 'sub __constant_cpu_to_be32 {
        local($x) = @_;
	    eval q((( &__force  &__be32) &___constant_swab32(($x))));
    }' unless defined(&__constant_cpu_to_be32);
    eval 'sub __constant_be32_to_cpu {
        local($x) = @_;
	    eval q( &___constant_swab32(( &__force  &__u32)( &__be32)($x)));
    }' unless defined(&__constant_be32_to_cpu);
    eval 'sub __constant_cpu_to_be16 {
        local($x) = @_;
	    eval q((( &__force  &__be16) &___constant_swab16(($x))));
    }' unless defined(&__constant_cpu_to_be16);
    eval 'sub __constant_be16_to_cpu {
        local($x) = @_;
	    eval q( &___constant_swab16(( &__force  &__u16)( &__be16)($x)));
    }' unless defined(&__constant_be16_to_cpu);
    eval 'sub __cpu_to_le64 {
        local($x) = @_;
	    eval q((( &__force  &__le64)( &__u64)($x)));
    }' unless defined(&__cpu_to_le64);
    eval 'sub __le64_to_cpu {
        local($x) = @_;
	    eval q((( &__force  &__u64)( &__le64)($x)));
    }' unless defined(&__le64_to_cpu);
    eval 'sub __cpu_to_le32 {
        local($x) = @_;
	    eval q((( &__force  &__le32)( &__u32)($x)));
    }' unless defined(&__cpu_to_le32);
    eval 'sub __le32_to_cpu {
        local($x) = @_;
	    eval q((( &__force  &__u32)( &__le32)($x)));
    }' unless defined(&__le32_to_cpu);
    eval 'sub __cpu_to_le16 {
        local($x) = @_;
	    eval q((( &__force  &__le16)( &__u16)($x)));
    }' unless defined(&__cpu_to_le16);
    eval 'sub __le16_to_cpu {
        local($x) = @_;
	    eval q((( &__force  &__u16)( &__le16)($x)));
    }' unless defined(&__le16_to_cpu);
    eval 'sub __cpu_to_be64 {
        local($x) = @_;
	    eval q((( &__force  &__be64) &__swab64(($x))));
    }' unless defined(&__cpu_to_be64);
    eval 'sub __be64_to_cpu {
        local($x) = @_;
	    eval q( &__swab64(( &__force  &__u64)( &__be64)($x)));
    }' unless defined(&__be64_to_cpu);
    eval 'sub __cpu_to_be32 {
        local($x) = @_;
	    eval q((( &__force  &__be32) &__swab32(($x))));
    }' unless defined(&__cpu_to_be32);
    eval 'sub __be32_to_cpu {
        local($x) = @_;
	    eval q( &__swab32(( &__force  &__u32)( &__be32)($x)));
    }' unless defined(&__be32_to_cpu);
    eval 'sub __cpu_to_be16 {
        local($x) = @_;
	    eval q((( &__force  &__be16) &__swab16(($x))));
    }' unless defined(&__cpu_to_be16);
    eval 'sub __be16_to_cpu {
        local($x) = @_;
	    eval q( &__swab16(( &__force  &__u16)( &__be16)($x)));
    }' unless defined(&__be16_to_cpu);
    eval 'sub __cpu_to_le64p {
        local($p) = @_;
	    eval q({ ( &__force  &__le64)*$p; });
    }' unless defined(&__cpu_to_le64p);
    eval 'sub __le64_to_cpup {
        local($p) = @_;
	    eval q({ ( &__force  &__u64)*$p; });
    }' unless defined(&__le64_to_cpup);
    eval 'sub __cpu_to_le32p {
        local($p) = @_;
	    eval q({ ( &__force  &__le32)*$p; });
    }' unless defined(&__cpu_to_le32p);
    eval 'sub __le32_to_cpup {
        local($p) = @_;
	    eval q({ ( &__force  &__u32)*$p; });
    }' unless defined(&__le32_to_cpup);
    eval 'sub __cpu_to_le16p {
        local($p) = @_;
	    eval q({ ( &__force  &__le16)*$p; });
    }' unless defined(&__cpu_to_le16p);
    eval 'sub __le16_to_cpup {
        local($p) = @_;
	    eval q({ ( &__force  &__u16)*$p; });
    }' unless defined(&__le16_to_cpup);
    eval 'sub __cpu_to_be64p {
        local($p) = @_;
	    eval q({ ( &__force  &__be64) &__swab64p($p); });
    }' unless defined(&__cpu_to_be64p);
    eval 'sub __be64_to_cpup {
        local($p) = @_;
	    eval q({  &__swab64p(( &__u64 *)$p); });
    }' unless defined(&__be64_to_cpup);
    eval 'sub __cpu_to_be32p {
        local($p) = @_;
	    eval q({ ( &__force  &__be32) &__swab32p($p); });
    }' unless defined(&__cpu_to_be32p);
    eval 'sub __be32_to_cpup {
        local($p) = @_;
	    eval q({  &__swab32p(( &__u32 *)$p); });
    }' unless defined(&__be32_to_cpup);
    eval 'sub __cpu_to_be16p {
        local($p) = @_;
	    eval q({ ( &__force  &__be16) &__swab16p($p); });
    }' unless defined(&__cpu_to_be16p);
    eval 'sub __be16_to_cpup {
        local($p) = @_;
	    eval q({  &__swab16p(( &__u16 *)$p); });
    }' unless defined(&__be16_to_cpup);
    eval 'sub __cpu_to_le64s {
        local($x) = @_;
	    eval q( &do {}  &while (0));
    }' unless defined(&__cpu_to_le64s);
    eval 'sub __le64_to_cpus {
        local($x) = @_;
	    eval q( &do {}  &while (0));
    }' unless defined(&__le64_to_cpus);
    eval 'sub __cpu_to_le32s {
        local($x) = @_;
	    eval q( &do {}  &while (0));
    }' unless defined(&__cpu_to_le32s);
    eval 'sub __le32_to_cpus {
        local($x) = @_;
	    eval q( &do {}  &while (0));
    }' unless defined(&__le32_to_cpus);
    eval 'sub __cpu_to_le16s {
        local($x) = @_;
	    eval q( &do {}  &while (0));
    }' unless defined(&__cpu_to_le16s);
    eval 'sub __le16_to_cpus {
        local($x) = @_;
	    eval q( &do {}  &while (0));
    }' unless defined(&__le16_to_cpus);
    eval 'sub __cpu_to_be64s {
        local($x) = @_;
	    eval q( &__swab64s(($x)));
    }' unless defined(&__cpu_to_be64s);
    eval 'sub __be64_to_cpus {
        local($x) = @_;
	    eval q( &__swab64s(($x)));
    }' unless defined(&__be64_to_cpus);
    eval 'sub __cpu_to_be32s {
        local($x) = @_;
	    eval q( &__swab32s(($x)));
    }' unless defined(&__cpu_to_be32s);
    eval 'sub __be32_to_cpus {
        local($x) = @_;
	    eval q( &__swab32s(($x)));
    }' unless defined(&__be32_to_cpus);
    eval 'sub __cpu_to_be16s {
        local($x) = @_;
	    eval q( &__swab16s(($x)));
    }' unless defined(&__cpu_to_be16s);
    eval 'sub __be16_to_cpus {
        local($x) = @_;
	    eval q( &__swab16s(($x)));
    }' unless defined(&__be16_to_cpus);
    require 'linux/byteorder/generic.ph';
}
1;
