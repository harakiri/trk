require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_SLAB_H)) {
    eval 'sub _LINUX_SLAB_H () {1;}' unless defined(&_LINUX_SLAB_H);
    if(defined( &__KERNEL__)) {
	require 'linux/config.ph';
	require 'linux/gfp.ph';
	require 'linux/init.ph';
	require 'linux/types.ph';
	require 'asm/page.ph';
	require 'asm/cache.ph';
	eval 'sub SLAB_NOFS () { &GFP_NOFS;}' unless defined(&SLAB_NOFS);
	eval 'sub SLAB_NOIO () { &GFP_NOIO;}' unless defined(&SLAB_NOIO);
	eval 'sub SLAB_ATOMIC () { &GFP_ATOMIC;}' unless defined(&SLAB_ATOMIC);
	eval 'sub SLAB_USER () { &GFP_USER;}' unless defined(&SLAB_USER);
	eval 'sub SLAB_KERNEL () { &GFP_KERNEL;}' unless defined(&SLAB_KERNEL);
	eval 'sub SLAB_DMA () { &GFP_DMA;}' unless defined(&SLAB_DMA);
	eval 'sub SLAB_LEVEL_MASK () { &GFP_LEVEL_MASK;}' unless defined(&SLAB_LEVEL_MASK);
	eval 'sub SLAB_NO_GROW () { &__GFP_NO_GROW;}' unless defined(&SLAB_NO_GROW);
	eval 'sub SLAB_DEBUG_FREE () {0x100;}' unless defined(&SLAB_DEBUG_FREE);
	eval 'sub SLAB_DEBUG_INITIAL () {0x200;}' unless defined(&SLAB_DEBUG_INITIAL);
	eval 'sub SLAB_RED_ZONE () {0x400;}' unless defined(&SLAB_RED_ZONE);
	eval 'sub SLAB_POISON () {0x800;}' unless defined(&SLAB_POISON);
	eval 'sub SLAB_NO_REAP () {0x1000;}' unless defined(&SLAB_NO_REAP);
	eval 'sub SLAB_HWCACHE_ALIGN () {0x2000;}' unless defined(&SLAB_HWCACHE_ALIGN);
	eval 'sub SLAB_CACHE_DMA () {0x4000;}' unless defined(&SLAB_CACHE_DMA);
	eval 'sub SLAB_MUST_HWCACHE_ALIGN () {0x8000;}' unless defined(&SLAB_MUST_HWCACHE_ALIGN);
	eval 'sub SLAB_STORE_USER () {0x10000;}' unless defined(&SLAB_STORE_USER);
	eval 'sub SLAB_RECLAIM_ACCOUNT () {0x20000;}' unless defined(&SLAB_RECLAIM_ACCOUNT);
	eval 'sub SLAB_PANIC () {0x40000;}' unless defined(&SLAB_PANIC);
	eval 'sub SLAB_DESTROY_BY_RCU () {0x80000;}' unless defined(&SLAB_DESTROY_BY_RCU);
	eval 'sub SLAB_CTOR_CONSTRUCTOR () {0x1;}' unless defined(&SLAB_CTOR_CONSTRUCTOR);
	eval 'sub SLAB_CTOR_ATOMIC () {0x2;}' unless defined(&SLAB_CTOR_ATOMIC);
	eval 'sub SLAB_CTOR_VERIFY () {0x4;}' unless defined(&SLAB_CTOR_VERIFY);
	if(defined(&CONFIG_NUMA)) {
	} else {
	    eval 'sub void {
	        eval q(* &kmem_cache_alloc_node( &kmem_cache_t * &cachep, \'int\'  &node) {  &kmem_cache_alloc( &cachep,  &GFP_KERNEL); });
	    }' unless defined(&void);
	}
	eval 'sub void {
	    eval q(* &kmalloc(\'size_t\'  &size, \'int\'  &flags) {  &if ( &__builtin_constant_p( &size)) { \'int\'  &i = 0;  &define  &CACHE( &x)  &if ( &size <=  &x)  &goto  &found;  &else  &i++;  &include \\"kmalloc_sizes.h\\"  &undef  &CACHE {  &void  &__you_cannot_kmalloc_that_much( &void);  &__you_cannot_kmalloc_that_much(); }  &found:  &kmem_cache_alloc(( &flags &  &GFP_DMA) ?  ($malloc_sizes[&i]->{cs_dmacachep}) :  ($malloc_sizes[&i]->{cs_cachep}),  &flags); }  &__kmalloc( &size,  &flags); });
	}' unless defined(&void);
    }
}
1;
