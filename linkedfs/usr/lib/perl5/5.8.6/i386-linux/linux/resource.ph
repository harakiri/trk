require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_RESOURCE_H)) {
    eval 'sub _LINUX_RESOURCE_H () {1;}' unless defined(&_LINUX_RESOURCE_H);
    require 'linux/time.ph';
    eval 'sub RUSAGE_SELF () {0;}' unless defined(&RUSAGE_SELF);
    eval 'sub RUSAGE_CHILDREN () {(-1);}' unless defined(&RUSAGE_CHILDREN);
    eval 'sub RUSAGE_BOTH () {(-2);}' unless defined(&RUSAGE_BOTH);
    eval 'sub PRIO_MIN () {(-20);}' unless defined(&PRIO_MIN);
    eval 'sub PRIO_MAX () {20;}' unless defined(&PRIO_MAX);
    eval 'sub PRIO_PROCESS () {0;}' unless defined(&PRIO_PROCESS);
    eval 'sub PRIO_PGRP () {1;}' unless defined(&PRIO_PGRP);
    eval 'sub PRIO_USER () {2;}' unless defined(&PRIO_USER);
    eval 'sub _STK_LIM () {(8*1024*1024);}' unless defined(&_STK_LIM);
    eval 'sub MLOCK_LIMIT () {(8*  &PAGE_SIZE);}' unless defined(&MLOCK_LIMIT);
    require 'asm/resource.ph';
}
1;
