require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_UTSNAME_H)) {
    eval 'sub _LINUX_UTSNAME_H () {1;}' unless defined(&_LINUX_UTSNAME_H);
    eval 'sub __OLD_UTS_LEN () {8;}' unless defined(&__OLD_UTS_LEN);
    eval 'sub __NEW_UTS_LEN () {64;}' unless defined(&__NEW_UTS_LEN);
}
1;
