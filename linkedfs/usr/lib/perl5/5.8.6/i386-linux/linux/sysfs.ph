require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_SYSFS_H_)) {
    eval 'sub _SYSFS_H_ () {1;}' unless defined(&_SYSFS_H_);
    require 'asm/atomic.ph';
    eval 'sub __ATTR {
        local($_name,$_mode,$_show,$_store) = @_;
	    eval q({ . &attr = {. &name =  &__stringify($_name), . &mode = $_mode, . &owner =  &THIS_MODULE }, . &show = $_show, . &store = $_store, });
    }' unless defined(&__ATTR);
    eval 'sub __ATTR_RO {
        local($_name) = @_;
	    eval q({ . &attr = { . &name =  &__stringify($_name), . &mode = 0444, . &owner =  &THIS_MODULE }, . &show = $_name &_show, });
    }' unless defined(&__ATTR_RO);
    eval 'sub __ATTR_NULL () {{ . &attr = { . &name =  &NULL } };}' unless defined(&__ATTR_NULL);
    eval 'sub attr_name {
        local($_attr) = @_;
	    eval q(($_attr). ($attr->{name}));
    }' unless defined(&attr_name);
    eval 'sub SYSFS_ROOT () {0x1;}' unless defined(&SYSFS_ROOT);
    eval 'sub SYSFS_DIR () {0x2;}' unless defined(&SYSFS_DIR);
    eval 'sub SYSFS_KOBJ_ATTR () {0x4;}' unless defined(&SYSFS_KOBJ_ATTR);
    eval 'sub SYSFS_KOBJ_BIN_ATTR () {0x8;}' unless defined(&SYSFS_KOBJ_BIN_ATTR);
    eval 'sub SYSFS_KOBJ_LINK () {0x20;}' unless defined(&SYSFS_KOBJ_LINK);
    eval 'sub SYSFS_NOT_PINNED () {( &SYSFS_KOBJ_ATTR |  &SYSFS_KOBJ_BIN_ATTR |  &SYSFS_KOBJ_LINK);}' unless defined(&SYSFS_NOT_PINNED);
    if(defined(&CONFIG_SYSFS)) {
    } else {
	eval 'sub sysfs_create_dir {
	    local($k) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_create_dir);
	eval 'sub sysfs_remove_dir {
	    local($k) = @_;
    	    eval q({ ; });
	}' unless defined(&sysfs_remove_dir);
	eval 'sub sysfs_rename_dir {
	    local($k,$new_name) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_rename_dir);
	eval 'sub sysfs_create_file {
	    local($k,$a) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_create_file);
	eval 'sub sysfs_update_file {
	    local($k,$a) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_update_file);
	eval 'sub sysfs_remove_file {
	    local($k,$a) = @_;
    	    eval q({ ; });
	}' unless defined(&sysfs_remove_file);
	eval 'sub sysfs_create_link {
	    local($k,$t,$n) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_create_link);
	eval 'sub sysfs_remove_link {
	    local($k,$name) = @_;
    	    eval q({ ; });
	}' unless defined(&sysfs_remove_link);
	eval 'sub sysfs_create_bin_file {
	    local($k,$a) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_create_bin_file);
	eval 'sub sysfs_remove_bin_file {
	    local($k,$a) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_remove_bin_file);
	eval 'sub sysfs_create_group {
	    local($k,$g) = @_;
    	    eval q({ 0; });
	}' unless defined(&sysfs_create_group);
	eval 'sub sysfs_remove_group {
	    local($k,$g) = @_;
    	    eval q({ ; });
	}' unless defined(&sysfs_remove_group);
    }
}
1;
