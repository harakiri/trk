require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_TIME_H)) {
    eval 'sub _LINUX_TIME_H () {1;}' unless defined(&_LINUX_TIME_H);
    require 'linux/types.ph';
    if(defined(&__KERNEL__)) {
	require 'linux/seqlock.ph';
    }
    unless(defined(&_STRUCT_TIMESPEC)) {
	eval 'sub _STRUCT_TIMESPEC () {1;}' unless defined(&_STRUCT_TIMESPEC);
    }
    if(defined(&__KERNEL__)) {
	unless(defined(&USEC_PER_SEC)) {
	    eval 'sub USEC_PER_SEC () {(1000000);}' unless defined(&USEC_PER_SEC);
	}
	unless(defined(&NSEC_PER_SEC)) {
	    eval 'sub NSEC_PER_SEC () {(1000000000);}' unless defined(&NSEC_PER_SEC);
	}
	unless(defined(&NSEC_PER_USEC)) {
	    eval 'sub NSEC_PER_USEC () {(1000);}' unless defined(&NSEC_PER_USEC);
	}
	eval 'sub timespec_equal {
	    local($a,$b) = @_;
    	    eval q({ ( ($a->{tv_sec}) ==  ($b->{tv_sec}))  && ( ($a->{tv_nsec}) ==  ($b->{tv_nsec})); } );
	}' unless defined(&timespec_equal);
	eval 'sub get_seconds {
	    local($void) = @_;
    	    eval q({  ($xtime->{tv_sec}); });
	}' unless defined(&get_seconds);
	eval 'sub CURRENT_TIME () {( &current_kernel_time());}' unless defined(&CURRENT_TIME);
	eval 'sub set_normalized_timespec {
	    local($ts,$sec,$nsec) = @_;
    	    eval q({  &while ($nsec >  &NSEC_PER_SEC) { $nsec -=  &NSEC_PER_SEC; ++$sec; }  &while ($nsec < 0) { $nsec +=  &NSEC_PER_SEC; --$sec; }  ($ts->{tv_sec}) = $sec;  ($ts->{tv_nsec}) = $nsec; });
	}' unless defined(&set_normalized_timespec);
    }
    eval 'sub NFDBITS () { &__NFDBITS;}' unless defined(&NFDBITS);
    eval 'sub FD_SETSIZE () { &__FD_SETSIZE;}' unless defined(&FD_SETSIZE);
    eval 'sub FD_SET {
        local($fd,$fdsetp) = @_;
	    eval q( &__FD_SET($fd,$fdsetp));
    }' unless defined(&FD_SET);
    eval 'sub FD_CLR {
        local($fd,$fdsetp) = @_;
	    eval q( &__FD_CLR($fd,$fdsetp));
    }' unless defined(&FD_CLR);
    eval 'sub FD_ISSET {
        local($fd,$fdsetp) = @_;
	    eval q( &__FD_ISSET($fd,$fdsetp));
    }' unless defined(&FD_ISSET);
    eval 'sub FD_ZERO {
        local($fdsetp) = @_;
	    eval q( &__FD_ZERO($fdsetp));
    }' unless defined(&FD_ZERO);
    eval 'sub ITIMER_REAL () {0;}' unless defined(&ITIMER_REAL);
    eval 'sub ITIMER_VIRTUAL () {1;}' unless defined(&ITIMER_VIRTUAL);
    eval 'sub ITIMER_PROF () {2;}' unless defined(&ITIMER_PROF);
    eval 'sub CLOCK_REALTIME () {0;}' unless defined(&CLOCK_REALTIME);
    eval 'sub CLOCK_MONOTONIC () {1;}' unless defined(&CLOCK_MONOTONIC);
    eval 'sub CLOCK_PROCESS_CPUTIME_ID () {2;}' unless defined(&CLOCK_PROCESS_CPUTIME_ID);
    eval 'sub CLOCK_THREAD_CPUTIME_ID () {3;}' unless defined(&CLOCK_THREAD_CPUTIME_ID);
    eval 'sub CLOCK_REALTIME_HR () {4;}' unless defined(&CLOCK_REALTIME_HR);
    eval 'sub CLOCK_MONOTONIC_HR () {5;}' unless defined(&CLOCK_MONOTONIC_HR);
    eval 'sub CLOCK_SGI_CYCLE () {10;}' unless defined(&CLOCK_SGI_CYCLE);
    eval 'sub MAX_CLOCKS () {16;}' unless defined(&MAX_CLOCKS);
    eval 'sub CLOCKS_MASK () {( &CLOCK_REALTIME |  &CLOCK_MONOTONIC |  &CLOCK_REALTIME_HR |  &CLOCK_MONOTONIC_HR);}' unless defined(&CLOCKS_MASK);
    eval 'sub CLOCKS_MONO () {( &CLOCK_MONOTONIC &  &CLOCK_MONOTONIC_HR);}' unless defined(&CLOCKS_MONO);
    eval 'sub TIMER_ABSTIME () {0x1;}' unless defined(&TIMER_ABSTIME);
}
1;
