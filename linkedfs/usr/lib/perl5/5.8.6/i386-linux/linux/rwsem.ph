require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_RWSEM_H)) {
    eval 'sub _LINUX_RWSEM_H () {1;}' unless defined(&_LINUX_RWSEM_H);
    require 'linux/linkage.ph';
    eval 'sub RWSEM_DEBUG () {0;}' unless defined(&RWSEM_DEBUG);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	require 'linux/types.ph';
	require 'linux/kernel.ph';
	require 'asm/system.ph';
	require 'asm/atomic.ph';
	if(defined(&CONFIG_RWSEM_GENERIC_SPINLOCK)) {
	    require 'linux/rwsem-spinlock.ph';
	} else {
	    require 'asm/rwsem.ph';
	}
	unless(defined(&rwsemtrace)) {
	    if((defined(&RWSEM_DEBUG) ? &RWSEM_DEBUG : 0)) {
	    } else {
		eval 'sub rwsemtrace {
		    local($SEM,$FMT) = @_;
    		    eval q();
		}' unless defined(&rwsemtrace);
	    }
	}
    }
}
1;
