require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_FS_STRUCT_H)) {
    eval 'sub _LINUX_FS_STRUCT_H () {1;}' unless defined(&_LINUX_FS_STRUCT_H);
    eval 'sub INIT_FS () {{ . &count =  &ATOMIC_INIT(1), . &lock =  &RW_LOCK_UNLOCKED, . &umask = 0022, };}' unless defined(&INIT_FS);
}
1;
