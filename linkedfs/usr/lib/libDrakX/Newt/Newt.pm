package Newt; # $Id: Newt.pm,v 1.4 2002/12/18 16:21:39 prigaux Exp $



use DynaLoader;


@ISA = qw(DynaLoader);
$VERSION = '0.01';
Newt->bootstrap($VERSION);

package Newt::Component;

our @ISA = qw(); # help perl_checker

package Newt::Grid;

our @ISA = qw(); # help perl_checker

1;
