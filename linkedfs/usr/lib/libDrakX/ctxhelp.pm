package ctxhelp;

use lib qw(/usr/lib/libDrakX);
use common;
use lang;

# 'id' => ['Globalhtmlfile', 'relativelink']
my %helppage = (
		'diskdrake' => ['diskdrake.html', undef],
		'diskdrake-fileshare' => ['diskdrake-fileshare.html', undef],
		'diskdrake-nfs' => ['diskdrake.html', undef],
		'diskdrake-removable' => ['diskdrake-removable.html', undef],
		'diskdrake-smb' => ['diskdrake-smb.html', undef],
		'drakautoinst' => ['drakautoinst.html', undef],
		'drakbackup' => ['drakbackup.html', undef],
		'drakboot' => ['drakboot.html', undef],
		'drakbug' => ['drakbug.html', undef],
		'drakconf-intro' => ['drakconf-intro.html', undef],
		'drakconsole' => ['drakconsole.html', undef],
		'drakfloppy' => ['drakfloppy.html', undef],
		'drakfont' => ['drakfont.html', undef],
		'drakgw' => ['drakgw.html', undef],
		'drakperm' => ['drakperm.html', undef],
		'draksec' => ['draksec.html', undef],
		'draktime' => ['draktime.html', undef],
		'drakxservices' => ['drakxservices.html', undef],
		'harddrake' => ['harddrake.html', undef],
		'internet-connection' => ['internet-connection.html', undef],
		'keyboarddrake' => ['keyboarddrake.html', undef],
		'logdrake' => ['logdrake.html', undef],
		'mcc-boot' => ['mcc-boot.html', undef],
		'mcc-hardware' => ['mcc-hardware.html', undef],
		'mcc-mountpoints' => ['mcc-mountpoints.html', undef],
		'mcc-network' => ['mcc-network.html', undef],
		'mcc-security' => ['mcc-security.html', undef],
		'mcc-system' => ['mcc-system.html', undef],
		'menudrake' => ['menudrake.html', undef],
		'mousedrake' => ['mousedrake.html', undef],
		'printerdrake' => ['printerdrake.html', undef],
		'software-management' => ['software-management.html', undef],
		'software-management-install' => ['software-management-install.html', undef],
		'software-management-remove' => ['software-management-remove.html', undef],
		'software-management-sources' => ['software-management-sources.html', undef],
		'software-management-update' => ['software-management-update.html', undef],
		'tinyfirewall' => ['tinyfirewall.html', undef],
		'userdrake' => ['userdrake.html', undef],
		'wiz-client' => ['wizdrake.html', undef],
		'wiz-dhcp' => ['wiz-dhcp.html', undef],
		'wiz-dns' => ['wiz-dns.html', undef],
		'wizdrake' => ['wizdrake.html', undef],
		'wiz-ftp' => ['wiz-ftp.html', undef],
		'wiz-mail' => ['wiz-mail.html', undef],
		'wiz-news' => ['wiz-news.html', undef],
		'wiz-proxy' => ['wiz-proxy.html', undef],
		'wiz-samba' => ['wiz-samba.html', undef],
		'wiz-server' => ['wiz-server.html', undef],
		'wiz-time' => ['wiz-time.html', undef],
		'wiz-web' => ['wiz-web.html', undef],
		'xfdrake' => ['xfdrake.html', undef]
	       );

sub id2file { exists $helppage{$_[0]} && $helppage{$_[0]}[0] }

sub id2anchorage { exists $helppage{$_[0]} && defined $helppage{$_[0]}[1] ? $helppage{$_[0]}[0] . '#' . $helppage{$_[0]}[1] : $helppage{$_[0]}[0] }

sub path2help {
    my ($opt, $id) = @_;
    my ($l, $path);
    my $locale = lang::read($>);
    $l = lc($locale->{lang});
    # For Debug Purpose
    # printf("\n** LANG = %s **\n", $l);
    $path = '/usr/share/doc/mandrake/';
    $l = 'en' unless member($l, qw(de en es fr it zh_CN ru));
    my %option = (
		  '--id' => sub { return ($path . $l . '/Drakxtools-Guide/Drakxtools-Guide.html/' . id2file($id), $path . $l . '/Drakxtools-Guide/Drakxtools-Guide.html/' . id2anchorage($id), 'mandrake-doc-' . if_(member($l, 'ru', 'zh_CN'), 'contrib-') . 'Drakxtools-Guide-' . $l) },
		  '--doc' => sub { return ($path . $l . '/' . $id, $path . $l . '/' . $id, 'mandrake-doc-' . $l) }
		 );
    $option{$opt}->();
}

1;
